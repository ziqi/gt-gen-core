/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANEGROUPCATALOGUE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANEGROUPCATALOGUE_H

#include "Core/Tests/TestUtils/MapCatalogue/raw_map_builder.h"

namespace gtgen::core::test_utils
{

class LaneGroupCatalogue
{
  public:
    enum class MergeType
    {
        kNone,
        kMergeLeft,
        kMergeRight,
        kMergeBoth,
        kSplitLeft,
        kSplitRight,
        kSplitBoth
    };

    struct LaneGroupConfiguration
    {
        std::size_t number_of_lanes{1};
        mantle_api::Vec3<units::length::meter_t> origin{units::length::meter_t(0),
                                                        units::length::meter_t(0),
                                                        units::length::meter_t(0)};
        MergeType merge_type{MergeType::kNone};
    };

    static void AddLaneGroupWithNEastingLanes(RawMapBuilder& map_builder,
                                              LaneGroupConfiguration config,
                                              bool right_lanes = true)
    {
        std::vector<std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>>
            lane_and_boundaries_vec{};
        for (std::size_t i = 0; i < config.number_of_lanes; i++)
        {
            if ((config.merge_type == MergeType::kMergeRight || config.merge_type == MergeType::kMergeBoth) &&
                i == config.number_of_lanes - 1)
            {
                lane_and_boundaries_vec.push_back(
                    LaneCatalogue::EastingMergingLeftLaneWithHundredPoints(config.origin));
            }
            else if ((config.merge_type == MergeType::kMergeLeft || config.merge_type == MergeType::kMergeBoth) &&
                     i == 0)
            {
                lane_and_boundaries_vec.push_back(
                    LaneCatalogue::EastingMergingRightRightLaneWithHundredPoints(config.origin));
            }
            else if ((config.merge_type == MergeType::kSplitLeft || config.merge_type == MergeType::kSplitBoth) &&
                     i == config.number_of_lanes - 1)
            {
                lane_and_boundaries_vec.push_back(
                    LaneCatalogue::EastingSplittingLeftLaneWithHundredPoints(config.origin));
            }
            else if ((config.merge_type == MergeType::kSplitRight || config.merge_type == MergeType::kSplitBoth) &&
                     i == 0)
            {
                lane_and_boundaries_vec.push_back(
                    LaneCatalogue::EastingSplittingRightLaneWithHundredPoints(config.origin));
            }
            else
            {
                lane_and_boundaries_vec.push_back(LaneCatalogue::StraightEastingLaneWithHundredPoints(config.origin));
            }

            config.origin.y += units::length::meter_t(4);

            // Add local IDs according to OpenDRIVE logic
            if (right_lanes)
            {
                lane_and_boundaries_vec.at(i).first.local_id = -config.number_of_lanes + i;
            }
            else
            {
                lane_and_boundaries_vec.at(i).first.local_id = config.number_of_lanes - i;
            }
        }

        // Connect neighbouring lanes
        for (std::size_t i = 1; i < config.number_of_lanes; i++)
        {
            lane_and_boundaries_vec.at(i - 1).first.left_adjacent_lanes.push_back(
                lane_and_boundaries_vec.at(i).first.id);
            lane_and_boundaries_vec.at(i).first.right_adjacent_lanes.push_back(
                lane_and_boundaries_vec.at(i - 1).first.id);
        }

        map_builder.AddNewLaneGroup();
        for (const auto& lane_and_boundaries : lane_and_boundaries_vec)
        {
            map_builder.AddFromCatalogue(lane_and_boundaries);
        }
    }

    static void AddLaneGroupWithOneEastingLane(RawMapBuilder& map_builder,
                                               mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 1;
        config.origin = origin;
        config.merge_type = MergeType::kNone;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithTwoEastingLanes(RawMapBuilder& map_builder,
                                                mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 2;
        config.origin = origin;
        config.merge_type = MergeType::kNone;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithThreeEastingLanes(RawMapBuilder& map_builder,
                                                  mantle_api::Vec3<units::length::meter_t> origin,
                                                  bool right_lanes = true)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 3;
        config.origin = origin;
        config.merge_type = MergeType::kNone;
        AddLaneGroupWithNEastingLanes(map_builder, config, right_lanes);
    }

    static void AddLaneGroupWithTwoEastingLanesMergeToRight(RawMapBuilder& map_builder,
                                                            mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 2;
        config.origin = origin;
        config.merge_type = MergeType::kMergeRight;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithTwoEastingLanesMergeToLeft(RawMapBuilder& map_builder,
                                                           mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 2;
        config.origin = origin;
        config.merge_type = MergeType::kMergeLeft;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithTwoEastingLanesSplitToLeft(RawMapBuilder& map_builder,
                                                           mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 2;
        config.origin = origin;
        config.merge_type = MergeType::kSplitLeft;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithTwoEastingLanesSplitToRight(RawMapBuilder& map_builder,
                                                            mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 2;
        config.origin = origin;
        config.merge_type = MergeType::kSplitRight;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithThreeEastingLanesSplitBoth(RawMapBuilder& map_builder,
                                                           mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 3;
        config.origin = origin;
        config.merge_type = MergeType::kSplitBoth;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithThreeEastingLanesMergeBoth(RawMapBuilder& map_builder,
                                                           mantle_api::Vec3<units::length::meter_t> origin)
    {
        LaneGroupConfiguration config;
        config.number_of_lanes = 3;
        config.origin = origin;
        config.merge_type = MergeType::kMergeBoth;
        AddLaneGroupWithNEastingLanes(map_builder, config);
    }

    static void AddLaneGroupWithOneNorthingToWestingCurve(RawMapBuilder& map_builder,
                                                          mantle_api::Vec3<units::length::meter_t> /*origin*/)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::NorthingToWestingCurve());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void AddLaneGroupWithRightSideCounterClockwiseSemiCircle(RawMapBuilder& map_builder)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::RightSideCounterClockwiseSemiCircle());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void AddLaneGroupWithLeftSideCounterClockwiseSemiCircle(RawMapBuilder& map_builder)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::LeftSideCounterClockwiseSemiCircle());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void MapOdrProblematicCurve(RawMapBuilder& map_builder, mantle_api::Vec3<units::length::meter_t> /*origin*/)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::MapOdrProblematicCurve());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void MapOdrProblematicStraightLine(RawMapBuilder& map_builder,
                                              mantle_api::Vec3<units::length::meter_t> /*origin*/)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::MapOdrProblematicStraightLine());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void MapOdrProblematicCurvyLine(RawMapBuilder& map_builder,
                                           mantle_api::Vec3<units::length::meter_t> /*origin*/)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::MapOdrProblematicCurvyLane());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void LaneWithSpatiallyIdenticalDuplicatedBoundaries(RawMapBuilder& map_builder,
                                                               mantle_api::Vec3<units::length::meter_t> /*origin*/)
    {
        map_builder.AddNewLaneGroup();
        map_builder.AddFromCatalogue(LaneCatalogue::LaneWithSpatiallyIdenticalDuplicatedBoundaries());
        map_builder.GetLastAddedLaneHandle().local_id = -1;
    }

    static void AddArcLaneGroupWithThreeLanes(
        RawMapBuilder& map_builder,
        const units::angle::radian_t start_angle,
        const units::angle::radian_t end_angle,
        const mantle_api::Vec3<units::length::meter_t> center = {},
        const units::length::meter_t middle_lane_radius = units::length::meter_t{100.0},
        const std::size_t number_of_points = 50)
    {
        const auto left_lane_radius = middle_lane_radius - units::length::meter_t{2.0};
        const auto right_lane_radius = middle_lane_radius + units::length::meter_t{2.0};

        map_builder.AddNewLaneGroup();

        auto left_lane_and_boundaries =
            LaneCatalogue::CounterClockwiseArcLane(center, left_lane_radius, start_angle, end_angle, number_of_points);
        auto middle_lane_and_boundaries = LaneCatalogue::CounterClockwiseArcLane(
            center, middle_lane_radius, start_angle, end_angle, number_of_points);
        auto right_lane_and_boundaries =
            LaneCatalogue::CounterClockwiseArcLane(center, right_lane_radius, start_angle, end_angle, number_of_points);

        left_lane_and_boundaries.first.right_adjacent_lanes.push_back(middle_lane_and_boundaries.first.id);
        middle_lane_and_boundaries.first.left_adjacent_lanes.push_back(left_lane_and_boundaries.first.id);
        middle_lane_and_boundaries.first.right_adjacent_lanes.push_back(right_lane_and_boundaries.first.id);
        right_lane_and_boundaries.first.left_adjacent_lanes.push_back(middle_lane_and_boundaries.first.id);

        // Add local IDs according to OpenDRIVE logic
        left_lane_and_boundaries.first.local_id = -1;
        middle_lane_and_boundaries.first.local_id = -2;
        right_lane_and_boundaries.first.local_id = -3;

        map_builder.AddFromCatalogue(left_lane_and_boundaries);
        map_builder.AddFromCatalogue(middle_lane_and_boundaries);
        map_builder.AddFromCatalogue(right_lane_and_boundaries);
    }

    static void AddLaneGroupWithNorthEastCounterClockwiseQuarterCircle(RawMapBuilder& map_builder)
    {
        AddArcLaneGroupWithThreeLanes(map_builder, units::angle::radian_t{0.0}, units::angle::radian_t{M_PI * 0.5});
    }

    static void AddLaneGroupWithNorthWestCounterClockwiseQuarterCircle(RawMapBuilder& map_builder)
    {
        AddArcLaneGroupWithThreeLanes(map_builder, units::angle::radian_t{M_PI * 0.5}, units::angle::radian_t{M_PI});
    }

    static void AddLaneGroupWithSouthWestCounterClockwiseQuarterCircle(RawMapBuilder& map_builder)
    {
        AddArcLaneGroupWithThreeLanes(map_builder, units::angle::radian_t{M_PI}, units::angle::radian_t{M_PI * 1.5});
    }

    static void AddLaneGroupWithSouthEastCounterClockwiseQuarterCircle(RawMapBuilder& map_builder)
    {
        AddArcLaneGroupWithThreeLanes(
            map_builder, units::angle::radian_t{M_PI * 1.5}, units::angle::radian_t{M_PI * 2.0});
    }

    static void AddLaneGroupWithStraightRoad2km(RawMapBuilder& map_builder)
    {
        map_builder.AddNewLaneGroup();
        for (auto& lane : LaneCatalogue::ThreeParallelStraightLanes2km())
        {
            map_builder.AddFromCatalogue(lane);
        }
    }
};

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANEGROUPCATALOGUE_H
