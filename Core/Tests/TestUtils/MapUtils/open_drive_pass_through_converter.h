/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>

namespace gtgen::core::test_utils
{
using UnderlyingMapCoordinate =
    std::variant<mantle_api::OpenDriveLanePosition, mantle_api::OpenDriveRoadPosition, mantle_api::LatLonPosition>;

class PassThroughCoordinateConverter
    : public environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>
{
  public:
    using environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>::Convert;

    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(const UnderlyingMapCoordinate& coord) const override
    {
        auto lane_pos = std::get<mantle_api::OpenDriveLanePosition>(coord);
        return mantle_api::Vec3<units::length::meter_t>{
            lane_pos.s_offset, lane_pos.t_offset, units::length::meter_t(2)};
    }

    std::optional<UnderlyingMapCoordinate> Convert(const mantle_api::Vec3<units::length::meter_t>& coord) const override
    {
        return mantle_api::OpenDriveLanePosition{"0", 0, coord.x, coord.y};
    }

    mantle_api::Orientation3<units::angle::radian_t> GetLaneOrientation(
        const mantle_api::OpenDriveLanePosition& open_drive_lane_position) const override
    {
        return {};
    }

    mantle_api::Orientation3<units::angle::radian_t> GetRoadOrientation(
        const mantle_api::OpenDriveRoadPosition& open_drive_road_position) const override
    {
        return {};
    }
};

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H
