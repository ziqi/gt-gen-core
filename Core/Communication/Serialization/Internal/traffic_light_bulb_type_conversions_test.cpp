/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/traffic_light_bulb_type_conversions.h"

#include <gtest/gtest.h>

#include <utility>

namespace gtgen::core::communication
{

//
// GtGenToProtoOsiTrafficLightColor
//
class TrafficLightColorConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<environment::map::OsiTrafficLightColor, messages::map::TrafficLightColor>>
{
};

INSTANTIATE_TEST_SUITE_P(
    TrafficLightBulbConverter,
    TrafficLightColorConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::OsiTrafficLightColor, messages::map::TrafficLightColor>>{
        std::make_tuple(environment::map::OsiTrafficLightColor::kUnknown,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_UNKNOWN),
        std::make_tuple(environment::map::OsiTrafficLightColor::kOther,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_OTHER),
        std::make_tuple(environment::map::OsiTrafficLightColor::kRed,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_RED),
        std::make_tuple(environment::map::OsiTrafficLightColor::kYellow,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_YELLOW),
        std::make_tuple(environment::map::OsiTrafficLightColor::kGreen,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_GREEN),
        std::make_tuple(environment::map::OsiTrafficLightColor::kBlue,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_BLUE),
        std::make_tuple(environment::map::OsiTrafficLightColor::kWhite,
                        messages::map::TrafficLightColor::TRAFFIC_LIGHT_COLOR_WHITE)}));

TEST_P(TrafficLightColorConverterTestFixture, GivenTrafficLightColor_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::OsiTrafficLightColor gtgen_type = std::get<0>(GetParam());
    messages::map::TrafficLightColor expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GtGenToProtoOsiTrafficLightColor(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// GtGenToProtoOsiTrafficLightIcon
//
class TrafficLightIconConverterTestFixture
    : public testing::TestWithParam<std::tuple<environment::map::OsiTrafficLightIcon, messages::map::TrafficLightIcon>>
{
};

INSTANTIATE_TEST_SUITE_P(
    TrafficLightBulbConverter,
    TrafficLightIconConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::OsiTrafficLightIcon, messages::map::TrafficLightIcon>>{
        std::make_tuple(environment::map::OsiTrafficLightIcon::kUnknown,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_UNKNOWN),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kOther,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_OTHER),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kNone,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_NONE),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowStraightAhead,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEAD),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowLeft,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWLEFT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowDiagonalLeft,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWDIAGONALLEFT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowStraightAheadLeft,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADLEFT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowRight,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWRIGHT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowDiagonalRight,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWDIAGONALRIGHT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowStraightAheadRight,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADRIGHT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowLeftRight,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWLEFTRIGHT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowDown,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWDOWN),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowDownLeft,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWDOWNLEFT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowDownRight,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWDOWNRIGHT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kArrowCross,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_ARROWCROSS),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kPedestrian,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_PEDESTRIAN),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kWalk,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_WALK),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kDontWalk,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_DONTWALK),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kBicycle,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_BICYCLE),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kPedestrianAndBicycle,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_PEDESTRIANANDBICYCLE),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kCountdownSeconds,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_COUNTDOWNSECONDS),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kCountdownPercent,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_COUNTDOWNPERCENT),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kTram,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_TRAM),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kBus,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_BUS),
        std::make_tuple(environment::map::OsiTrafficLightIcon::kBusAndTram,
                        messages::map::TrafficLightIcon::TRAFFIC_LIGHT_ICON_BUSANDTRAM)}));

TEST_P(TrafficLightIconConverterTestFixture, GivenTrafficLightIcon_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::OsiTrafficLightIcon gtgen_type = std::get<0>(GetParam());
    messages::map::TrafficLightIcon expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GtGenToProtoOsiTrafficLightIcon(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// GtGenToProtoOsiTrafficLightMode
//
class TrafficLightModeConverterTestFixture
    : public testing::TestWithParam<std::tuple<environment::map::OsiTrafficLightMode, messages::map::TrafficLightMode>>
{
};

INSTANTIATE_TEST_SUITE_P(
    TrafficLightBulbConverter,
    TrafficLightModeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::OsiTrafficLightMode, messages::map::TrafficLightMode>>{
        std::make_tuple(environment::map::OsiTrafficLightMode::kUnknown,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_UNKNOWN),
        std::make_tuple(environment::map::OsiTrafficLightMode::kOther,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_OTHER),
        std::make_tuple(environment::map::OsiTrafficLightMode::kOff,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_OFF),
        std::make_tuple(environment::map::OsiTrafficLightMode::kConstant,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_CONSTANT),
        std::make_tuple(environment::map::OsiTrafficLightMode::kFlashing,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_FLASHING),
        std::make_tuple(environment::map::OsiTrafficLightMode::kCounting,
                        messages::map::TrafficLightMode::TRAFFIC_LIGHT_MODE_COUNTING)}));

TEST_P(TrafficLightModeConverterTestFixture, GivenTrafficLightMode_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::OsiTrafficLightMode gtgen_type = std::get<0>(GetParam());
    messages::map::TrafficLightMode expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GtGenToProtoOsiTrafficLightMode(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace gtgen::core::communication
