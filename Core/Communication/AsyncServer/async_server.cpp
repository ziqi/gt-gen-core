/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/AsyncServer/async_server.h"

#include "Core/Communication/AsyncServer/network_session.h"
#include "Core/Communication/Common/exceptions.h"
#include "Core/Communication/Dispatchers/dispatcher_factory.h"
#include "Core/Service/Logging/logging.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <future>
#include <memory>
#include <thread>
#include <utility>

namespace gtgen::core::communication
{

class AsyncServer::Impl
{
  public:
    Impl() = default;
    Impl(Impl&& other) = delete;
    Impl(const Impl&) = delete;
    Impl& operator=(const Impl&) = delete;
    Impl& operator=(Impl&& other) = delete;
    ~Impl();

    /// @brief Starts asynchronous service accepting GUI connections, tries to bind to a port in [min_port, max_port]
    /// @param dispatcher_factory Factory to create dispatchers for each accepted connection
    /// @param allow_multiple Enables concurrent connections. By default new connection requests will be queued.
    /// @throws AsyncServerException::AcceptorCreationError, AsyncServerException::ServiceRunError
    void StartAcceptingConnections(std::unique_ptr<DispatcherFactory> dispatcher_factory, bool allow_multiple = false);

    /// @brief Provides the port reserved by the server if StartAcceptingConnections has been called, 0 otherwise.
    std::uint16_t GetReservedPort() const { return reserved_port_; }

    /// @brief Interrupts the server and shuts down its current sessions. The server can be restarted by calling
    /// StartAcceptingConnections() with a new DispatcherFactory again
    void StopServing();

  private:
    void CreateAcceptor();
    void AsyncAccept();
    void RunService();
    void OnConnectionAccepted(const std::shared_ptr<NetworkSession>& network_session,
                              const boost::system::error_code& error_code);
    void DiscardClosedSessions();

    std::thread worker_;
    boost::asio::io_service io_service_{};
    std::unique_ptr<tcp::acceptor> acceptor_{nullptr};
    std::unique_ptr<DispatcherFactory> dispatcher_factory_{nullptr};
    std::atomic_bool run_service_{false};

    // this is just our reference to the sessions, boost::asio keeps its own shared ptrs ..
    // check PR#2583 - issuecomment-1810034
    std::vector<std::shared_ptr<NetworkSession>> network_sessions_{};
    std::mutex session_storage_mutex_;

    bool allow_multiple_connections_{false};

    std::uint16_t reserved_port_{0};
    const std::uint16_t min_port{27000};
    const std::uint16_t max_port{28000};
};

void AsyncServer::Impl::StartAcceptingConnections(std::unique_ptr<DispatcherFactory> dispatcher_factory,
                                                  const bool allow_multiple)
{
    dispatcher_factory_ = std::move(dispatcher_factory);
    allow_multiple_connections_ = allow_multiple;

    CreateAcceptor();
    AsyncAccept();

    // Start the worker (service) that handles tasks from existing connections
    // In theory there could be more than one worker executing read/send in parallel, but let's stick to one first
    run_service_ = true;
    worker_ = std::thread(&AsyncServer::Impl::RunService, this);

    Info("{} server started, listening on port {}.", dispatcher_factory_->GetDispatcherPurpose(), reserved_port_);
}

void AsyncServer::Impl::CreateAcceptor()
{
    for (std::uint16_t port = min_port; port <= max_port; port++)
    {
        try
        {
            acceptor_ = std::make_unique<tcp::acceptor>(io_service_, tcp::endpoint{tcp::v4(), port});
            // port is now reserved for us
            reserved_port_ = acceptor_->local_endpoint().port();
            break;
        }
        catch (boost::system::system_error& error)
        {
            if (port == max_port)
            {
                LogAndThrow(exception::AcceptorCreationError(
                    "Couldn't create {} server for any port in range [{};{}]! Last occurred error: {}",
                    dispatcher_factory_->GetDispatcherPurpose(),
                    min_port,
                    max_port,
                    error.what()));
            }
        }
    }
}

void AsyncServer::Impl::AsyncAccept()
{
    std::scoped_lock lock(session_storage_mutex_);

    DiscardClosedSessions();
    // yes :(  check PR#2583 - issuecomment-1810034
    auto network_session =
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-static-cast-downcast)
        std::make_shared<NetworkSession>(static_cast<boost::asio::io_context&>(acceptor_->get_executor().context()),
                                         dispatcher_factory_->GetDispatcherPurpose());

    acceptor_->async_accept(
        network_session->GetSocket(),
        // NOLINTNEXTLINE(modernize-avoid-bind) - accepted
        boost::bind(&AsyncServer::Impl::OnConnectionAccepted, this, network_session, boost::asio::placeholders::error));

    network_sessions_.push_back(network_session);
}

void AsyncServer::Impl::DiscardClosedSessions()
{
    network_sessions_.erase(std::remove_if(network_sessions_.begin(),
                                           network_sessions_.end(),
                                           [](auto& session) { return !session->GetSocket().is_open(); }),
                            network_sessions_.end());
}

void AsyncServer::Impl::RunService()
{
    while (run_service_)
    {
        io_service_.run();
        io_service_.reset();

        if (run_service_ && !allow_multiple_connections_)
        {
            // required because OnConnectionAccepted didn't start accepting again,
            // when only one connection is allowed
            AsyncAccept();
        }
    }

    acceptor_->close();
    Info("{} server shut down.", dispatcher_factory_->GetDispatcherPurpose());
}

void AsyncServer::Impl::OnConnectionAccepted(const std::shared_ptr<NetworkSession>& network_session,
                                             const boost::system::error_code& error_code)
{
    if (error_code.value() != 0)
    {
        LogAndThrow(
            exception::ServiceRunError("Error while accepting connection, error message: {}", error_code.message()));
    }

    network_session->Start(*dispatcher_factory_);

    if (allow_multiple_connections_)
    {
        AsyncAccept();
    }
}

void AsyncServer::Impl::StopServing()
{
    // nothing to stop
    if (!run_service_)
    {
        return;
    }

    run_service_ = false;
    io_service_.stop();

    {
        std::scoped_lock lock(session_storage_mutex_);
        for (auto& session : network_sessions_)
        {
            session->ShutDown();
        }
    }

    if (worker_.joinable())
    {
        Info("Waiting for {} server shutdown...", dispatcher_factory_->GetDispatcherPurpose());
        auto future = std::async(std::launch::async, &std::thread::join, &worker_);
        if (future.wait_for(std::chrono::seconds(2)) == std::future_status::timeout)
        {
            Info("Terminating {} server.", dispatcher_factory_->GetDispatcherPurpose());
            worker_.~thread();
        }
    }

    network_sessions_.clear();
    reserved_port_ = 0;
}

AsyncServer::Impl::~Impl()
{
    StopServing();
}

AsyncServer::AsyncServer()
{
    impl_ = std::make_unique<AsyncServer::Impl>();
}

AsyncServer::~AsyncServer() = default;

void AsyncServer::StartAcceptingConnections(std::unique_ptr<DispatcherFactory> dispatcher_factory,
                                            const bool allow_multiple)
{
    impl_->StartAcceptingConnections(std::move(dispatcher_factory), allow_multiple);
}

void AsyncServer::StopServing()
{
    impl_->StopServing();
}

std::uint16_t AsyncServer::GetReservedPort() const
{
    return impl_->GetReservedPort();
}

}  // namespace gtgen::core::communication
