/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_COMMON_IDISPATCHER_H
#define GTGEN_CORE_COMMUNICATION_COMMON_IDISPATCHER_H

#include <functional>
#include <memory>

namespace gtgen::core::messages::gui
{
class MessageWrapper;
}

namespace gtgen::core::communication
{

struct MessageContainer;

class IDispatcher
{
  public:
    IDispatcher() = default;
    virtual ~IDispatcher() = default;
    IDispatcher(const IDispatcher&) = delete;
    IDispatcher& operator=(const IDispatcher&) = delete;
    IDispatcher(IDispatcher&&) = default;
    IDispatcher& operator=(IDispatcher&&) = default;

    /// @brief Prepares dispatcher for processing messages and triggering answers
    /// @param async_send Callback to the AsyncSend(std::unique_ptr<MessageContainer>) method
    /// @attention async_send should be called EXACTLY ONCE as reply to a SUCCESSFULLY processed message
    virtual void Initialize(std::function<void(std::unique_ptr<MessageContainer>)> async_send) = 0;

    /// @brief This method shall process a received message and can trigger any consequences. Must not throw!
    /// @param received_message A valid MessageWrapper filled with valid or invalid data.
    /// @return True if the request has been processed and an answer has been sent. May only be false if no
    /// answer could be sent, e.g. due to a malformed or not supported request!
    virtual bool ProcessMessage(const messages::gui::MessageWrapper& received_message) noexcept = 0;

    /// @brief Allows for proper dispatcher cleanup.
    /// @attention Should be thread safe synchronized with ProcessMessage, so that Shutdown cannot happen while
    /// ProcessMessage is executed by a server worker thread!!
    virtual void ShutDown() = 0;
};

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_COMMON_IDISPATCHER_H
