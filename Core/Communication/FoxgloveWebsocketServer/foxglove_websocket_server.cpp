/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"

#include "Core/Service/Logging/logging.h"

#include <foxglove/websocket/common.hpp>
#include <foxglove/websocket/server_interface.hpp>
#include <foxglove/websocket/websocket_notls.hpp>
#include <google/protobuf/descriptor.pb.h>
#include <osi_groundtruth.pb.h>

#include <chrono>
#include <optional>
#include <queue>
#include <unordered_set>

namespace foxglove
{
template <>
// NOLINTNEXTLINE(readability-identifier-naming)
void Server<WebSocketNoTls>::setupTlsHandler()
{
}
}  // namespace foxglove

namespace gtgen::core::communication
{

namespace detail
{

// Writes the FileDescriptor of this descriptor and all transitive dependencies
// to a string, for use as a channel schema.
static std::string SerializeFdSet(const google::protobuf::Descriptor* top_level_descriptor)
{
    google::protobuf::FileDescriptorSet fd_set;
    std::queue<const google::protobuf::FileDescriptor*> to_add;
    to_add.push(top_level_descriptor->file());
    std::unordered_set<std::string> seen_dependencies;
    while (!to_add.empty())
    {
        const google::protobuf::FileDescriptor* next = to_add.front();
        to_add.pop();
        next->CopyTo(fd_set.add_file());
        for (int i = 0; i < next->dependency_count(); ++i)
        {
            const auto& dep = next->dependency(i);
            if (seen_dependencies.find(dep->name()) == seen_dependencies.end())
            {
                seen_dependencies.insert(dep->name());
                to_add.push(dep);
            }
        }
    }
    return fd_set.SerializeAsString();
}

// Adapted from:
// https://gist.github.com/tomykaira/f0fd86b6c73063283afe550bc5d77594
// https://github.com/protocolbuffers/protobuf/blob/01fe22219a0312b178a265e75fe35422ea6afbb1/src/google/protobuf/compiler/csharp/csharp_helpers.cc#L346
static std::string Base64Encode(std::string_view input)
{
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,readability-identifier-naming)
    constexpr const char ALPHABET[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    std::string result;
    // Every 3 bytes of data yields 4 bytes of output
    result.reserve((input.size() + (3 - 1 /* round up */)) / 3 * 4);

    // Unsigned values are required for bit-shifts below to work properly
    const unsigned char* data =
        reinterpret_cast<const unsigned char*>(input.data());  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)

    size_t i = 0;
    for (; i + 2 < input.size(); i += 3)
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
        result.push_back(ALPHABET[data[i] >> 2]);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
        result.push_back(ALPHABET[((data[i] & 0b11) << 4) | (data[i + 1] >> 4)]);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
        result.push_back(ALPHABET[((data[i + 1] & 0b1111) << 2) | (data[i + 2] >> 6)]);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
        result.push_back(ALPHABET[data[i + 2] & 0b111111]);
    }
    switch (input.size() - i)
    {
        case 2:
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
            result.push_back(ALPHABET[data[i] >> 2]);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
            result.push_back(ALPHABET[((data[i] & 0b11) << 4) | (data[i + 1] >> 4)]);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
            result.push_back(ALPHABET[(data[i + 1] & 0b1111) << 2]);
            result.push_back('=');
            break;
        case 1:
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
            result.push_back(ALPHABET[data[i] >> 2]);
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index,cppcoreguidelines-pro-bounds-pointer-arithmetic,hicpp-signed-bitwise)
            result.push_back(ALPHABET[(data[i] & 0b11) << 4]);
            result.push_back('=');
            result.push_back('=');
            break;
    }

    return result;
}

static std::uint64_t NanoSecondsSinceEpoch()
{
    return static_cast<std::uint64_t>(
        std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch())
            .count());
}

}  // namespace detail

FoxgloveWebsocketServer::~FoxgloveWebsocketServer()  // NOLINT(bugprone-exception-escape)
{
    Stop();
}

void FoxgloveWebsocketServer::Stop()
{
    if (is_running_)
    {
        server_->removeChannels({channel_id_});
        server_->stop();
    }
}

void FoxgloveWebsocketServer::Start()
{
    is_running_ = true;
    const auto log_handler = [this](foxglove::WebSocketLogLevel log_level, char const* msg) {
        std::lock_guard<std::mutex> guard(log_msg_mutex_);

        if (log_level == foxglove::WebSocketLogLevel::Error)
        {
            Error(msg);
        }
        else if (log_level == foxglove::WebSocketLogLevel::Warn)
        {
            Warn(msg);
        }
        else
        {
            Info(msg);
        }
    };

    foxglove::ServerOptions server_options;
    server_ = std::make_unique<foxglove::Server<foxglove::WebSocketNoTls>>(
        "OSI Ground Truth Provider", log_handler, server_options);

    foxglove::ServerHandlers<foxglove::ConnHandle> hdlrs;
    hdlrs.subscribeHandler = [&](foxglove::ChannelId chanId, foxglove::ConnHandle clientHandle) {
        const auto clientStr = server_->remoteEndpointString(clientHandle);
        Info("Client {} subscribed to channel {}", clientStr, chanId);
    };
    hdlrs.unsubscribeHandler = [&](foxglove::ChannelId chanId, foxglove::ConnHandle clientHandle) {
        const auto clientStr = server_->remoteEndpointString(clientHandle);
        Info("Client {} unsubscribed from channel {}", clientStr, chanId);
    };
    server_->setHandlers(std::move(hdlrs));

    server_->start("localhost", 8765);
    const auto channel_ids =
        server_->addChannels({{"/sim/groundtruth_topic",
                               "protobuf",
                               osi3::GroundTruth::descriptor()->full_name(),
                               detail::Base64Encode(detail::SerializeFdSet(osi3::GroundTruth::descriptor())),
                               std::nullopt}});
    channel_id_ = channel_ids.front();
}

void FoxgloveWebsocketServer::Send(const osi3::SensorView& sensor_view)
{
    const auto& ground_truth = sensor_view.global_ground_truth();

    const auto serialized_ground_truth = ground_truth.SerializeAsString();
    const auto now = detail::NanoSecondsSinceEpoch();

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    server_->broadcastMessage(
        channel_id_,
        now,
        reinterpret_cast<const std::uint8_t*>(  // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
            serialized_ground_truth.data()),
        serialized_ground_truth.size());
}

}  // namespace gtgen::core::communication
