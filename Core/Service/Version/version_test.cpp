/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Version/version.h"

#include <gtest/gtest.h>

namespace gtgen::core
{

TEST(VersionTest, GivenGetGtCoreVersion_WhenCallingTheFunction_ThenLinkingShallWorkAndNonEmptyStringReturned)
{
    EXPECT_FALSE(GetGtGenCoreVersion().empty());
}

TEST(VersionTest, GivenGetOsiVersion_WhenCallingTheFunction_ThenLinkingShallWorkAndNonEmptyStringReturned)
{
    auto expected_at_least_version = Version{3, 5, 0};
    EXPECT_TRUE(GetOsiVersion() >= expected_at_least_version);
}

}  // namespace gtgen::core
