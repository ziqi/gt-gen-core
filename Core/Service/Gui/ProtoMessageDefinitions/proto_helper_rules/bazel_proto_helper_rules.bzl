def _get_only_source_files_impl(ctx):
    gen_files_extensions = ["h", "cc"]
    source_files = [f for f in ctx.files.srcs if f.extension in gen_files_extensions]

    if not source_files:
        fail("Protohelper: No source files with the specified extensions found.")

    return DefaultInfo(files = depset(source_files))

get_only_source_files = rule(
    implementation = _get_only_source_files_impl,
    attrs = {
        "srcs": attr.label_list(allow_files = True, mandatory = True),
    },
)
