/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGS_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGS_H

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/UserSettings/user_settings_enums.h"
#include "Core/Service/UserSettings/user_settings_list.h"
#include "Core/Service/UserSettings/user_settings_path_list.h"

#include <fmt/format.h>

#include <string>

namespace gtgen::core::service::user_settings
{

/// @brief Represents the FileLogging section in the UserSettings ini file
struct FileLogging
{
    std::string log_level{"Debug"};
};

/// @brief Represents the GroundTruth section in the UserSettings ini file
struct GroundTruth
{
    double lane_marking_distance_in_m{0.4};
    double lane_marking_downsampling_epsilon{0.01};
    bool lane_marking_downsampling{true};
    bool allow_invalid_lane_locations{true};
};

/// @brief Represents the HostVehicle section in the UserSettings ini file
struct HostVehicle
{
    HostControlMode host_control_mode{HostControlMode::kExternal};
    double time_scale{1.0};
    bool blocking_communication{false};  // ?
    bool recovery_mode{false};           // ?
};

/// @brief Represents the Map section in the UserSettings ini file
struct Map
{
    bool include_obstacles{false};
};

/// @brief Represents the MapChunking section in the UserSettings ini file
struct MapChunking
{
    std::uint16_t chunk_grid_size{50};
    std::uint16_t cells_per_direction{2};
};

/// @brief Represents the UserDirectories section in the UserSettings ini file
struct UserDirectories
{
    UserSettingsPathList scenarios{};
    UserSettingsPathList maps{};
    UserSettingsPathList plugins{};
};

struct Clock
{
    bool use_system_clock{false};
};

struct Foxglove
{
    bool websocket_server{false};
};

struct TraceRecording
{
    bool enabled{false};
    fs::path trace_file_path{};
};

struct SimulationResults
{
    fs::path output_directory_path{};
    bool log_cyclics{false};
};

/// @brief Represents the UserSettings ini file
struct UserSettings
{
    FileLogging file_logging;
    GroundTruth ground_truth;
    HostVehicle host_vehicle;
    Map map;
    MapChunking map_chunking;
    UserDirectories user_directories;
    Clock clock;
    Foxglove foxglove;
    std::string gtgen_log_directory;  // @todo temporary location for this setting
    TraceRecording trace_recording;
    SimulationResults simulation_results;
};

}  // namespace gtgen::core::service::user_settings

///////////////////////////////////////////////////////////////////////////////
// In the section below: The boilerplate code for the fmt printing

template <>
struct fmt::formatter<gtgen::core::service::user_settings::FileLogging>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::FileLogging& file_logging, FormatContext& ctx)
    {
        return format_to(ctx.out(), "LogLevel: {}", file_logging.log_level);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::GroundTruth>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::GroundTruth& ground_truth, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "LaneMarkingDistance: {}\nSimplifyLaneMarkingsEpsilon: "
                         "{}\nSimplifyLaneMarkings: {}"
                         "\nAllowInvalidLaneLocations: {}",
                         ground_truth.lane_marking_distance_in_m,
                         ground_truth.lane_marking_downsampling_epsilon,
                         ground_truth.lane_marking_downsampling,
                         ground_truth.allow_invalid_lane_locations);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::HostVehicle>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::HostVehicle& host_vehicle, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "HostControlMode: {}\nTimeScale: {}\nBlockingCommunication: {}\nRecoveryMode: {}",
                         host_vehicle.host_control_mode,
                         host_vehicle.time_scale,
                         host_vehicle.blocking_communication,
                         host_vehicle.recovery_mode);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::Map>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::Map& map, FormatContext& ctx)
    {
        return format_to(ctx.out(), "IncludeObstacles: {}", map.include_obstacles);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::MapChunking>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::MapChunking& map_chunking, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "ChunkGridSize: {}\nCellsPerDirection: {}",
                         map_chunking.chunk_grid_size,
                         map_chunking.cells_per_direction);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::UserDirectories>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::UserDirectories& user_directories, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "Scenarios: '{}'\nMaps: '{}'\nPlugins: '{}'",
                         user_directories.scenarios,
                         user_directories.maps,
                         user_directories.plugins);
    }
};

template <>
struct fmt::formatter<gtgen::core::service::user_settings::UserSettings>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::service::user_settings::UserSettings& user_settings, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "[FileLogging]\n{}\n\n"
                         "[GroundTruth]\n{}\n\n"
                         "[HostVehicle]\n{}\n\n"
                         "[Map]\n{}\n\n"
                         "[MapChunking]\n{}\n\n"
                         "[UserDirectories]\n{}\n\n",
                         user_settings.file_logging,
                         user_settings.ground_truth,
                         user_settings.host_vehicle,
                         user_settings.map,
                         user_settings.map_chunking,
                         user_settings.user_directories);
    }
};

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGS_H
