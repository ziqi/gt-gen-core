/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_H
#define GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_H

#include "Core/Service/FileSystem/filesystem.h"

#include <osi_sensorview.pb.h>

#include <stdint.h>

#include <fstream>
#include <string>

namespace gtgen::core::service
{

class TraceWriter
{
  public:
    enum class ExtensionType : uint8_t
    {
        kTXT,
        kOSI,
        kTXTH
    };

    static constexpr std::string_view TRACE_SEPARATOR = "$$__$$";
    static constexpr std::uint32_t UINT_LENGTH = 4;

    TraceWriter(const gtgen::core::fs::path& trace_path);
    ~TraceWriter();

    gtgen::core::fs::path GetTraceFile() const { return trace_path_; }
    void WriteToTraceFile(const osi3::SensorView& sensor_view);

  private:
    void SetFileExtension(const gtgen::core::fs::path& trace_path);
    void RemoveFileIfExist(const gtgen::core::fs::path& trace_file);
    gtgen::core::fs::path trace_path_{};
    std::ofstream trace_{};
    ExtensionType extension_type_{};
    bool first_call_{true};
};

}  // namespace gtgen::core::service

#endif  // GTGEN_CORE_SERVICE_TRACEWRITER_TRACE_WRITER_H
