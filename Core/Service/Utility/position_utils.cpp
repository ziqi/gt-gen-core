/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/position_utils.h"

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/floating_point_helper.h>

#include <cmath>

namespace gtgen::core::service::utility
{

mantle_api::Vec3<units::length::meter_t> GetVerticalShiftedPosition(
    mantle_api::Vec3<units::length::meter_t> initial_position,
    const mantle_api::BoundingBox& bounding_box)
{
    // TODO: Can be removed, if origin of entity coordinate system at bottom of bounding box is considered
    const mantle_api::Vec3<units::length::meter_t> vertical_shift{
        units::length::meter_t{0.0}, units::length::meter_t{0.0}, bounding_box.dimension.height * 0.5};
    return initial_position + vertical_shift;
}

mantle_api::Vec3<units::length::meter_t> GetLateralShiftedPosition(
    mantle_api::Vec3<units::length::meter_t> initial_position,
    mantle_api::Vec3<units::length::meter_t> direction,
    mantle_api::Vec3<units::length::meter_t> lane_normal,
    units::length::meter_t t_offset)
{
    const auto normalized_left = service::glmwrapper::Normalize(service::glmwrapper::Cross(direction, lane_normal));
    const auto lateral_shift = normalized_left * t_offset();

    return initial_position - lateral_shift;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> GetLineIntersection2d(
    const mantle_api::Vec3<units::length::meter_t>& line1_p1,
    const mantle_api::Vec3<units::length::meter_t>& line1_p2,
    const mantle_api::Vec3<units::length::meter_t>& line2_p1,
    const mantle_api::Vec3<units::length::meter_t>& line2_p2)
{
    const auto x1 = line1_p1.x();
    const auto x2 = line1_p2.x();
    const auto x3 = line2_p1.x();
    const auto x4 = line2_p2.x();

    const auto y1 = line1_p1.y();
    const auto y2 = line1_p2.y();
    const auto y3 = line2_p1.y();
    const auto y4 = line2_p2.y();

    auto numerator_for_x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4));
    auto numerator_for_y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4));
    auto denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    if (mantle_api::AlmostEqual(denominator, 0.0, MANTLE_API_DEFAULT_EPS, true))
    {
        return std::nullopt;
    }

    return mantle_api::Vec3<units::length::meter_t>{units::length::meter_t(numerator_for_x / denominator),
                                                    units::length::meter_t(numerator_for_y / denominator),
                                                    units::length::meter_t(0.0)};
}

std::optional<mantle_api::Vec3<units::length::meter_t>> GetLineToLineSegmentsIntersection2D(
    const mantle_api::Vec3<units::length::meter_t>& line_p1,
    const mantle_api::Vec3<units::length::meter_t>& line_p2,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& line_segments)
{
    if (line_segments.empty())
    {
        return std::nullopt;
    }

    const auto line_direction = line_p2 - line_p1;
    const auto line_left_vector =
        mantle_api::Vec3<units::length::meter_t>{-line_direction.y, line_direction.x, line_direction.z};
    auto current_point = line_segments[0];
    const auto start_direction = current_point - line_p1;
    const auto start_dot_product = service::glmwrapper::Dot(line_left_vector, start_direction);

    for (std::size_t i = 1; i < line_segments.size(); ++i)
    {
        auto previous_point = current_point;
        current_point = line_segments[i];
        const auto current_direction = current_point - line_p1;
        const auto current_dot_product = service::glmwrapper::Dot(line_left_vector, current_direction);
        if (start_dot_product * current_dot_product <= 0)  // dot product sign changed
        {
            return GetLineIntersection2d(previous_point, current_point, line_p1, line_p2);
        }
    }
    return std::nullopt;
}

bool IsRight2D(const mantle_api::Vec3<units::length::meter_t>& vec1,
               const mantle_api::Vec3<units::length::meter_t>& vec2)
{
    // Using the cross-product of two vectors in the XY plane, to determine the reference location of two
    // cross-producted vectors. vec3 = vec1 x vec2. Apply right-hand rule, if vec3.z > 0 then vec1 is on the right side
    // of vec2, vice-versa if vec3.z < 0
    return (vec1.x.value() * vec2.y.value() - vec1.y.value() * vec2.x.value()) > 0;
}

double GetDistance2D(const mantle_api::Vec3<units::length::meter_t>& vec1,
                     const mantle_api::Vec3<units::length::meter_t>& vec2)
{
    return std::sqrt(std::pow(vec1.x.value() - vec2.x.value(), 2) + std::pow(vec1.y.value() - vec2.y.value(), 2));
}

double GetDistance3D(const mantle_api::Vec3<units::length::meter_t>& vec1,
                     const mantle_api::Vec3<units::length::meter_t>& vec2)
{
    return std::sqrt(std::pow((vec1.x - vec2.x).value(), 2) + std::pow((vec1.y - vec2.y).value(), 2) +
                     std::pow((vec1.z - vec2.z).value(), 2));
}

bool IsAlmostEqual(const mantle_api::Vec3<units::length::meter_t>& vec1,
                   const mantle_api::Vec3<units::length::meter_t>& vec2)
{
    return (
        mantle_api::AlmostEqual(vec1.x, vec2.x, static_cast<units::length::meter_t>(MANTLE_API_DEFAULT_EPS), true) &&
        mantle_api::AlmostEqual(vec1.y, vec2.y, static_cast<units::length::meter_t>(MANTLE_API_DEFAULT_EPS), true) &&
        mantle_api::AlmostEqual(vec1.z, vec2.z, static_cast<units::length::meter_t>(MANTLE_API_DEFAULT_EPS), true));
}

}  // namespace gtgen::core::service::utility
