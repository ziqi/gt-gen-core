/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/math_utils.h"

#include <gtest/gtest.h>

#include <array>
#include <cstdint>

namespace gtgen::core::service::utility
{

TEST(GetSignTest, GivenPositiveNumber_WhenGetSign_ThenReturnPositiveSign)
{
    const double input_value = 100.0;
    const std::uint16_t expected_value = 1;

    EXPECT_DOUBLE_EQ(GetSign(input_value), expected_value);
}

TEST(GetSignTest, GivenNegativeNumber_WhenGetSign_ThenReturnNegativeSign)
{
    const double input_value = -100.0;
    const std::int16_t expected_value = -1;

    EXPECT_DOUBLE_EQ(GetSign(input_value), expected_value);
}

TEST(AvoidZeroTest, GivenNonZeroPositiveNumber_WhenCallAvoidZero_ThenReturnTheSameNumber)
{
    const double input_value = 100.0;

    EXPECT_DOUBLE_EQ(AvoidZero(input_value), input_value);
}

TEST(AvoidZeroTest, GivenNonZeroNegativeNumber_WhenCallAvoidZero_ThenReturnTheSameNumber)
{
    const double input_value = -100.0;

    EXPECT_DOUBLE_EQ(AvoidZero(input_value), input_value);
}

TEST(AvoidZeroTest, GivenZeroValue_WhenCallAvoidZero_ThenDeathDetected)
{
    const double input_value = 0.0;
    const double small_number = 1.0E-8;

    EXPECT_DOUBLE_EQ(AvoidZero(input_value), GetSign(input_value) * small_number);
}

TEST(CalculateOtherLegInARectangledTriangleTest,
     GivenHypotenuseAndOneLeg_WhenCalculateOtherLegInARectangledTriangle_ThenCorrectValueReturned)
{
    const double a = 3.0;
    const double c = 5.0;
    const double expected_value = 4.0;

    EXPECT_DOUBLE_EQ(CalculateOtherLegInARectangledTriangle(c, a), expected_value);
}

TEST(CalculateOtherLegInARectangledTriangleTest,
     GivenHypotenuseLongerOneLeg_WhenCalculateOtherLegInARectangledTriangle_ThenHypotenuseValueReturned)
{
    const double a = 5.0;
    const double c = 3.0;
    const double expected_value = 3.0;

    EXPECT_DOUBLE_EQ(CalculateOtherLegInARectangledTriangle(c, a), expected_value);
}

struct AbsAngleTestType
{
    units::angle::radian_t alpha;
    units::angle::radian_t beta;
    units::angle::radian_t expected_abs_angle;
};

const std::array<AbsAngleTestType, 5> kAbsAngleTestVector{{
    {units::angle::radian_t(0.0), units::angle::radian_t(0.0), units::angle::radian_t(0.0)},
    {units::angle::radian_t(M_PI_2), units::angle::radian_t(M_PI_4), units::angle::radian_t(M_PI_4)},
    {units::angle::radian_t(M_PI_4), units::angle::radian_t(M_PI_2), units::angle::radian_t(M_PI_4)},
    {units::angle::radian_t(3 * M_PI_2), units::angle::radian_t(7 * M_PI_4), units::angle::radian_t(M_PI_4)},
    {units::angle::radian_t(7 * M_PI_4), units::angle::radian_t(3 * M_PI_2), units::angle::radian_t(M_PI_4)},

}};

using AbsAngleTest = testing::TestWithParam<AbsAngleTestType>;

INSTANTIATE_TEST_SUITE_P(MathUtils, AbsAngleTest, testing::ValuesIn(kAbsAngleTestVector));

TEST_P(AbsAngleTest, GivenTwoAngles_WhenComputeAbsAngle_ThenPositiveRadian)
{
    const auto test_vector = GetParam();

    const auto abs_angle{AbsAngle(test_vector.alpha, test_vector.beta)};
    EXPECT_EQ(abs_angle, test_vector.expected_abs_angle);
}

}  // namespace gtgen::core::service::utility
