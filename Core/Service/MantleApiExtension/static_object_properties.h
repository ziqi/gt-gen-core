/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_STATICOBJECTPROPERTIES_H
#define GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_STATICOBJECTPROPERTIES_H

#include "Core/Service/Osi/road_marking_types.h"
#include "Core/Service/Osi/stationary_object_types.h"
#include "Core/Service/Osi/supplementary_sign_types.h"
#include "Core/Service/Osi/traffic_light_types.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <string>
#include <vector>

namespace mantle_ext
{

struct StationaryObjectProperties : public mantle_api::StaticObjectProperties
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> base_polygon{};
    osi::StationaryObjectEntityMaterial material{osi::StationaryObjectEntityMaterial::kOther};
};

struct TrafficSignProperties : public mantle_api::StaticObjectProperties
{
    osi::OsiTrafficSignType sign_type{};
    osi::OsiTrafficSignVariability variability{};
    osi::OsiTrafficSignValueUnit unit{};
    osi::OsiTrafficSignDirectionScope direction{};

    std::string text{};
    double value{0.0};
};

struct SupplementarySignProperties : public mantle_api::StaticObjectProperties
{
    osi::OsiSupplementarySignType supplementary_sign_type{};
    osi::OsiTrafficSignVariability variability{};
    osi::OsiTrafficSignValueUnit unit{};
    std::vector<osi::OsiSupplementarySignActor> actors{};

    std::string text{};
    double value{0.0};
};

struct TrafficLightBulbProperties : public mantle_api::StaticObjectProperties
{
    mantle_api::UniqueId id{0};
    mantle_api::Vec3<units::dimensionless::scalar_t> position_offset_coeffs{};

    mantle_api::TrafficLightBulbColor color{mantle_api::TrafficLightBulbColor::kUnknown};
    osi::OsiTrafficLightIcon icon{osi::OsiTrafficLightIcon::kUnknown};
    mantle_api::TrafficLightBulbMode mode{mantle_api::TrafficLightBulbMode::kUnknown};
    double count{0};
};

struct TrafficLightProperties : public mantle_api::StaticObjectProperties
{
    std::vector<TrafficLightBulbProperties> bulbs{};
};

struct RoadMarkingProperties : public mantle_api::StaticObjectProperties
{
    osi::OsiRoadMarkingsType marking_type{};
    osi::OsiRoadMarkingColor marking_color{};

    osi::OsiTrafficSignType sign_type{};
    std::string text{};
    double value{0.0};
    osi::OsiTrafficSignValueUnit unit{};
};

}  // namespace mantle_ext

#endif  // GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_STATICOBJECTPROPERTIES_H
