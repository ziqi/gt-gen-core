/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"

#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/internal_controller_factory.h"
#include "Core/Environment/Controller/external_controller_config_converter.h"
#include "Core/Environment/DataStore/utils.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Service/Version/version.h"

namespace gtgen::core::environment::api
{
namespace
{
const std::string kCoreInformationSchemaVersion{"0.0.3"};
const std::string kCoreInformationFilename = "CoreInformation.json";
constexpr units::length::meter_t longitudinal_distance_between_traffic_swarm_vehicles(50.0);

}  // namespace

GtGenEnvironment::GtGenEnvironment(const service::user_settings::UserSettings& user_settings,
                                   const mantle_api::Time& step_size,
                                   const std::uint32_t seed,
                                   const std::int32_t run_number,
                                   const std::int32_t total_runs)
    : GtGenEnvironment(user_settings,
                       step_size,
                       datastore::OutputGenerator::Settings{user_settings.simulation_results.output_directory_path,
                                                            run_number,
                                                            total_runs},
                       seed)
{
}

GtGenEnvironment::GtGenEnvironment(const service::user_settings::UserSettings& user_settings,
                                   const mantle_api::Time& step_size,
                                   const datastore::OutputGenerator::Settings& output_settings,
                                   const std::uint32_t seed)
    : user_settings_{user_settings},
      step_size_{step_size},
      lane_assignment_service_{entity_repository_, active_controller_repository_},
      output_generator_{output_settings},
      seed_{seed}
{
    if (IsDefaultRandomSeed(seed_))
    {
        Info("Random Seed not set or set as default, value: {}", seed_);
    }

    if (user_settings_.simulation_results.log_cyclics)
    {
        Info("Cyclics will be logged and saved to the following directory: {}",
             user_settings_.simulation_results.output_directory_path.string());
    }
}

GtGenEnvironment::~GtGenEnvironment()
{
    output_generator_.WriteCoreInformation(GenerateCoreInformation());

    if (user_settings_.simulation_results.log_cyclics)
    {
        output_generator_.FinishRun();
    }
}

void GtGenEnvironment::Init()
{
    gtgen_map_ = std::make_unique<map::GtGenMap>();

    const auto& chunking_settings = user_settings_.map_chunking;
    sensor_view_builder_ =
        std::make_unique<proto_groundtruth::SensorViewBuilder>(*gtgen_map_, step_size_, chunking_settings);

    traffic_command_builder_ = std::make_unique<traffic_command::TrafficCommandBuilder>();
    host_vehicle_interface_ = std::make_unique<host::HostVehicleInterface>();
    host_vehicle_model_ = std::make_unique<host::HostVehicleModel>();
    lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*gtgen_map_);

    controller_prototypes_.SetIsEntityAllowedToLeaveLane(user_settings_.ground_truth.allow_invalid_lane_locations);

    config_converter_ =
        std::make_unique<controller::ExternalControllerConfigConverter>(GetExternalControllerConfigConverterData());

    controller_prototypes_.SetConfigConverter(config_converter_.get());
    controller_prototypes_.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service_.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service_.SetIsEntityAllowedToLeaveLane(user_settings_.ground_truth.allow_invalid_lane_locations);

    if (user_settings_.simulation_results.log_cyclics)
    {
        output_generator_.Init();
    }
}

void GtGenEnvironment::CreateMap(const std::string& absolute_map_file_path,
                                 const mantle_api::MapDetails& map_details,
                                 const std::string& map_model_reference)
{
    if (!map_engine_)
    {
        throw EnvironmentException("No MapEngine is set in the Environment.");
    }

    map_engine_->Load(absolute_map_file_path, user_settings_, map_details, *gtgen_map_, &unique_id_provider_);
    gtgen_map_->SetModelReference(map_model_reference);
    coordinate_converter_ = std::make_unique<CoordinateConverter>(gtgen_map_->coordinate_converter.get());

    sensor_view_builder_->Init();
}

controller::ExternalControllerConfigConverterData GtGenEnvironment::GetExternalControllerConfigConverterData() const
{
    controller::ProtoGroundTruthBuilderConfig proto_ground_truth_builder_config{step_size_, &entity_repository_};
    controller::ExternalControllerConfigConverterData data{proto_ground_truth_builder_config,
                                                           gtgen_map_.get(),
                                                           host_vehicle_interface_.get(),
                                                           host_vehicle_model_.get(),
                                                           &user_settings_,
                                                           traffic_command_builder_.get(),
                                                           seed_,
                                                           &output_generator_.GetSettings()};

    return data;
}

void GtGenEnvironment::AddEntityToController(mantle_api::IEntity& entity, std::uint64_t controller_id)
{
    Info("Adding entity #{} to controller #{}", entity.GetUniqueId(), controller_id);
    auto controller = controller_prototypes_.Move(controller_id);
    active_controller_repository_.AddEntityToController(&entity, std::move(controller));
}

void GtGenEnvironment::RemoveEntityFromController(std::uint64_t entity_id, std::uint64_t controller_id)
{
    Info("Remove controllers of entity #{}", entity_id);
    std::ignore = controller_id;
    active_controller_repository_.RemoveControllersFromEntity(entity_id);
}

void GtGenEnvironment::UpdateControlStrategies(
    mantle_api::UniqueId entity_id,
    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies)
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);
    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            Debug("Adding control strategies for entity {} to TrafficCommandBuilder", entity_id);
            const auto entity = entity_repository_.Get(entity_id).value();
            traffic_command_builder_->AddControlStrategiesForEntity(
                entity.get(), lane_location_provider_.get(), control_strategies);
        }
        else
        {
            for (const auto& control_strategy : control_strategies)
            {
                Debug("Trying to add control unit for control strategy {} to controller #{}",
                      control_strategy->type,
                      composite_controller->GetUniqueId());
                if (auto control_unit = controller::InternalControllerFactory::CreateControlUnit(
                        control_strategy.get(), lane_location_provider_.get()))
                {
                    composite_controller->AddControlUnit(std::move(control_unit));
                }
            }
        }
    }
}

bool GtGenEnvironment::HasControlStrategyGoalBeenReached(std::uint64_t entity_id,
                                                         mantle_api::ControlStrategyType type) const
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);

    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (const auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            // We cannot ask the VehicleModel/TrafficParticipantControlUnit, so assuming false
            return false;
        }

        return composite_controller->HasControlStrategyGoalBeenReached(entity_id, type);
    }

    // Return false when no active controller available
    return false;
}

void GtGenEnvironment::Step(mantle_api::Time delta_time)
{
    std::ignore = delta_time;  // delta not needed. control units take current time and calculate delta.

    traffic_command_builder_->Step();
    StepEntities();
    sensor_view_builder_->Step(entity_repository_.GetEntities(), &entity_repository_.GetHost());

    if (user_settings_.simulation_results.log_cyclics)
    {
        PublishGlobalData();
        output_generator_.Step(GetSimulationTime());
    }
}

void GtGenEnvironment::StepEntities()
{
    active_controller_repository_.Step(service::utility::Clock::Instance().Now());

    lane_assignment_service_.Step();

    for (const auto& id : active_controller_repository_.GetCurrentEntityIdsWithoutControllers())
    {
        if (entity_repository_.GetHost().GetUniqueId() != id)
        {
            entity_repository_.Delete(id);
        }
    }
}

const mantle_api::ILaneLocationQueryService& GtGenEnvironment::GetQueryService() const
{
    return *lane_location_provider_;
}

mantle_api::ICoordConverter* GtGenEnvironment::GetConverter()
{
    return coordinate_converter_.get();
}

const mantle_api::IGeometryHelper* GtGenEnvironment::GetGeometryHelper() const
{
    return &geometry_helper_;
}

mantle_api::IEntityRepository& GtGenEnvironment::GetEntityRepository()
{
    return entity_repository_;
}

const mantle_api::IEntityRepository& GtGenEnvironment::GetEntityRepository() const
{
    return entity_repository_;
}

mantle_api::IControllerRepository& GtGenEnvironment::GetControllerRepository()
{
    return controller_prototypes_;
}

const mantle_api::IControllerRepository& GtGenEnvironment::GetControllerRepository() const
{
    return controller_prototypes_;
}

void GtGenEnvironment::SetWeather(mantle_api::Weather weather)
{
    sensor_view_builder_->SetWeather(weather);
}

void GtGenEnvironment::SetDateTime(mantle_api::Time date_time)
{
    sensor_view_builder_->SetDateTime(date_time);
}

void GtGenEnvironment::SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches)
{
    friction_patches_ = friction_patches;
}

void GtGenEnvironment::SetTrafficSignalState(const std::string& traffic_signal_name,
                                             const std::string& traffic_signal_state)
{
    auto entity = entity_repository_.Get(traffic_signal_name).value();
    auto& traffic_light_entity = dynamic_cast<entities::TrafficLightEntity&>(entity.get());
    traffic_light_entity.ChangeTrafficSignalState(traffic_signal_state);
}

void GtGenEnvironment::ExecuteCustomCommand(const std::vector<std::string>& actors,
                                            const std::string& type,
                                            const std::string& command)
{
    if (actors.empty())
    {
        Warn("Ignoring non-supported custom command of type '{}' without actors", type);
        return;
    }

    for (auto actor : actors)
    {
        const auto entity = &entity_repository_.Get(actor).value().get();

        // Check if actor has active external composite controller. Could also be a StationaryObject without controller
        auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity->GetUniqueId());

        if (composite_controllers.empty())
        {
            Warn("Ignoring non-supported custom command of type '{}' for actor '{}' without controller.", type, actor);
            continue;
        }

        for (auto& composite_controller : composite_controllers)
        {
            if (!composite_controller->IsActive())
            {
                continue;
            }

            if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
            {
                Debug("Adding CustomTrafficAction with type '{}' for entity '{}'", type, actor);
                traffic_command_builder_->AddCustomTrafficActionForEntity(entity->GetUniqueId(), type, command);
            }
            else
            {
                Warn("Ignoring custom command of type '{}' for actor '{}', as internal controllers don't support it.",
                     type,
                     actor);
            }
        }
    }
}

void GtGenEnvironment::SetUserDefinedValue(const std::string& name, const std::string& value)
{
    // Value is set in thread from async server
    std::scoped_lock lock(user_defined_value_mutex);
    user_defined_values_[name] = value;
}

std::optional<std::string> GtGenEnvironment::GetUserDefinedValue(const std::string& name)
{
    std::scoped_lock lock(user_defined_value_mutex);
    if (user_defined_values_.find(name) != user_defined_values_.end())
    {
        return user_defined_values_[name];
    }
    return {};
}

host::HostVehicleModel& GtGenEnvironment::GetHostVehicleModel()
{
    return *host_vehicle_model_;
}

const host::HostVehicleInterface& GtGenEnvironment::GetHostVehicleInterface() const
{
    return *host_vehicle_interface_;
}

chunking::StaticChunkList GtGenEnvironment::GetStaticChunks() const
{
    return sensor_view_builder_->GetStaticChunks();
}

const proto_groundtruth::SensorViewBuilder& GtGenEnvironment::GetSensorViewBuilder() const
{
    return *sensor_view_builder_;
}

const osi3::SensorView& GtGenEnvironment::GetSensorView() const
{
    return sensor_view_builder_->GetSensorView();
}

const std::vector<osi3::TrafficCommand>& GtGenEnvironment::GetTrafficCommands() const
{
    return traffic_command_builder_->GetTrafficCommands();
}

mantle_api::Time GtGenEnvironment::GetDateTime()
{
    return sensor_view_builder_->GetDateTime();
}

mantle_api::Time GtGenEnvironment::GetSimulationTime()
{
    return service::utility::Clock::Instance().Now();
}

const map::GtGenMap& GtGenEnvironment::GetGtGenMap() const
{
    return *gtgen_map_;
}

void GtGenEnvironment::SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior)
{
    controller_prototypes_.SetDefaultRoutingBehavior(default_routing_behavior);
}

void GtGenEnvironment::AssignRoute(mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition)
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);

    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            for (auto& control_unit : composite_controller->GetControlUnits())
            {
                if (dynamic_cast<controller::HostVehicleInterfaceControlUnit*>(control_unit.get()) != nullptr)
                {
                    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
                    waypoints.reserve(route_definition.waypoints.size());
                    for (auto& route_waypoint : route_definition.waypoints)
                    {
                        waypoints.push_back(route_waypoint.waypoint);
                    }
                    composite_controller->AddControlUnit(std::make_unique<controller::HostVehicleInterfaceControlUnit>(
                        waypoints, lane_location_provider_.get(), traffic_command_builder_.get()));
                    break;
                }
            }
        }
        else
        {
            composite_controller->AddControlUnit(controller::InternalControllerFactory::CreateControlUnit(
                route_definition, lane_location_provider_.get()));
        }
    }
}

mantle_api::ITrafficSwarmService& GtGenEnvironment::GetTrafficSwarmService()
{
    if (traffic_swarm_)
    {
        return *traffic_swarm_;
    }
    throw std::runtime_error("GtGenEnvironment::GetTrafficSwarmService : traffic swarm has not been initialized");
}

void GtGenEnvironment::InitTrafficSwarmService(const mantle_api::TrafficSwarmParameters& parameters)
{
    if (lane_location_provider_ == nullptr)
    {
        throw EnvironmentException("InitTrafficSwarmService : lane_location_provider_ is unassigned");
    }
    traffic_swarm::TrafficSwarmParameters traffic_parameters{
        traffic_swarm::TrafficSwarmXosc::ConvertParameters(parameters)};
    traffic_parameters.seed = seed_;

    traffic_swarm_ =
        std::make_unique<traffic_swarm::TrafficSwarmXosc>(*lane_location_provider_,
                                                          entity_repository_,
                                                          traffic_parameters,
                                                          longitudinal_distance_between_traffic_swarm_vehicles);
}

void GtGenEnvironment::SetMapEngine(std::unique_ptr<IMapEngine> map_engine)
{
    map_engine_ = std::move(map_engine);
}

void GtGenEnvironment::SetVariable(const std::string&, const mantle_api::ParameterType&) {}

std::optional<mantle_api::ParameterType> GtGenEnvironment::GetVariable(const std::string&) const
{
    return std::nullopt;
}

std::optional<mantle_api::Time> GtGenEnvironment::GetDesiredDeltaTime() const noexcept
{
    return std::nullopt;
}

mantle_api::IEnvironment* GtGenEnvironment::GetEnvironment() noexcept
{
    return dynamic_cast<mantle_api::IEnvironment*>(this);
}

void GtGenEnvironment::PublishGlobalData()
{
    for (const std::unique_ptr<mantle_api::IEntity>& entity : entity_repository_.GetEntities())
    {
        output_generator_.PutCyclic(entity->GetUniqueId(), "XPosition", entity->GetPosition().x.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "YPosition", entity->GetPosition().y.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "ZPosition", entity->GetPosition().z.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "YawAngle", entity->GetOrientation().yaw.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "PitchAngle", entity->GetOrientation().pitch.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "RollAngle", entity->GetOrientation().roll.value());
        output_generator_.PutCyclic(entity->GetUniqueId(), "Lane", entity->GetAssignedLaneIds());
        output_generator_.PutCyclic(entity->GetUniqueId(), "PositionRoute", NAN);
        output_generator_.PutCyclic(entity->GetUniqueId(), "Road", NAN);
        output_generator_.PutCyclic(entity->GetUniqueId(), "TCoordinate", NAN);
        output_generator_.PutCyclic(
            entity->GetUniqueId(), "AccelerationEgo", entity->GetAcceleration().Length().value());
        // Missing: output_generator_.PutCyclic(entity->GetUniqueId(), "BrakeLight", );
        const mantle_api::IVehicle* vehicle_entity{dynamic_cast<const mantle_api::IVehicle*>(entity.get())};
        if (vehicle_entity != nullptr)
        {
            output_generator_.PutCyclic(
                vehicle_entity->GetUniqueId(), "IndicatorState", static_cast<int>(vehicle_entity->GetIndicatorState()));
        }
        // Missing: output_generator_.PutCyclic(entity->GetUniqueId(), "LightStatus", );
        output_generator_.PutCyclic(entity->GetUniqueId(), "VelocityEgo", entity->GetVelocity().Length().value());
    }
}

bool GtGenEnvironment::IsDefaultRandomSeed(const std::uint32_t seed)
{
    return seed == 0U;
}

nlohmann::ordered_json GtGenEnvironment::GenerateCoreInformation() const
{
    nlohmann::ordered_json entities;
    for (const std::unique_ptr<mantle_api::IEntity>& generic_entity : entity_repository_.GetEntities())
    {
        nlohmann::ordered_json entity;
        entity["id"] = generic_entity->GetUniqueId();
        entity["name"] = generic_entity->GetName();

        entities.push_back(entity);
    }

    nlohmann::ordered_json simulation_run;
    simulation_run["id"] = 0;
    simulation_run["randomSeed"] = seed_;
    simulation_run["entities"] = entities;

    nlohmann::ordered_json simulation_runs;
    simulation_runs.push_back(simulation_run);

    nlohmann::ordered_json core_information;
    core_information["simulatorVersion"] = gtgen::core::GetGtGenCoreVersion();
    core_information["schemaVersion"] = kCoreInformationSchemaVersion;
    core_information["simulationRuns"] = simulation_runs;

    return core_information;
}

}  // namespace gtgen::core::environment::api
