/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENTITYREPOSITORY_H
#define GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENTITYREPOSITORY_H

#include "Core/Environment/GtGenEnvironment/Internal/entity_producer.h"

#include <MantleAPI/Traffic/i_entity_repository.h>

#include <memory>

namespace gtgen::core::service::utility
{
class UniqueIdProvider;
}

namespace gtgen::core::environment::api
{
class EntityRepository : public mantle_api::IEntityRepository
{
  public:
    explicit EntityRepository(service::utility::UniqueIdProvider* unique_id_provider);

    mantle_api::IVehicle& Create(const std::string& name, const mantle_api::VehicleProperties& properties) override;
    mantle_api::IVehicle& Create(mantle_api::UniqueId id,
                                 const std::string& name,
                                 const mantle_api::VehicleProperties& properties) override;
    mantle_api::IPedestrian& Create(const std::string& name,
                                    const mantle_api::PedestrianProperties& properties) override;
    mantle_api::IPedestrian& Create(mantle_api::UniqueId id,
                                    const std::string& name,
                                    const mantle_api::PedestrianProperties& properties) override;
    mantle_api::IStaticObject& Create(const std::string& name,
                                      const mantle_api::StaticObjectProperties& properties) override;
    mantle_api::IStaticObject& Create(mantle_api::UniqueId id,
                                      const std::string& name,
                                      const mantle_api::StaticObjectProperties& properties) override;

    mantle_api::IVehicle& GetHost() override;
    std::optional<std::reference_wrapper<mantle_api::IEntity>> Get(const std::string& name) override;
    std::optional<std::reference_wrapper<const mantle_api::IEntity>> Get(const std::string& name) const override;
    std::optional<std::reference_wrapper<mantle_api::IEntity>> Get(mantle_api::UniqueId id) override;
    std::optional<std::reference_wrapper<const mantle_api::IEntity>> Get(mantle_api::UniqueId id) const override;

    [[nodiscard]] const std::vector<std::unique_ptr<mantle_api::IEntity>>& GetEntities() const override;

    [[nodiscard]] bool Contains(mantle_api::UniqueId id) const override;
    void Delete(const std::string& name) override;
    void Delete(mantle_api::UniqueId id) override;

    void Reset() override
    { /* not used yet so not implemented */
    }

    void RegisterEntityCreatedCallback(const std::function<void(mantle_api::IEntity&)>& fct) override
    {
        std::ignore = fct;
    }
    void RegisterEntityDeletedCallback(const std::function<void(const std::string&)>& fct) override
    {
        std::ignore = fct;
    }
    void RegisterEntityDeletedCallback(const std::function<void(mantle_api::UniqueId)>& fct) override
    {
        std::ignore = fct;
    }

  private:
    void ThrowIfEntityWithIdExists(mantle_api::UniqueId id) const;
    void ThrowIfEntityWithNameExists(const std::string& name) const;
    [[nodiscard]] mantle_api::IEntity* Find(const std::string& name) const;
    [[nodiscard]] mantle_api::IEntity* Find(mantle_api::UniqueId id) const;
    mantle_api::UniqueId CreateUniqueIdFromName(const std::string& name);
    void ResetUniqueIdProviderForEntities();
    void HandleTrafficSignEntity(const mantle_api::UniqueId& id, mantle_api::IEntity* entity);
    void HandleSupplementarySignEntity(const mantle_api::UniqueId& id,
                                       const mantle_api::StaticObjectProperties& properties,
                                       mantle_api::IEntity* entity);

    std::vector<std::unique_ptr<mantle_api::IEntity>> entities_{};
    mantle_api::IEntity* host_{nullptr};
    service::utility::UniqueIdProvider* unique_id_provider_;
    EntityProducer entity_producer_{};
    bool first_id_creation_{true};

    // Used to associate supplementary sign entities with main sign entities
    std::unordered_map<std::string, mantle_api::UniqueId> main_sign_name_to_mantle_id_;
    std::unordered_multimap<std::string, mantle_api::UniqueId> supp_sign_reference_name_to_mantle_id_;
};

}  // namespace gtgen::core::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_ENTITYREPOSITORY_H
