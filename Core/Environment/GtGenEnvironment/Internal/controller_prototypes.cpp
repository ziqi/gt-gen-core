/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/controller_prototypes.h"

#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Controller/controller_factory.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Utility/algorithm_utils.h"

#include <algorithm>

namespace gtgen::core::environment::api
{

ControllerPrototypes::ControllerPrototypes(service::utility::UniqueIdProvider* unique_id_provider)
    : unique_id_provider_(unique_id_provider)
{
}

mantle_api::IController& ControllerPrototypes::Create(std::unique_ptr<mantle_api::IControllerConfig> config)
{
    return Create(unique_id_provider_->GetUniqueId(), std::move(config));
}

mantle_api::IController& ControllerPrototypes::Create(mantle_api::UniqueId id,
                                                      std::unique_ptr<mantle_api::IControllerConfig> config)
{
    if (Find(id) != nullptr)
    {
        throw EnvironmentException(
            "A controller already exists with ID {}. Please contact GTGen Support for further assistance.", id);
    }

    if (auto* external_config = dynamic_cast<mantle_api::ExternalControllerConfig*>(config.get()))
    {
        config.reset(config_converter_->GetConfig(external_config));
    }
    config->map_query_service = lane_location_provider_;

    if (!unique_id_provider_->IsIdReserved(id))
    {
        unique_id_provider_->ReserveId(id);
    }

    auto composite_controller = controller::ControllerFactory::Create(id, std::move(config), default_routing_behavior_);
    template_controllers_.push_back(std::move(composite_controller));

    return *Find(id);
}

std::unique_ptr<controller::CompositeController> ControllerPrototypes::Move(mantle_api::UniqueId id)
{
    auto it = std::find_if(
        template_controllers_.begin(),
        template_controllers_.end(),
        [&id](std::unique_ptr<controller::CompositeController>& object) { return object->GetUniqueId() == id; });
    if (it == template_controllers_.end())
    {
        // Debug("Could not find controller to move. A controller with id {} needs to be created first, by calling
        // Environment::CreateController()", id);
        throw EnvironmentException(
            "A controller with id {} could not be moved because it has not been created. Please open an issue "
            "for further assistance.",
            id);
    }

    auto moved_object = std::move(*it);
    template_controllers_.erase(it);

    return moved_object;
}

void ControllerPrototypes::SetIsEntityAllowedToLeaveLane(bool is_entity_allowed_to_leave_lane)
{
    is_entity_allowed_to_leave_lane_ = is_entity_allowed_to_leave_lane;
}

void ControllerPrototypes::SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior)
{
    default_routing_behavior_ = default_routing_behavior;
}

void ControllerPrototypes::SetConfigConverter(controller::ExternalControllerConfigConverter* converter)
{
    config_converter_ = converter;
}

void ControllerPrototypes::SetLaneLocationProvider(map::LaneLocationProvider* lane_location_provider)
{
    lane_location_provider_ = lane_location_provider;
}

controller::CompositeController* ControllerPrototypes::Find(mantle_api::UniqueId id) const
{
    auto it = service::utility::FindObjectById(template_controllers_, id);
    if (it != template_controllers_.end())
    {
        return (*it).get();
    }
    return nullptr;
}

}  // namespace gtgen::core::environment::api
