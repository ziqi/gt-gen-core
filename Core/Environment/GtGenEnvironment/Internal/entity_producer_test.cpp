/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/entity_producer.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Utility/unique_id_provider.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::api
{
namespace
{

using units::literals::operator""_m;

TEST(EntityProducerTest, GivenHostProperties_WhenCreatingVehicle_ThenHostIsCreated)
{
    mantle_api::VehicleProperties host_properties;
    host_properties.is_host = true;

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(0, "host", host_properties);

    EXPECT_EQ(0, entity->GetUniqueId());
    EXPECT_EQ("host", entity->GetName());
}

TEST(EntityProducerTest, GivenVehicleProperties_WhenCreatingEntity_ThenVehicleIsCreated)
{
    mantle_api::UniqueId expected_id{42};
    std::string expected_name{"a-car"};

    const auto entity_producer = EntityProducer();
    std::unique_ptr<mantle_api::IEntity> entity =
        entity_producer.Produce(expected_id, expected_name, mantle_api::VehicleProperties{});

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ(expected_name, entity->GetName());
}

TEST(EntityProducerTest, GivenPedestrianProperties_WhenCreatingEntity_ThenPedestrianIsCreated)
{
    mantle_api::UniqueId expected_id{42};
    std::string expected_name{"pedestrian"};

    const auto entity_producer = EntityProducer();
    std::unique_ptr<mantle_api::IEntity> entity =
        entity_producer.Produce(expected_id, expected_name, mantle_api::PedestrianProperties{});

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ(expected_name, entity->GetName());
}

TEST(EntityProducerTest,
     GivenPedestrianPropertiesWithTypeAnimal_WhenCreatingEntity_ThenPedestrianWithTypeAnimalIsCreated)
{
    mantle_api::PedestrianProperties pedestrian_properties;
    pedestrian_properties.type = mantle_api::EntityType::kAnimal;

    const auto entity_producer = EntityProducer();
    std::unique_ptr<mantle_api::IEntity> entity = entity_producer.Produce(42, "animal", pedestrian_properties);

    const auto properties = entity->GetProperties();
    EXPECT_EQ(properties->type, mantle_api::EntityType::kAnimal);
}

struct StaticObjectProperties
{
    std::string object_type;
    std::string type;
    std::string subtype;
    bool expected_valid;
};

class StaticObjectPropertiesTestFixture : public ::testing::TestWithParam<StaticObjectProperties>
{
};

INSTANTIATE_TEST_SUITE_P(StaticObjectProperties,
                         StaticObjectPropertiesTestFixture,
                         ::testing::ValuesIn({
                             // invalid
                             StaticObjectProperties{"invalid_object_type", "42", "42", false},
                             StaticObjectProperties{"supplementary_sign", "-42", "42", false},
                             StaticObjectProperties{"supplementary_sign", "42", "-42", false},
                             StaticObjectProperties{"", "abc", "42", false},
                             StaticObjectProperties{"", "42", "abc", false},
                             // valid
                             StaticObjectProperties{"traffic_light", "42", "42", true},
                             StaticObjectProperties{"traffic_sign", "none", "none", true},
                             StaticObjectProperties{"road_marking", "-1", "-1", true},
                             StaticObjectProperties{"traffic_sign", "", "42", true},
                             StaticObjectProperties{"traffic_sign", "42", "", true},
                             StaticObjectProperties{"", "42", "42", true},
                             StaticObjectProperties{"", "42-42", "", true},
                             StaticObjectProperties{"", "42.42-42", "", true},
                         }));

TEST_P(StaticObjectPropertiesTestFixture, GivenInvalidTypeOrSubtype_WhenCreatingStaticObject_ThenException)
{
    const auto test_vector = GetParam();

    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties static_object_properties;
    static_object_properties.properties.insert(std::make_pair("object_type", test_vector.object_type));
    static_object_properties.properties.insert(std::make_pair("type", test_vector.type));
    static_object_properties.properties.insert(std::make_pair("sub_type", test_vector.subtype));

    const auto entity_producer = EntityProducer();

    if (test_vector.expected_valid)
    {
        EXPECT_NO_THROW(entity_producer.Produce(expected_id, "static_object", static_object_properties));
    }
    else
    {
        EXPECT_THROW(entity_producer.Produce(expected_id, "static_object", static_object_properties),
                     EnvironmentException);
    }
}

TEST(EntityProducerTest, GivenPylonAsEntity_WhenCreatingPylon_ThenPylonWithStandardProperties)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties pylon_properties;
    pylon_properties.properties.insert(std::make_pair("type", "610-40"));
    mantle_api::Vec3<units::length::meter_t> expected_geometric_center{0_m, 0_m, 0.385_m};
    pylon_properties.bounding_box.geometric_center = expected_geometric_center;
    mantle_api::Dimension3 expected_dimension{0.5_m, 0.5_m, 0.77_m};

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "pylon", pylon_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("pylon", entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("pylon", properties->model);

    EXPECT_TRIPLE(expected_dimension, properties->bounding_box.dimension);
    EXPECT_TRIPLE(expected_geometric_center, properties->bounding_box.geometric_center);
}

TEST(EntityProducerTest, GivenTrafficLightProperties_WhenCreatingEntity_ThenTrafficLightWithStandardProperties)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties traffic_light_properties;
    traffic_light_properties.properties.insert(std::make_pair("object_type", "traffic_light"));
    traffic_light_properties.properties.insert(std::make_pair("type", "1.000.001"));
    traffic_light_properties.properties.insert(std::make_pair("sub_type", "none"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_light", traffic_light_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("traffic_light", entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("", properties->model);
}

TEST(EntityProducerTest, GivenTrafficSignProperties_WhenCreatingEntity_ThenTrafficSignWithStandardProperties)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", "traffic_sign"));
    traffic_sign_properties.properties.insert(std::make_pair("type", "274"));
    traffic_sign_properties.properties.insert(std::make_pair("sub_type", "50"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_sign_name", traffic_sign_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("traffic_sign_name", entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("", properties->model);
    EXPECT_EQ("traffic_sign", properties->properties["object_type"]);
    EXPECT_EQ("274", properties->properties["type"]);
    EXPECT_EQ("50", properties->properties["sub_type"]);
    const auto* output_traffic_sign_properties =
        dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    ASSERT_NE(nullptr, output_traffic_sign_properties);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kKilometerPerHour, output_traffic_sign_properties->unit);
}

TEST(EntityProducerTest, GivenTrafficSignProperties_WhenCreatingEntityWithoutSubtype_ThenStopSignWithStandardProperties)
{
    mantle_api::UniqueId expected_id{42};
    const std::string expected_type_stop_sign{"206"};
    const std::string expected_traffic_sign_name{"stop_sign"};
    const std::string expected_object_type{"traffic_sign"};

    mantle_api::StaticObjectProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", expected_object_type));
    traffic_sign_properties.properties.insert(std::make_pair("type", expected_type_stop_sign));

    const auto entity = EntityProducer().Produce(expected_id, expected_traffic_sign_name, traffic_sign_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ(expected_traffic_sign_name, entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("", properties->model);
    EXPECT_EQ(expected_object_type, properties->properties["object_type"]);
    EXPECT_EQ(expected_type_stop_sign, properties->properties["type"]);

    const auto* output_traffic_sign_properties =
        dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    ASSERT_NE(nullptr, output_traffic_sign_properties);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kUnknown, output_traffic_sign_properties->unit);
    EXPECT_EQ(osi::OsiTrafficSignType::kStop, output_traffic_sign_properties->sign_type);
}

TEST(EntityProducerTest, GivenTrafficSignPropertiesWithEmptyUnit_WhenCreatingEntity_ThenTrafficSignWithStandardUnit)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", "traffic_sign"));
    traffic_sign_properties.properties.insert(std::make_pair("type", "274"));
    traffic_sign_properties.properties.insert(std::make_pair("sub_type", "50"));
    traffic_sign_properties.properties.insert(std::make_pair("unit", ""));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_sign_name", traffic_sign_properties);

    const auto properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    ASSERT_NE(nullptr, properties);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kKilometerPerHour, properties->unit);
}

TEST(EntityProducerTest,
     GivenTrafficSignPropertiesWithNonExistentUnit_WhenCreatingEntity_ThenTrafficSignWithDefaultUnit)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", "traffic_sign"));
    traffic_sign_properties.properties.insert(std::make_pair("type", "274"));
    traffic_sign_properties.properties.insert(std::make_pair("sub_type", "50"));
    traffic_sign_properties.properties.insert(std::make_pair("unit", "aunit"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_sign_name", traffic_sign_properties);

    const auto properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    ASSERT_NE(nullptr, properties);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kKilometerPerHour, properties->unit);
}

TEST(EntityProducerTest, GivenTrafficSignPropertiesWithCustomUnit_WhenCreatingEntity_ThenTrafficSignWithCustomUnit)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", "traffic_sign"));
    traffic_sign_properties.properties.insert(std::make_pair("type", "274"));
    traffic_sign_properties.properties.insert(std::make_pair("sub_type", "50"));
    traffic_sign_properties.properties.insert(std::make_pair("unit", "mph"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_sign_name", traffic_sign_properties);

    const auto properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    ASSERT_NE(nullptr, properties);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kMilePerHour, properties->unit);
}

TEST(EntityProducerTest, GivenStaticObjectPropertiesWithMountHeight_WhenCreatingEntity_ThenVerticalOffsetIsSet)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties static_object_properties;
    static_object_properties.properties.insert(std::make_pair("type", "610-40"));
    static_object_properties.properties.insert(std::make_pair("mount_height", "10"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "static_object_name", static_object_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("static_object_name", entity->GetName());

    auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity->GetProperties());
    EXPECT_EQ(10, properties->vertical_offset());
}

TEST(EntityProducerTest, GivenStaticObjectPropertiesWithoutMountHeight_WhenCreatingEntity_ThenVerticalOffsetIsDefault)
{
    mantle_api::UniqueId expected_id{1};
    mantle_api::StaticObjectProperties static_object_properties;
    static_object_properties.properties.insert(std::make_pair("type", "610-40"));
    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "static_object_name", static_object_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("static_object_name", entity->GetName());

    auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity->GetProperties());
    EXPECT_EQ(0, properties->vertical_offset());
}

TEST(EntityProducerTest, GivenTrafficLightPropertiesWithoutMountHeight_WhenCreatingEntity_ThenVerticalOffsetIsDefault)
{
    mantle_api::UniqueId expected_id{1};
    mantle_ext::TrafficLightProperties traffic_light_properties;
    traffic_light_properties.properties.insert(std::make_pair("object_type", "traffic_light"));
    traffic_light_properties.properties.insert(std::make_pair("type", "1.000.001"));
    traffic_light_properties.properties.insert(std::make_pair("sub_type", ""));
    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_light_name", traffic_light_properties);
    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("traffic_light_name", entity->GetName());
    auto properties = dynamic_cast<mantle_ext::TrafficLightProperties*>(entity->GetProperties());
    EXPECT_EQ(2.2, properties->vertical_offset());
}

TEST(EntityProducerTest, GivenTrafficSignPropertiesWithoutMountHeight_WhenCreatingEntity_ThenVerticalOffsetIsDefault)
{
    mantle_api::UniqueId expected_id{1};
    mantle_ext::TrafficSignProperties traffic_sign_properties;
    traffic_sign_properties.properties.insert(std::make_pair("object_type", "traffic_sign"));
    traffic_sign_properties.properties.insert(std::make_pair("type", "101"));
    traffic_sign_properties.properties.insert(std::make_pair("sub_type", "12"));

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, "traffic_sign_name", traffic_sign_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ("traffic_sign_name", entity->GetName());

    auto properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    EXPECT_EQ(2.2, properties->vertical_offset());
}

TEST(EntityProducerTest, GivenStopLineRoadMarkingProperties_WhenCreatingEntity_ThenStopLineWithCorrectProperties)
{
    mantle_api::UniqueId expected_id{43};
    const std::string expected_type_stop_line{"294"};
    const std::string expected_road_marking_name{"stop_line"};
    const std::string expected_object_type{"road_marking"};
    const mantle_api::Dimension3 expected_dimension{3.0_m, 0.5_m, 0.01_m};
    const units::length::meter_t expected_vertical_offset = 42.1_m;

    mantle_api::StaticObjectProperties road_marking_properties;
    road_marking_properties.properties.insert(std::make_pair("object_type", expected_object_type));
    road_marking_properties.properties.insert(std::make_pair("type", expected_type_stop_line));
    road_marking_properties.properties.insert(
        std::make_pair("mount_height", std::to_string(expected_vertical_offset.value())));
    road_marking_properties.bounding_box.dimension = expected_dimension;
    road_marking_properties.vertical_offset = expected_vertical_offset;

    const auto entity = EntityProducer().Produce(expected_id, expected_road_marking_name, road_marking_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ(expected_road_marking_name, entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("", properties->model);
    EXPECT_EQ(expected_object_type, properties->properties["object_type"]);
    EXPECT_EQ(expected_type_stop_line, properties->properties["type"]);
    EXPECT_EQ("", properties->properties["sub_type"]);

    const auto* output_road_marking_properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(properties);
    ASSERT_NE(nullptr, output_road_marking_properties);
    EXPECT_TRIPLE(expected_dimension, output_road_marking_properties->bounding_box.dimension);
    EXPECT_EQ(expected_vertical_offset, output_road_marking_properties->vertical_offset);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kUnknown, output_road_marking_properties->unit);
    EXPECT_EQ(osi::OsiRoadMarkingsType::kSymbolicTrafficSign, output_road_marking_properties->marking_type);
    EXPECT_EQ(osi::OsiTrafficSignType::kStop, output_road_marking_properties->sign_type);
}

TEST(EntityProducerTest, GivenSupplementarySignProperties_WhenCreatingEntity_ThenSupplementarySignWithCorrectProperties)
{
    std::string expected_name{"supplementary_sign"};
    mantle_api::UniqueId expected_id{1};
    mantle_api::Dimension3 expected_dimension{0.1_m, 0.3_m, 0.3_m};
    mantle_api::StaticObjectProperties supplementary_sign_properties;
    supplementary_sign_properties.properties.insert(std::make_pair("object_type", "supplementary_sign"));
    supplementary_sign_properties.properties.insert(std::make_pair("type", "1010"));
    supplementary_sign_properties.properties.insert(std::make_pair("sub_type", "51"));
    supplementary_sign_properties.properties.insert(std::make_pair("main_sign_reference_id", "1"));
    supplementary_sign_properties.properties.insert(std::make_pair("value", "100"));
    supplementary_sign_properties.properties.insert(std::make_pair("unit", "m"));
    supplementary_sign_properties.bounding_box.dimension = expected_dimension;

    const auto entity_producer = EntityProducer();
    const auto entity = entity_producer.Produce(expected_id, expected_name, supplementary_sign_properties);

    EXPECT_EQ(expected_id, entity->GetUniqueId());
    EXPECT_EQ(expected_name, entity->GetName());

    const auto properties = entity->GetProperties();
    EXPECT_EQ(mantle_api::EntityType::kStatic, properties->type);
    EXPECT_EQ("", properties->model);
    EXPECT_EQ("supplementary_sign", properties->properties["object_type"]);
    EXPECT_EQ("1010", properties->properties["type"]);
    EXPECT_EQ("51", properties->properties["sub_type"]);
    EXPECT_EQ("1", properties->properties["main_sign_reference_id"]);
    EXPECT_EQ("100", properties->properties["value"]);
    EXPECT_EQ("m", properties->properties["unit"]);
    const auto* output_supplementary_sign_properties =
        dynamic_cast<mantle_ext::SupplementarySignProperties*>(properties);
    ASSERT_NE(nullptr, output_supplementary_sign_properties);
    EXPECT_EQ(osi::OsiSupplementarySignType::kConstrainedTo,
              output_supplementary_sign_properties->supplementary_sign_type);
    EXPECT_EQ(100, output_supplementary_sign_properties->value);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kMeter, output_supplementary_sign_properties->unit);
    ASSERT_EQ(1, output_supplementary_sign_properties->actors.size());
    EXPECT_EQ(osi::OsiSupplementarySignActor::kTrucks, output_supplementary_sign_properties->actors.at(0));
    EXPECT_TRIPLE(expected_dimension, output_supplementary_sign_properties->bounding_box.dimension);
}

}  // namespace
}  // namespace gtgen::core::environment::api
