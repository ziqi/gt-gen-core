/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CHUNKING_CHUNKING_H
#define GTGEN_CORE_ENVIRONMENT_CHUNKING_CHUNKING_H

#include "Core/Environment/Chunking/Internal/polyline_chunking.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Profiling/profiling.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::chunking
{

/// @brief Chunker Interface, for implementations details see the implementations.
///
/// Implementations:
///     - PolylineChunking -- default
///
/// Basic idea: Chunk keep pointers to elements that should be part of the ground truth.
///             For elements that are part of the map, this means that elements in the given `map` in Initialize have be
///             stable over the lifetime of the chunker. Once they change, will be in an invalid state.
template <typename Implementation>
class Chunking
{
  public:
    /// @brief Initialize the static chunks from the given map.
    /// @pre Negative values of map chunking properties are not supported.
    std::size_t Initialize(const environment::map::GtGenMap& map,
                           std::uint16_t chunk_grid_size,
                           std::uint16_t cells_per_direction);

    /// @brief Update dynamic objects in chunks and return the relevant chunks around the host vehicle.
    WorldChunks GetWorldChunks(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities,
                               const CoordinateType& host_position);

    StaticChunkList GetStaticChunks() const { return static_chunks_; }

  private:
    void AddChunkForKeyToGroundTruthResult(const std::vector<ChunkKey>& relevant_keys, WorldChunks& result);

    Implementation chunker_impl_{};
    StaticChunkList static_chunks_{};
};

template <typename Implementation>
std::size_t Chunking<Implementation>::Initialize(const environment::map::GtGenMap& map,
                                                 std::uint16_t chunk_grid_size,
                                                 std::uint16_t cells_per_direction)
{
    GTGEN_PROFILE_SCOPE
    Debug("Initializing map chunks...");

    chunker_impl_.SetChunkSize(chunk_grid_size);
    chunker_impl_.SetCellsPerDirection(cells_per_direction);
    static_chunks_ = chunker_impl_.InitializeChunks(map);
    return static_chunks_.size();
}

template <typename Implementation>
WorldChunks Chunking<Implementation>::GetWorldChunks(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities,
                                                     const CoordinateType& host_position)
{
    GTGEN_PROFILE_SCOPE

    // these are expected to be the 'theoretical keys', they don't need to exist!
    const auto relevant_keys{chunker_impl_.GetChunkKeysAroundPoint(host_position)};

    WorldChunks result;
    result.reserve(relevant_keys.size());

    AddChunkForKeyToGroundTruthResult(relevant_keys, result);

    auto add_object_to_correct_list = [](const auto& object, auto& chunk) {
        if constexpr (std::is_same_v<std::decay_t<decltype(object)>, environment::map::RoadObject>)
        {
            // TODO: in the best case we do not need to distinguish here and we have everything in the entities list
            chunk.road_objects.push_back(&object);
        }
        else if constexpr (std::is_base_of_v<environment::map::TrafficSign, std::decay_t<decltype(object)>>)
        {
            // TODO: in the best case we do not need to distinguish here and we have everything in the entities list
            chunk.traffic_signs.push_back(&object);
        }
        else if constexpr (std::is_base_of_v<mantle_api::IEntity, std::decay_t<decltype(object)>>)
        {
            chunk.entities.push_back(&object);
        }
        else
        {
            LogAndThrow(EnvironmentException{"Object with Id {} has an unsupported ground truth type ({})",
                                             object.GetUniqueId(),
                                             typeid(object).name()});
        }
    };

    auto get_position = [](const auto& object) { return object.GetPosition(); };

    auto add_object_to_relevant_chunks =
        [this, &relevant_keys, &result, &add_object_to_correct_list, &get_position](const auto& object) {
            const auto& position = get_position(*object);
            const auto key{chunker_impl_.GetChunkKey(position)};

            // check if in the relevant keys
            auto pred = [&key](const auto& chunk_key) { return chunk_key == key; };
            auto relevant_chunk_key = std::find_if(relevant_keys.begin(), relevant_keys.end(), pred);
            if (relevant_chunk_key == relevant_keys.end())
            {
                // not relevant for the currently relevant chunks
                return;
            }

            auto pred2 = [&key](const auto& chunk) { return chunk.key == key; };
            auto relevant_chunk = std::find_if(result.begin(), result.end(), pred2);
            if (relevant_chunk != result.end())
            {
                add_object_to_correct_list(*object, *relevant_chunk);
            }
            else
            {
                add_object_to_correct_list(*object, result.emplace_back());
            }
        };

    // Not part of MVP for Json scenario engine. But we need to check how we handle RoadObjects and MountedSigns
    // which have been part of the entity_placement, but we do now rather have a entity_repo, which... currently
    // does not support this out of the box.
    // TODO: Fix this in another item
    // const auto& road_objects = entity_placement.GetRoadObjects();
    // const auto& mounted_signs = entity_placement.GetMountedSigns();

    std::for_each(entities.begin(), entities.end(), add_object_to_relevant_chunks);
    // std::for_each(road_objects.begin(), road_objects.end(), add_object_to_relevant_chunks);
    // std::for_each(mounted_signs.begin(), mounted_signs.end(), add_object_to_relevant_chunks);

    return result;
}

template <typename Implementation>
void Chunking<Implementation>::AddChunkForKeyToGroundTruthResult(const std::vector<ChunkKey>& relevant_keys,
                                                                 WorldChunks& result)
{
    for (const auto& key : relevant_keys)
    {
        if (const auto key_value{static_chunks_.find(key)}; key_value != static_chunks_.end())
        {
            result.push_back(chunker_impl_.ConvertToGroundTruthChunk(key_value->second));
        }
    }
}

using DefaultMapChunker = Chunking<PolylineChunking>;

}  // namespace gtgen::core::environment::chunking

#endif  // GTGEN_CORE_ENVIRONMENT_CHUNKING_CHUNKING_H
