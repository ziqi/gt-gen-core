/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/a_star_algorithm.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::path_finding
{

std::optional<Path> AStarAlgorithm::ShortestPath(const std::vector<SharedNode>& nodes)
{
    ASSERT(nodes.size() >= 2 && "At least 2 nodes must be given to compute a path")

    Path overall_path = InitPath(nodes);

    for (std::size_t i = 1; i < nodes.size(); ++i)
    {
        const auto& start_node = nodes.at(i - 1);
        const auto& target_node = nodes.at(i);

        const auto path_segment = ShortestPathBetweenTwoNodes(start_node, target_node);
        if (!path_segment)
        {
            return std::nullopt;
        }
        AppendToOverallPath(overall_path, *path_segment);
    }

    return overall_path;
}

Path AStarAlgorithm::InitPath(const std::vector<SharedNode>& nodes) const
{
    Path path;
    if (nodes.front() == nodes.back() && nodes.size() > 2)
    {
        path.EnableClosedPath();
    }

    return path;
}

void AStarAlgorithm::AppendToOverallPath(Path& overall_path, const Path& path_to_append) const
{
    // Avoid duplicate nodes in overall path when appending
    if (!overall_path.empty())
    {
        overall_path.pop_back();
    }

    overall_path.insert(overall_path.end(), path_to_append.begin(), path_to_append.end());
}

std::optional<Path> AStarAlgorithm::ShortestPathBetweenTwoNodes(const SharedNode& start_node,
                                                                const SharedNode& target_node)
{
    start_node_ = start_node;
    target_node_ = target_node;

    return CalculateShortestPath();
}

std::optional<Path> AStarAlgorithm::CalculateShortestPath()
{
    TRACE("Calculating shortest path from lane {} to lane {}", start_node_->lane->id, target_node_->lane->id)

    CreateStartRecord();

    RecordPtr closest_record_to_target = TraverseNodesToTarget();
    if (closest_record_to_target->node.lock() != target_node_)
    {
        Error(
            "Could not calculate a path between lane with ID {} and lane with ID {}. \nThe closest lane to the target "
            "has ID {}, check the map for missing connections!",
            start_node_->lane->id,
            target_node_->lane->id,
            closest_record_to_target->node.lock()->lane->id);
        return std::nullopt;
    }

    return ConvertInternalStructureToPath(closest_record_to_target, start_node_);
}

void AStarAlgorithm::CreateStartRecord()
{
    auto start_record = std::make_shared<Record>();
    start_record->node = start_node_;
    start_record->estimated_total_cost = DistanceBetweenNodes(start_node_, target_node_);

    open_list_.push(start_record);
}

RecordPtr AStarAlgorithm::TraverseNodesToTarget()
{
    RecordPtr closest_record_to_target = nullptr;
    while (!open_list_.empty())
    {
        closest_record_to_target = open_list_.TopAndPop();
        if (closest_record_to_target->node.lock() == target_node_)
        {
            TRACE("Reached end node {}", target_node_->lane->id)
            break;
        }

        ProcessSuccessors(closest_record_to_target);

        closed_list_.push(closest_record_to_target);
    }
    return closest_record_to_target;
}

double AStarAlgorithm::DistanceBetweenNodes(const SharedNode& from_node, const SharedNode& to_node) const
{
    return service::glmwrapper::Distance(from_node->world_position, to_node->world_position);
}

bool AStarAlgorithm::CanNodeBeSkipped(const SharedNode& successor_node) const
{
    bool node_already_visited = closed_list_.Contains(successor_node);
    bool is_not_target_node = successor_node != target_node_;
    return node_already_visited && is_not_target_node;
}

bool AStarAlgorithm::AlreadyVisitedNodeIsBetter(const RecordPtr& already_visited_record, double cost_to_successor) const
{
    return already_visited_record != nullptr && already_visited_record->cost_so_far <= cost_to_successor;
}

void AStarAlgorithm::ProcessSuccessors(const RecordPtr& current_record)
{
    for (const auto& weak_edge : current_record->node.lock()->edges)
    {
        auto current_edge = weak_edge.lock();
        TRACE("Processing edge from {} to {} with weight {}",
              current_edge->from_node.lock()->lane->id,
              current_edge->to_node.lock()->lane->id,
              current_edge->weight)

        SharedNode successor_node = current_edge->to_node.lock();
        if (CanNodeBeSkipped(successor_node))
        {
            TRACE("Node {} previously visited. Skip", successor_node->lane->id)
            continue;
        }

        double cost_to_successor = current_record->cost_so_far + current_edge->weight;
        EdgeType edge_type = current_edge->type;

        auto open_list_record = open_list_.GetIfContained(successor_node);
        if (AlreadyVisitedNodeIsBetter(open_list_record, cost_to_successor))
        {
            TRACE("Successor {} is already in in open list, but a shorter path to it is known",
                  successor_node->lane->id)
            continue;
        }

        if (open_list_record == nullptr)
        {
            AddOpenListRecord(successor_node, current_record, cost_to_successor, edge_type);
        }
        else
        {
            UpdateOpenListRecord(open_list_record, successor_node, current_record, cost_to_successor, edge_type);
        }
    }
}

void AStarAlgorithm::AddOpenListRecord(const SharedNode& successor_node,
                                       const RecordPtr& current,
                                       double cost_to_successor,
                                       EdgeType edge_type)
{
    double successor_heuristic_to_target{DistanceBetweenNodes(successor_node, target_node_)};

    RecordPtr end_node_record{std::make_shared<Record>()};
    end_node_record->node = successor_node;
    end_node_record->from_node_record = current;
    end_node_record->to_node_record = end_node_record;
    end_node_record->cost_so_far = cost_to_successor;
    end_node_record->estimated_total_cost = cost_to_successor + successor_heuristic_to_target;
    end_node_record->edge_type = edge_type;

    open_list_.push(end_node_record);

    TRACE("New open list record for node {}: {} -> {} to open list with cost_so_far {} and estimated cost {}",
          end_node_record->node.lock()->lane->id,
          end_node_record->from_node_record.lock()->node.lock()->lane->id,
          end_node_record->to_node_record.lock()->node.lock()->lane->id,
          end_node_record->cost_so_far,
          end_node_record->estimated_total_cost)
}

void AStarAlgorithm::UpdateOpenListRecord(const RecordPtr& open_list_end_node_record,
                                          const SharedNode& successor_node,
                                          const RecordPtr& current,
                                          double cost_to_successor,
                                          EdgeType edge_type)
{
    double successor_heuristic_to_target{DistanceBetweenNodes(successor_node, target_node_)};

    TRACE("Updating existing open-list entry for {} -> {} with cost_so_far {} and estimated cost {}",
          open_list_end_node_record->from_node_record.lock()->node.lock()->lane->id,
          open_list_end_node_record->to_node_record.lock()->node.lock()->lane->id,
          open_list_end_node_record->cost_so_far,
          open_list_end_node_record->estimated_total_cost)

    open_list_end_node_record->from_node_record = current;
    open_list_end_node_record->cost_so_far = cost_to_successor;
    open_list_end_node_record->estimated_total_cost = cost_to_successor + successor_heuristic_to_target;
    open_list_end_node_record->edge_type = edge_type;

    TRACE("Updating existing open-list entry for {} -> {} with cost_so_far {} and estimated cost {}",
          open_list_end_node_record->from_node_record.lock()->node.lock()->lane->id,
          open_list_end_node_record->to_node_record.lock()->node.lock()->lane->id,
          open_list_end_node_record->cost_so_far,
          open_list_end_node_record->estimated_total_cost)
}

std::optional<Path> AStarAlgorithm::ConvertInternalStructureToPath(RecordPtr& to_node, const SharedNode& from_node)
{
    Path path;
    while (to_node->node.lock() != from_node)
    {
        auto shared_node_record = to_node->to_node_record.lock();
        if (shared_node_record == nullptr)
        {
            Error("Could not calculate a path between lane with ID {} and lane with ID {}",
                  path.back()->lane->id,
                  from_node->lane->id);
            return std::nullopt;
        }
        auto shared_node = shared_node_record->node.lock();

        TRACE("Push to path lane {} with lane-change {}",
              shared_node->lane->id,
              shared_node_record->edge_type == EdgeType::kFollow ? "Follow" : "Change")

        path.emplace_front(std::make_shared<PathEntry>(shared_node->lane, shared_node_record->edge_type));
        to_node = to_node->from_node_record.lock();
    }
    TRACE("Push to path lane {} with lane change: Follow", from_node->lane->id)
    path.emplace_front(
        std::make_shared<PathEntry>(from_node->lane, EdgeType::kFollow));  // don't forget to add the start node

    return path;
}

}  // namespace gtgen::core::environment::path_finding
