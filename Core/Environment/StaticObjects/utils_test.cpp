/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/utils.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::static_objects
{

TEST(UtilsTest, GivenIdentifierWithTypeAndSubtype_WhenGetType_ThenCorrectTypeIdentifierReturned)
{
    const auto identifier_with_type_and_subtype = "274-50";
    const auto expected_type_identifier = "274";
    auto type = GetTypeIdentifier(identifier_with_type_and_subtype);
    EXPECT_EQ(expected_type_identifier, type);
}

TEST(UtilsTest, GivenEmptyIdentifier_WhenGetType_ThenEmptyStringIsReturned)
{
    const auto identifier_with_type_and_subtype = "";
    const auto expected_type_identifier = "";
    auto type = GetTypeIdentifier(identifier_with_type_and_subtype);
    EXPECT_EQ(expected_type_identifier, type);
}

TEST(UtilsTest, GivenIdentifierWithTypeAndSubtype_WhenGetSubType_ThenCorrectSubTypeIdentifierReturned)
{
    const auto identifier_with_type_and_subtype = "274-50";
    const auto expected_subtype_identifier = "50";
    auto subtype = GetSignalSubType(identifier_with_type_and_subtype);
    EXPECT_EQ(expected_subtype_identifier, subtype);
}

TEST(UtilsTest, GivenUnexpectedIdentifier_WhenGetSubType_ThenReturnNull)
{
    const auto identifier_with_type_and_subtype = "274-50-10";
    EXPECT_EQ(std::nullopt, GetSignalSubType(identifier_with_type_and_subtype));
}

}  // namespace gtgen::core::environment::static_objects
