/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_ROADMARKINGPROPERTIESPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_ROADMARKINGPROPERTIESPROVIDER_H

#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Osi/road_marking_types.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <map>
#include <memory>
#include <string>

namespace gtgen::core::environment::static_objects
{

class RoadMarkingPropertiesProvider
{
  public:
    static std::unique_ptr<mantle_api::StaticObjectProperties> Get(const std::string& identifier);

  private:
    static std::unique_ptr<mantle_api::StaticObjectProperties> ToProperties(const std::string& identifier);
    static osi::OsiRoadMarkingsType GetOsiMarkingType(const std::string& identifier);
    static osi::OsiTrafficSignType GetOsiSignType(const std::string& identifier);
    static double GetSignalValue(const std::string& identifier);
    static osi::OsiTrafficSignValueUnit GetSignValueUnit(const std::string& identifier);

    using DataMap = std::map<std::string, std::pair<osi::OsiRoadMarkingsType, osi::OsiTrafficSignType>>;
    const static DataMap kStvoSignsToOsiTypes;
};

}  // namespace gtgen::core::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_ROADMARKINGPROPERTIESPROVIDER_H
