/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTPHASESTATE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTPHASESTATE_H

#include "Core/Environment/Entities/traffic_light_entity.h"

namespace gtgen::core::environment::controller
{

class TrafficLightStateMachine;

class TrafficLightPhaseState
{
  public:
    TrafficLightPhaseState(const mantle_api::TrafficLightPhase& traffic_light_phase,
                           entities::TrafficLightEntity* traffic_light_entity);
    TrafficLightPhaseState(TrafficLightPhaseState const&) = delete;
    TrafficLightPhaseState& operator=(const TrafficLightPhaseState&) = delete;
    TrafficLightPhaseState(TrafficLightPhaseState&&) = delete;
    TrafficLightPhaseState& operator=(TrafficLightPhaseState&&) = delete;
    virtual ~TrafficLightPhaseState() = default;

    virtual void Start();
    virtual bool HasPhaseFinished(mantle_api::Time current_simulation_time);

  private:
    const mantle_api::TrafficLightPhase& traffic_light_phase_;
    entities::TrafficLightEntity* traffic_light_entity_{nullptr};
};
}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTPHASESTATE_H
