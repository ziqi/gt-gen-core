/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"

#include "Core/Environment/Controller/Internal/Utilities/path_spline_fitting.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/LaneFollowing/find_longest_path.h"
#include "Core/Environment/LaneFollowing/line_follow_data_from_lanes.h"
#include "Core/Environment/LaneFollowing/line_follow_data_from_path.h"
#include "Core/Environment/PathFinding/path_finder.h"
#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"

namespace gtgen::core::environment::controller
{
using units::literals::operator""_m;

ControllerHelpers::ControllerHelpers(const map::LaneLocationProvider* lane_location_provider)
    : lane_location_provider_{lane_location_provider}
{
}

lanefollowing::PointDistanceList ControllerHelpers::ComputePointDistanceListFromPosition(
    mantle_api::Vec3<units::length::meter_t> position)
{
    const auto start_location = lane_location_provider_->GetLaneLocation(position);
    const auto longest_path =
        lanefollowing::FindLongestPath(lane_location_provider_->GetGtGenMap(), start_location.lanes);

    auto point_list =
        lanefollowing::CreateLineFollowDataFromLanes(longest_path, start_location, mantle_api::Direction::kForward);

    return point_list;
}

path_finding::Path ControllerHelpers::ComputeRoute(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints)
{
    path_finding::PathFinder path_finder(lane_location_provider_->GetGtGenMap());
    const auto path = path_finder.ComputeRoute(waypoints);

    if (!path.has_value())
    {
        throw EnvironmentException(
            "Cannot create PathControlUnit. No path was found for the given waypoints. This "
            "indicates the waypoints are not correctly defined in the scenario file");
    }

    auto info_text = fmt::format("PathControlUnit computed path from {} to {}", waypoints.front(), waypoints.back());
    Info(info_text);

    return path.value();
}

lanefollowing::PointDistanceList ControllerHelpers::ConvertTrajectoryIntoPointDistanceList(
    const mantle_api::Trajectory& trajectory)
{
    lanefollowing::PointDistanceList trajectory_points_list;
    if (std::holds_alternative<mantle_api::PolyLine>(trajectory.type))
    {
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory.type);

        if (poly_line.size() == 0)
        {
            throw environment::EnvironmentException{"Trajectory does not contain polyline points"};
        }
        for (std::size_t i = 0; i < poly_line.size(); ++i)
        {
            lanefollowing::PointDistanceListEntry trajectory_point_entry;
            trajectory_point_entry.point = (poly_line[i].pose.position);
            trajectory_point_entry.heading =
                service::glmwrapper::GetWorldSpaceForwardVector(poly_line[i].pose.orientation);
            trajectory_point_entry.distance_to_next_point = 0_m;
            if (i + 1 < poly_line.size())
            {
                trajectory_point_entry.distance_to_next_point = units::length::meter_t(
                    service::glmwrapper::Distance(poly_line[i + 1].pose.position, poly_line[i].pose.position));
            }

            trajectory_points_list.emplace_back(trajectory_point_entry);
        }
    }
    return trajectory_points_list;
}

lanefollowing::PointDistanceList ControllerHelpers::ComputePointDistanceListFromPath(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints)
{
    lanefollowing::PointDistanceList point_distance_list;

    if (lane_location_provider_ != nullptr)
    {
        if (waypoints.size() == 1)
        {

            point_distance_list = ComputePointDistanceListFromPosition(waypoints.front());
        }
        else
        {
            try
            {
                auto path = ComputeRoute(waypoints);
                auto start_location = lane_location_provider_->GetLaneLocation(waypoints.front());
                auto end_location = lane_location_provider_->GetLaneLocation(waypoints.back());
                point_distance_list = lanefollowing::CreateLineFollowDataFromPath(path, start_location, end_location);
            }
            catch (const EnvironmentException& e)
            {
                throw;
            }
        }
    }
    return SamplePathAsSpline(std::move(point_distance_list));
}

}  // namespace gtgen::core::environment::controller
