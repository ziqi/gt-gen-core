/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_position_derivatives.h"

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"
#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"
#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"
#include "Core/Service/Utility/derivative_utils.h"

namespace gtgen::core::environment::plausibility_check
{

using Velocity = units::velocity::meters_per_second_t;
using Acceleration = units::acceleration::meters_per_second_squared_t;

namespace detail
{
template <typename T>
mantle_api::Vec3<T> RotateVector(const mantle_api::Vec3<T>& mantle_vector,
                                 const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    using namespace gtgen::core::service::glmwrapper;

    const auto glm_vector = ToGlmVec3(mantle_vector);
    const auto rotation_matrix = glm::transpose(CreateRotationMatrix(orientation));
    const auto glm_rotated_vector = rotation_matrix * glm_vector;
    return mantle_api::Vec3<T>{T(glm_rotated_vector.x), T(glm_rotated_vector.y), T(glm_rotated_vector.z)};
}
}  // namespace detail

void PlausibilityCheckPositionDerivatives::ProcessAttributes()
{
    current_external_position_ = entity_->GetPosition();
    current_external_velocity_ = entity_->GetVelocity();
    local_current_external_velocity_ = detail::RotateVector(entity_->GetVelocity(), entity_->GetOrientation());
    current_external_acceleration_ = entity_->GetAcceleration();
    local_current_external_acceleration_ = detail::RotateVector(entity_->GetAcceleration(), entity_->GetOrientation());

    if (previous_external_position_.has_value())
    {
        current_internal_velocity_ = service::utility::GetPositionDerivativeVector<units::length::meter_t, Velocity>(
            current_external_position_, previous_external_position_.value(), elapsed_time_);
        local_current_internal_velocity_ = detail::RotateVector(*current_internal_velocity_, entity_->GetOrientation());
    }

    if (previous_internal_velocity_.has_value())
    {
        current_internal_acceleration_ = service::utility::GetPositionDerivativeVector<Velocity, Acceleration>(
            current_internal_velocity_.value(), previous_internal_velocity_.value(), elapsed_time_);
        local_current_internal_acceleration_ =
            detail::RotateVector(*current_internal_acceleration_, entity_->GetOrientation());
    }
}

void PlausibilityCheckPositionDerivatives::CompareInternalAndExternalAttributes() const
{

    if (current_internal_velocity_.has_value())
    {
        auto comparator = details::NotAlmostEqualEpsilon<Velocity>(thresholds_.velocity_epsilon);

        CompareAndLog<Velocity>(local_current_external_velocity_.x,
                                local_current_internal_velocity_->x,
                                comparator,
                                log_messages_.longitudinal_velocity_unexpected);
        CompareAndLog<Velocity>(local_current_external_velocity_.y,
                                local_current_internal_velocity_->y,
                                comparator,
                                log_messages_.lateral_velocity_unexpected);
        CompareAndLog<Velocity>(local_current_external_velocity_.z,
                                local_current_internal_velocity_->z,
                                comparator,
                                log_messages_.vertical_velocity_unexpected);
    }

    if (current_internal_acceleration_.has_value())
    {
        auto comparator = details::NotAlmostEqualEpsilon<Acceleration>(thresholds_.acceleration_epsilon);

        CompareAndLog<Acceleration>(local_current_external_acceleration_.x,
                                    local_current_internal_acceleration_->x,
                                    comparator,
                                    log_messages_.longitudinal_acceleration_unexpected);
        CompareAndLog<Acceleration>(local_current_external_acceleration_.y,
                                    local_current_internal_acceleration_->y,
                                    comparator,
                                    log_messages_.lateral_acceleration_unexpected);
    }
}

void PlausibilityCheckPositionDerivatives::CheckExternalVelocityPlausibility() const
{
    CompareAndLog<Velocity>(local_current_external_velocity_.x,
                            thresholds_.longitudinal_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.longitudinal_velocity_above_threshold);
    CompareAndLog<Velocity>(local_current_external_velocity_.x,
                            thresholds_.longitudinal_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.longitudinal_velocity_below_threshold);

    CompareAndLog<Velocity>(local_current_external_velocity_.y,
                            thresholds_.lateral_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.lateral_velocity_above_threshold);
    CompareAndLog<Velocity>(local_current_external_velocity_.y,
                            thresholds_.lateral_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.lateral_velocity_below_threshold);

    // we check the vertical velocity to make sure we don't have cars flying for a few steps
    CompareAndLog<Velocity>(local_current_external_velocity_.z,
                            thresholds_.vertical_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.vertical_velocity_above_threshold);
    CompareAndLog<Velocity>(local_current_external_velocity_.z,
                            thresholds_.vertical_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.vertical_velocity_below_threshold);
}

void PlausibilityCheckPositionDerivatives::CheckExternalAccelerationPlausibility() const
{
    CompareAndLog<Acceleration>(local_current_external_acceleration_.x,
                                thresholds_.longitudinal_acceleration_maximum,
                                std::greater<Acceleration>(),
                                log_messages_.longitudinal_acceleration_above_threshold);
    CompareAndLog<Acceleration>(local_current_external_acceleration_.x,
                                thresholds_.longitudinal_acceleration_minimum,
                                std::less<Acceleration>(),
                                log_messages_.longitudinal_acceleration_below_threshold);

    CompareAndLog<Acceleration>(local_current_external_acceleration_.y,
                                thresholds_.lateral_acceleration_maximum,
                                std::greater<Acceleration>(),
                                log_messages_.lateral_acceleration_above_threshold);
    CompareAndLog<Acceleration>(local_current_external_acceleration_.y,
                                thresholds_.lateral_acceleration_minimum,
                                std::less<Acceleration>(),
                                log_messages_.lateral_acceleration_below_threshold);
}

void PlausibilityCheckPositionDerivatives::CheckExternalAttributesPlausibility() const
{
    CheckExternalVelocityPlausibility();
    CheckExternalAccelerationPlausibility();
}

void PlausibilityCheckPositionDerivatives::UpdatePreviousAttributes()
{
    previous_external_position_ = current_external_position_;

    if (current_internal_velocity_.has_value())
    {
        previous_internal_velocity_ = current_internal_velocity_;
    }
}

}  // namespace gtgen::core::environment::plausibility_check
