/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/TrafficCommand/traffic_command_builder.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/convert_first_custom_traffic_command_of_host_vehicle_to_route.h"

#include <gtest/gtest.h>

#include <cstdint>

namespace gtgen::core::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_deg;

class HostVehicleInterfaceControlUnitTest : public testing::Test
{
  public:
    void WithInitializedSimpleMap()
    {
        map_ = test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach();
        lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*map_);
    }

  protected:
    std::unique_ptr<environment::map::GtGenMap> map_;

    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_;
    traffic_command::TrafficCommandBuilder traffic_command_builder_{};
};

TEST_F(HostVehicleInterfaceControlUnitTest, GivenHostVehicleInterface_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    WithInitializedSimpleMap();

    HostVehicleInterfaceControlUnit control_unit{std::vector<mantle_api::Vec3<units::length::meter_t>>{},
                                                 lane_location_provider_.get(),
                                                 &traffic_command_builder_};

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST_F(HostVehicleInterfaceControlUnitTest, GivenHostVehicleInterface_WhenClone_ThenCopyCreated)
{
    WithInitializedSimpleMap();

    auto control_unit_ptr =
        std::make_unique<HostVehicleInterfaceControlUnit>(std::vector<mantle_api::Vec3<units::length::meter_t>>{},
                                                          lane_location_provider_.get(),
                                                          &traffic_command_builder_);

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

TEST_F(HostVehicleInterfaceControlUnitTest,
       GivenHostVehicleInterfaceWithWaypoint_WhenConstructed_ThenHostInterfaceHasEmptyTrafficCommand)
{
    WithInitializedSimpleMap();

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints{
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}};

    HostVehicleInterfaceControlUnit control_unit{waypoints, lane_location_provider_.get(), &traffic_command_builder_};

    traffic_command_builder_.Step();
    EXPECT_TRUE(traffic_command_builder_.GetTrafficCommands().empty());
}

TEST_F(
    HostVehicleInterfaceControlUnitTest,
    GivenHostVehicleInterfaceAndWaypoints_WhenConstructed_ThenHostInterfaceHasHostVehiclePathAndCustomAndTrafficCommandIsSetAccordingly)
{
    const auto expected_path = std::vector<std::uint64_t>{3, 7, 11};
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    WithInitializedSimpleMap();

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints{
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    HostVehicleInterfaceControlUnit control_unit{waypoints, lane_location_provider_.get(), &traffic_command_builder_};

    traffic_command_builder_.Step();
    const auto actual_path = gtgen::core::test_utils::ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(
        traffic_command_builder_.GetTrafficCommands());
    EXPECT_EQ(expected_path, actual_path);
}

TEST_F(HostVehicleInterfaceControlUnitTest,
       GivenHostVehicleInterfaceAndInvalidWaypoints_WhenConstructed_ThenHostInterfaceControlUnitHasEmptyTrafficCommand)
{
    WithInitializedSimpleMap();

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints{
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{-100_m, 0_m, 0_m}};

    EXPECT_NO_THROW(
        HostVehicleInterfaceControlUnit(waypoints, lane_location_provider_.get(), &traffic_command_builder_));

    traffic_command_builder_.Step();
    EXPECT_TRUE(traffic_command_builder_.GetTrafficCommands().empty());
}

}  // namespace gtgen::core::environment::controller
