/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"

#include "Core/Environment/Entities/entity_container.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/Logging/logging.h"
#include "MantleAPI/Traffic/i_entity.h"
#include "osi_groundtruth.pb.h"
#include "osi_object.pb.h"
#include "osi_sensorview.pb.h"
#include "osi_trafficupdate.pb.h"

#include <MantleAPI/Traffic/entity_helper.h>
#include <boost/dll/import.hpp>
#include <fmt/format.h>

#include <chrono>

namespace gtgen::core::environment::controller
{

std::unique_ptr<proto_groundtruth::SensorViewBuilder> InitSensorViewBuilder(
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
{
    auto sensor_view_builder = std::make_unique<proto_groundtruth::SensorViewBuilder>(
        *traffic_participant_control_unit_config.gtgen_map,
        traffic_participant_control_unit_config.proto_ground_truth_builder_config.step_size,
        traffic_participant_control_unit_config.map_chunking);
    sensor_view_builder->Init();

    return sensor_view_builder;
}

boost::function<osi_traffic_participant::Create> TrafficParticipantControlUnit::ImportTrafficParticipantCreator(
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
{
    // Dynamically load TPM shared library
    fs::path so_file_path = FindPluginPath(traffic_participant_control_unit_config);
    return boost::dll::import_alias<osi_traffic_participant::Create>(
        so_file_path.c_str(), "CreateTpm", boost::dll::load_mode::rtld_now);
}

TrafficParticipantControlUnit::TrafficParticipantControlUnit(
    const std::map<std::string, std::string>& parameters,
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
    : TrafficParticipantControlUnit{parameters,
                                    traffic_participant_control_unit_config,
                                    ImportTrafficParticipantCreator(traffic_participant_control_unit_config)}
{
}

TrafficParticipantControlUnit::TrafficParticipantControlUnit(
    const TrafficParticipantControlUnit& traffic_participant_control_unit)
    : IAbstractControlUnit(traffic_participant_control_unit)
{
    this->traffic_participant_control_unit_config_ =
        traffic_participant_control_unit.traffic_participant_control_unit_config_;
    this->sensor_view_builder_ = InitSensorViewBuilder(traffic_participant_control_unit_config_);
    this->tpm_ = traffic_participant_control_unit.tpm_;
    this->tpm_creator_ = traffic_participant_control_unit.tpm_creator_;
}

TrafficParticipantControlUnit::TrafficParticipantControlUnit(
    const std::map<std::string, std::string>& parameters,
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config,
    const boost::function<osi_traffic_participant::Create> tpm_creator)
    : parameters_{parameters},
      traffic_participant_control_unit_config_{traffic_participant_control_unit_config},
      tpm_creator_{tpm_creator}
{
    sensor_view_builder_ = InitSensorViewBuilder(traffic_participant_control_unit_config_);
}

TrafficParticipantControlUnit::~TrafficParticipantControlUnit()
{
    tpm_.reset();
}

std::unique_ptr<IControlUnit> TrafficParticipantControlUnit::Clone() const
{
    return std::make_unique<TrafficParticipantControlUnit>(*this);
}

fs::path TrafficParticipantControlUnit::ResolvePluginsPath(fs::path& input_path,
                                                           const std::vector<std::string>& search_directories)
{
    std::vector<fs::path> search_dirs{};
    std::transform(search_directories.begin(),
                   search_directories.end(),
                   std::back_inserter(search_dirs),
                   [](const std::string& directory) { return fs::path(directory); });

    search_dirs.push_back(fs::current_path());

    service::file_system::ReplaceTildeWithAbsoluteHomeDirectoryPath(input_path);

    std::string not_found_files{};
    for (const auto& search_directory : search_dirs)
    {
        if (fs::exists(search_directory))
        {
            auto found_path = service::file_system::SearchRecursivelyInDirectory(input_path, search_directory);
            if (found_path.empty() || !fs::exists(found_path))
            {
                not_found_files.append(search_directory.string() + " (including all sub-folders)\n");
            }
            else
            {
                return fs::canonical(found_path);
            }
        }
    }

    throw EnvironmentException(
        "Input file path '{}' could not be resolved. The following locations have been considered:\n{}",
        input_path.string(),
        not_found_files);
}

void TrafficParticipantControlUnit::SetEntity(mantle_api::IEntity& entity)
{
    IAbstractControlUnit::SetEntity(entity);

    const std::string path_key{ExternalControllerConfigParamKeys::output_path_key};
    if (traffic_participant_control_unit_config_.output_settings)
    {
        parameters_[path_key] =
            traffic_participant_control_unit_config_.output_settings->GetEntityOutputFolder(entity.GetUniqueId());
    }

    tpm_ = tpm_creator_(static_cast<uint64_t>(entity_->GetUniqueId()), osi3::GroundTruth{}, parameters_);
}

void TrafficParticipantControlUnit::StepControlUnit()
{
    // Skip steps with a time delta of zero: the model cannot move if no time passes
    if (delta_time_ == decltype(delta_time_)(0))
    {
        return;
    }

    const auto* entity_repository =
        traffic_participant_control_unit_config_.proto_ground_truth_builder_config.entity_repository;
    sensor_view_builder_->Step(entity_repository->GetEntities(), entity_);
    std::uint64_t raw_milliseconds{static_cast<std::uint64_t>(delta_time_() * 1e3)};

    std::optional<osi3::TrafficCommand> entity_traffic_command = {};
    for (auto traffic_command : traffic_participant_control_unit_config_.traffic_command_builder->GetTrafficCommands())
    {
        if (traffic_command.traffic_participant_id().value() == static_cast<uint64_t>(entity_->GetUniqueId()))
        {
            entity_traffic_command = traffic_command;
            break;
        }
    }

    auto traffic_participant_result = tpm_->Update(
        std::chrono::milliseconds(raw_milliseconds), sensor_view_builder_->GetSensorView(), entity_traffic_command);

    for (const auto& internal_state : traffic_participant_result.traffic_update.internal_state())
    {
        if (internal_state.host_vehicle_id().value() == entity_->GetUniqueId())
            sensor_view_builder_->MergeHostVehicleData(internal_state);
    }

    UpdateEntity(traffic_participant_result.traffic_update);

    const std::string map_coordinates{environment::map::GetMapCoordinateString(
        entity_->GetPosition(), traffic_participant_control_unit_config_.gtgen_map->coordinate_converter.get())};
    plausibility_check_.CheckPlausibility(*entity_, delta_time_, map_coordinates);
}

void TrafficParticipantControlUnit::UpdateBaseEntityProperties(const osi3::MovingObject& moving_object)
{
    auto base = moving_object.base();

    const auto& position = base.position();
    entity_->SetPosition(service::gt_conversion::ToVec3Length(position));

    const auto& orientation = base.orientation();
    entity_->SetOrientation(service::gt_conversion::ToOrientation3(orientation));

    const auto& orientation_rate = base.orientation_rate();
    entity_->SetOrientationRate(service::gt_conversion::ToOrientation3Rate(orientation_rate));

    const auto& orientation_acceleration = base.orientation_acceleration();
    entity_->SetOrientationAcceleration(service::gt_conversion::ToOrientation3Acceleration(orientation_acceleration));

    const auto& acceleration = base.acceleration();
    entity_->SetAcceleration(service::gt_conversion::ToVec3Acceleration(acceleration));

    const auto& velocity = base.velocity();
    entity_->SetVelocity(service::gt_conversion::ToVec3Velocity(velocity));
}

void TrafficParticipantControlUnit::UpdateVehicleEntityProperties(const osi3::MovingObject& moving_object)
{
    auto vehicle_entity = entities::CastEntityTypeTo<mantle_api::IVehicle>(entity_);
    if (!vehicle_entity)
    {
        return;
    }

    if (moving_object.vehicle_attributes().has_steering_wheel_angle())
    {
        units::angle::radian_t steering_wheel_angle{moving_object.vehicle_attributes().steering_wheel_angle()};
        vehicle_entity->SetSteeringWheelAngle(steering_wheel_angle);
    }
}

void TrafficParticipantControlUnit::UpdateEntity(const osi3::TrafficUpdate& traffic_update)
{
    auto moving_object = traffic_update.update(0);

    UpdateBaseEntityProperties(moving_object);
    UpdateVehicleEntityProperties(moving_object);
}

bool TrafficParticipantControlUnit::HasFinished() const
{
    return false;
}
fs::path TrafficParticipantControlUnit::FindPluginPath(
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
{
    fs::path so_name = fmt::format("{}.so", traffic_participant_control_unit_config.name);
    return ResolvePluginsPath(so_name, traffic_participant_control_unit_config.plugins_paths.elements);
}

fs::path TrafficParticipantControlUnit::FindPluginPath() const
{
    return FindPluginPath(traffic_participant_control_unit_config_);
}

}  // namespace gtgen::core::environment::controller
