/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H

#include <OsiTrafficParticipant/i_traffic_participant_model.h>
#include <boost/dll/alias.hpp>
#include <osi_sensorview.pb.h>
#include <osi_trafficupdate.pb.h>

#include <map>
#include <memory>
#include <string>

namespace gtgen::core::environment::controller
{

using osi_traffic_participant::ITrafficParticipantModel;

class TpmExample : public ITrafficParticipantModel
{
  public:
    TpmExample() = default;

    TpmExample(TpmExample const&) = delete;
    TpmExample& operator=(const TpmExample&) = delete;
    TpmExample(TpmExample&&) = delete;
    TpmExample& operator=(TpmExample&&) = delete;

    /// @brief Destructor.
    ~TpmExample() override;

    static std::shared_ptr<ITrafficParticipantModel> Create(uint64_t entity_id,
                                                            const osi3::GroundTruth& ground_truth_init,
                                                            const std::map<std::string, std::string>& parameters);

    /// @brief Request is sent to the simulator. (minimum) cycle time can be requested, but a smaller
    ///        cycle time can be set by the simulator.
    std::optional<osi3::SensorViewConfiguration> GetSensorViewConfigurationRequest() const override;

    /// @brief Response of the simulator to the model with the actual sensor view configuration
    /// @param sensor_view_config osi3::SensorViewConfiguration
    void SetSensorViewConfiguration(osi3::SensorViewConfiguration sensor_view_config) override;

    /// @brief Identifies the OSI entity from osi3::GroundTruth entities in the provided \p sensor_view and
    ///        provides a new state.
    /// @param delta_time  Delta time between the step in milliseconds.
    /// @param sensor_view osi3::SensorView containing osi3::GroundTruth containing all the entities
    ///                    (including the controlled entity).
    osi_traffic_participant::TrafficParticipantUpdateResult Update(
        std::chrono::milliseconds delta_time,
        const osi3::SensorView& sensor_view,
        const std::optional<osi3::TrafficCommand>& traffic_command) override;
};

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-avoid-non-const-global-variables,misc-definitions-in-headers)
BOOST_DLL_ALIAS(TpmExample::Create, CreateTpm)

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H
