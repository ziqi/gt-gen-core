/*******************************************************************************
 * Copyright (c) 2023-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h"

#include "osi_groundtruth.pb.h"
#include "osi_hostvehicledata.pb.h"
#include "osi_object.pb.h"
#include "osi_sensorview.pb.h"
#include "osi_trafficupdate.pb.h"

#include <gtest/gtest.h>

#include <cstdint>

namespace gtgen::core::environment::controller
{

class TrafficParticipantModelExampleTest : public testing::Test
{
  public:
    TrafficParticipantModelExampleTest()
    {
        SetHostVehicleId(1);
        auto* new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();
        new_moving_object->mutable_base()->mutable_position()->set_x(0.0);

        new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();
        new_moving_object->mutable_base()->mutable_position()->set_x(10.0);
    }

    const osi3::GroundTruth& GetGroundTruth() const { return sensor_view_.global_ground_truth(); }

    osi3::GroundTruth& GetMutableGroundTruth() { return *sensor_view_.mutable_global_ground_truth(); }

    const osi3::SensorView& GetSensorView() const { return sensor_view_; }

    void SetHostVehicleId(std::uint64_t id) { sensor_view_.mutable_host_vehicle_id()->set_value(id); }

  private:
    osi3::SensorView sensor_view_;
};

TEST_F(TrafficParticipantModelExampleTest, GivenTpm_WhenUpdate_ThenTrafficUpdateHasCorrectValues)
{
    TpmExample tpm_example{};

    auto traffic_update_result =
        tpm_example.Update(std::chrono::milliseconds(0), GetSensorView(), osi3::TrafficCommand{});

    const double expected_steering_wheel_angle{3.14};

    const osi3::TrafficUpdate& traffic_update = traffic_update_result.traffic_update;
    ASSERT_EQ(1, traffic_update.update_size());
    ASSERT_EQ(1, traffic_update.internal_state_size());

    // update
    const osi3::MovingObject& update = traffic_update.update(0);
    EXPECT_EQ(update.base().position().x(), 123.45);
    EXPECT_EQ(update.vehicle_attributes().steering_wheel_angle(), expected_steering_wheel_angle);

    // internal state
    const osi3::HostVehicleData host_vehicle_data = traffic_update.internal_state(0);
    EXPECT_EQ(host_vehicle_data.host_vehicle_id().value(), 1);
    EXPECT_EQ(host_vehicle_data.vehicle_steering().vehicle_steering_wheel().angle(), expected_steering_wheel_angle);
}

}  // namespace gtgen::core::environment::controller
