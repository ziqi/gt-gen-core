/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERFACTORY_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERFACTORY_H

#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"
#include "Core/Environment/Controller/Internal/external_controller_factory.h"
#include "Core/Environment/Controller/Internal/internal_controller_factory.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Exception/exception.h"

#include <MantleAPI/Traffic/i_controller_config.h>

#include <memory>

namespace gtgen::core::environment::controller
{
class ControllerFactory
{
  public:
    template <typename T = controller::CompositeController>
    static std::unique_ptr<T> Create(
        mantle_api::UniqueId id,
        std::unique_ptr<mantle_api::IControllerConfig> config,
        mantle_api::DefaultRoutingBehavior default_routing_behavior = mantle_api::DefaultRoutingBehavior::kStop)
    {
        auto composite_controller = std::make_unique<T>(id, config->map_query_service);
        composite_controller->SetDefaultRoutingBehavior(default_routing_behavior);
        composite_controller->SetName(config->name);

        if (auto* external_config = dynamic_cast<mantle_api::ExternalControllerConfig*>(config.get()))
        {
            composite_controller->SetType(CompositeController::Type::kExternal);
            ExternalControllerFactory::Create(composite_controller.get(), external_config);
        }
        else if (auto* internal_config = dynamic_cast<mantle_api::InternalControllerConfig*>(config.get()))
        {
            composite_controller->SetType(CompositeController::Type::kInternal);
            InternalControllerFactory::Create(composite_controller.get(), internal_config);
        }
        else if (dynamic_cast<mantle_api::NoOpControllerConfig*>(config.get()) != nullptr)
        {
            composite_controller->AddControlUnit(std::make_unique<controller::NoOpControlUnit>());
        }
        else
        {
            throw EnvironmentException(
                "Tried to create a controller with an unsupported configuration. Please contact GTGen Support for "
                "further assistance.");
        }

        return composite_controller;
    }
};
}  // namespace gtgen::core::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERFACTORY_H
