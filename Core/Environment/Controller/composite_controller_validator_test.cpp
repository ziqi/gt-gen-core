/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/composite_controller_validator.h"

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "osi_groundtruth.pb.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{

class CompositeControllerValidatorTest : public testing::Test
{
  public:
    controller::TrafficParticipantControlUnitConfig CreateTpmControlConfig()
    {
        controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
        gtgen_map_ = std::move(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());
        tpm_control_unit_config.gtgen_map = gtgen_map_.get();
        tpm_control_unit_config.name = "traffic_participant_model_example";

        return tpm_control_unit_config;
    }

    void WithVelocityControlUnit()
    {
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> splines;
        auto control_unit = std::make_unique<VelocityControlUnit>(splines);
        control_units_.push_back(std::move(control_unit));
    }

    void WithKeepVelocityControlUnit()
    {
        auto control_unit = std::make_unique<KeepVelocityControlUnit>();
        control_units_.push_back(std::move(control_unit));
    }

    void WithLaneOffsetControlUnit()
    {
        auto control_unit = std::make_unique<LaneOffsetControlUnit>(nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithLaneChangeControlUnit()
    {
        auto control_unit =
            std::make_unique<LaneChangeControlUnit>(mantle_api::PerformLaneChangeControlStrategy{}, nullptr, nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithVehicleModelControlUnit()
    {
        host::HostVehicleModel vehicle_model;
        auto control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithPathControlUnit()
    {
        auto control_unit = std::make_unique<PathControlUnit>(nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithManeuverControlUnit()
    {
        std::vector<mantle_api::SplineSection<units::angle::radian>> splines;
        auto control_unit = std::make_unique<ManeuverControlUnit>(splines, nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithTpmControlUnit()
    {
        std::map<std::string, std::string> tpm_params{{"velocity", "22"}};
        auto control_unit = std::make_unique<TrafficParticipantControlUnit>(tpm_params, CreateTpmControlConfig());
        control_units_.push_back(std::move(control_unit));
    }

    void WithHostVehicleInterfaceControlUnit()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
        auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
        map::LaneLocationProvider lane_location_provider{*map};
        auto control_unit =
            std::make_unique<HostVehicleInterfaceControlUnit>(waypoints, &lane_location_provider, nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithRecoveryControlUnit()
    {
        auto control_unit = std::make_unique<RecoveryControlUnit>(nullptr, nullptr);
        control_units_.push_back(std::move(control_unit));
    }

    void WithFollowTrajectoryWithSpeedControlUnit()
    {
        const mantle_api::Trajectory& trajectory{
            "", mantle_api::PolyLine{mantle_api::PolyLinePoint()}, mantle_api::TrajectoryReferencePoint::kRearAxle};

        mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference{
            mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference()};

        auto control_unit = std::make_unique<FollowTrajectoryWithSpeedControlUnit>(trajectory, time_reference);
        control_units_.push_back(std::move(control_unit));
    }

    void WithFollowTrajectoryControlUnit()
    {
        const mantle_api::Trajectory& trajectory{
            "", mantle_api::PolyLine{mantle_api::PolyLinePoint()}, mantle_api::TrajectoryReferencePoint::kRearAxle};
        auto control_unit = std::make_unique<FollowTrajectoryControlUnit>(nullptr, trajectory);
        control_units_.push_back(std::move(control_unit));
    }

    void WithAllControlUnits()
    {
        WithVelocityControlUnit();
        WithKeepVelocityControlUnit();
        WithLaneOffsetControlUnit();
        WithLaneChangeControlUnit();
        WithVehicleModelControlUnit();
        WithPathControlUnit();
        WithManeuverControlUnit();
        WithTpmControlUnit();
        WithHostVehicleInterfaceControlUnit();
        WithFollowTrajectoryControlUnit();
        WithFollowTrajectoryWithSpeedControlUnit();
        WithRecoveryControlUnit();
    }

  protected:
    std::vector<std::unique_ptr<IControlUnit>> control_units_;
    std::unique_ptr<map::GtGenMap> gtgen_map_;
};

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithVelocityControlUnit_ThenLongitudinalControlUnitsAreDeleted)
{
    WithAllControlUnits();

    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> splines;
    auto control_unit = std::make_unique<VelocityControlUnit>(splines);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_FALSE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithKeepVelocityControlUnit_ThenLongitudinalControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<KeepVelocityControlUnit>();
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_FALSE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithLaneOffsetControlUnit_ThenLateralControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<LaneOffsetControlUnit>(nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithLaneChangeControlUnit_ThenPositionUpdateAndLateralControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto control_unit =
        std::make_unique<LaneChangeControlUnit>(mantle_api::PerformLaneChangeControlStrategy{}, nullptr, nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithVehicleModelControlUnit_ThenPositionUpdateControlUnitsAreDeleted)
{
    WithAllControlUnits();

    host::HostVehicleModel vehicle_model;
    auto control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithPathControlUnit_ThenPositionUpdateControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<PathControlUnit>(nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithManeuverControlUnit_ThenPositionUpdateControlUnitsAreDeleted)
{
    WithAllControlUnits();

    std::vector<mantle_api::SplineSection<units::angle::radian>> splines;
    auto control_unit = std::make_unique<ManeuverControlUnit>(splines, nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithTpmControlUnit_ThenPositionUpdateControlUnitsAreDeleted)
{
    WithAllControlUnits();

    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};
    auto control_unit = std::make_unique<TrafficParticipantControlUnit>(tpm_params, CreateTpmControlConfig());
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_FALSE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithHostVehicleInterfaceControlUnit_ThenNoControlUnitsAreDeleted)
{
    WithAllControlUnits();

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    auto control_unit = std::make_unique<HostVehicleInterfaceControlUnit>(waypoints, &lane_location_provider, nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_FALSE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithRecoveryControlUnit_ThenNoControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<RecoveryControlUnit>(nullptr, nullptr);
    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithFollowTrajectoryControlUnit_ThenLateralAndRoutingControlUnitsAreDeleted)
{
    WithAllControlUnits();

    const mantle_api::Trajectory& trajectory{
        "", mantle_api::PolyLine{mantle_api::PolyLinePoint()}, mantle_api::TrajectoryReferencePoint::kRearAxle};

    auto control_unit = std::make_unique<FollowTrajectoryControlUnit>(nullptr, trajectory);

    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_TRUE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
}

TEST_F(
    CompositeControllerValidatorTest,
    GivenAllControlUnits_WhenDeleteConflictingControlUnitsWithFollowTrajectoryWithSpeedControlUnit_ThenLongitudinalLateralAndRoutingControlUnitsAreDeleted)
{
    WithAllControlUnits();

    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};

    const mantle_api::Trajectory& trajectory{
        "", mantle_api::PolyLine{mantle_api::PolyLinePoint()}, mantle_api::TrajectoryReferencePoint::kRearAxle};
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference{
        mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference()};
    auto control_unit = std::make_unique<FollowTrajectoryWithSpeedControlUnit>(trajectory, time_reference);

    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), control_units_, 0);

    EXPECT_FALSE(ContainsController<VelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<KeepVelocityControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneOffsetControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<LaneChangeControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<VehicleModelControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<PathControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<ManeuverControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<TrafficParticipantControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<HostVehicleInterfaceControlUnit>(control_units_));
    EXPECT_TRUE(ContainsController<RecoveryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryControlUnit>(control_units_));
    EXPECT_FALSE(ContainsController<FollowTrajectoryWithSpeedControlUnit>(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenAllControlUnits_WhenInsertVelocityControlUnit_ThenControlUnitAtBeginning)
{
    WithAllControlUnits();
    control_units_.erase(control_units_.begin());  // delete VelocityControlUnit

    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> splines;
    auto control_unit = std::make_unique<VelocityControlUnit>(splines);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    EXPECT_TRUE(dynamic_cast<VelocityControlUnit*>(control_units_.begin()->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertKeepVelocityControlUnit_ThenControlUnitAtBeginning)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<KeepVelocityControlUnit>();
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    EXPECT_TRUE(dynamic_cast<KeepVelocityControlUnit*>(control_units_.begin()->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertLaneOffsetControlUnit_ThenControlUnitAtBeginning)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<LaneOffsetControlUnit>(nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    EXPECT_TRUE(dynamic_cast<LaneOffsetControlUnit*>(control_units_.begin()->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertLaneChangeControlUnit_ThenControlUnitBeforeRecoveryControlUnit)
{
    WithAllControlUnits();

    auto control_unit =
        std::make_unique<LaneChangeControlUnit>(mantle_api::PerformLaneChangeControlStrategy{}, nullptr, nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    auto it = control_units_.end() - 2;
    EXPECT_TRUE(dynamic_cast<LaneChangeControlUnit*>(it->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertVehicleModelControlUnit_ThenControlUnitBeforeRecoveryControlUnit)
{
    WithAllControlUnits();

    host::HostVehicleModel vehicle_model;
    auto control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    auto it = control_units_.end() - 2;
    EXPECT_TRUE(dynamic_cast<VehicleModelControlUnit*>(it->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertPathControlUnit_ThenControlUnitBeforeRecoveryControlUnit)
{
    WithAllControlUnits();

    auto control_unit = std::make_unique<PathControlUnit>(nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    auto it = control_units_.end() - 2;
    EXPECT_TRUE(dynamic_cast<PathControlUnit*>(it->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertManeuverControlUnit_ThenControlUnitBeforeRecoveryControlUnit)
{
    WithAllControlUnits();

    std::vector<mantle_api::SplineSection<units::angle::radian>> splines;
    auto control_unit = std::make_unique<ManeuverControlUnit>(splines, nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    auto it = control_units_.end() - 2;
    EXPECT_TRUE(dynamic_cast<ManeuverControlUnit*>(it->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest,
       GivenAllControlUnits_WhenInsertTpmControlUnit_ThenControlUnitBeforeRecoveryControlUnit)
{
    WithAllControlUnits();

    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};
    auto control_unit = std::make_unique<TrafficParticipantControlUnit>(tpm_params, CreateTpmControlConfig());
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    auto it = control_units_.end() - 2;
    EXPECT_TRUE(dynamic_cast<TrafficParticipantControlUnit*>(it->get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest, GivenAllControlUnits_WhenInsertRecoveryControlUnit_ThenControlUnitAtEnd)
{
    WithAllControlUnits();
    control_units_.erase(control_units_.end());  // delete RecoveryControlUnit

    auto control_unit = std::make_unique<RecoveryControlUnit>(nullptr, nullptr);
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), control_units_, 0);

    EXPECT_TRUE(dynamic_cast<RecoveryControlUnit*>(control_units_.back().get()) != nullptr);
}

TEST_F(CompositeControllerValidatorTest, GivenVelocityControlUnit_WhenNeedsPathControlUnit_ThenReturnTrue)
{
    WithVelocityControlUnit();

    EXPECT_TRUE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenKeepVelocityControlUnit_WhenNeedsPathControlUnit_ThenReturnTrue)
{
    WithKeepVelocityControlUnit();

    EXPECT_TRUE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenLaneOffsetControlUnit_WhenNeedsPathControlUnit_ThenReturnTrue)
{
    WithLaneOffsetControlUnit();

    EXPECT_TRUE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenLaneChangeControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithLaneChangeControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenVehicleModelControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithVehicleModelControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenPathControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithPathControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenManeuverControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithManeuverControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenTpmControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithTpmControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenHostVehicleInterfaceControlUnit_WhenNeedsPathControlUnit_ThenReturnTrue)
{
    WithHostVehicleInterfaceControlUnit();

    EXPECT_TRUE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenRecoveryControlUnit_WhenNeedsPathControlUnit_ThenReturnTrue)
{
    WithRecoveryControlUnit();

    EXPECT_TRUE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest,
       GivenFollowTrajectoryWithSpeedControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithFollowTrajectoryWithSpeedControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

TEST_F(CompositeControllerValidatorTest, GivenFollowTrajectoryControlUnit_WhenNeedsPathControlUnit_ThenReturnFalse)
{
    WithFollowTrajectoryControlUnit();

    EXPECT_FALSE(CompositeControllerValidator::NeedsPathControlUnit(control_units_));
}

}  // namespace gtgen::core::environment::controller
