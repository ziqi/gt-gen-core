/*******************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <gtest/gtest.h>

#include <string>

namespace gtgen::core::environment::traffic_command
{
using units::literals::operator""_mps;
using units::literals::operator""_m;
using units::literals::operator""_rad;

using ::testing::HasSubstr;
using ::testing::Not;

class TrafficActionBuilderTest : public testing::Test
{
  protected:
    entities::VehicleEntity vehicle_entity_{0, "host"};
    std::unique_ptr<map::GtGenMap> gtgen_map_{test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints()};
    map::LaneLocationProvider lane_location_provider_{*gtgen_map_};
};

class FollowTrajectoryControlStrategyTest : public TrafficActionBuilderTest
{
  protected:
    void AssertFollowTrajectoryCustomActionResult(const std::optional<osi3::TrafficAction>& result,
                                                  const std::string& expected_result)
    {
        ASSERT_TRUE(result.has_value());
        ASSERT_TRUE(result.value().has_custom_action());

        const std::string result_command_type = result.value().custom_action().command_type();
        ASSERT_EQ(result_command_type, "follow_clothoid_spline_action");

        const std::string result_command = result.value().custom_action().command();
        ASSERT_EQ(result_command, expected_result);
    }

    mantle_api::FollowTrajectoryControlStrategy control_strategy_;
    mantle_api::ClothoidSpline clothoid_spline_;
    mantle_api::PolyLine polyline_;
};

struct TrajectoryReferencePointTestType
{
    mantle_api::TrajectoryReferencePoint reference_point;
    std::string expected_reference_point_str;
};

class FollowTrajectoryControlStrategyTestWithParams
    : public FollowTrajectoryControlStrategyTest,
      public ::testing::WithParamInterface<TrajectoryReferencePointTestType>
{
};

TEST_F(TrafficActionBuilderTest, GivenUnsupportedControlStrategy_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::KeepVelocityControlStrategy control_strategy;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    EXPECT_FALSE(result.has_value());
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowVelocitySplineControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithSpeedActionReturned)
{
    mantle_api::FollowVelocitySplineControlStrategy control_strategy;
    control_strategy.default_value = 10_mps;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_speed_action());
    EXPECT_EQ(control_strategy.default_value(), result.value().speed_action().absolute_target_speed());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenPerformLaneChangeControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneChangeActionReturned)
{
    mantle_api::PerformLaneChangeControlStrategy control_strategy;
    control_strategy.target_lane_id = -3;  // shift 1 lane right

    vehicle_entity_.SetPosition(mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m});  // on local lane -2

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_change_action());
    EXPECT_EQ(1, result.value().lane_change_action().relative_target_lane());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenAcquireLaneOffsetControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneOffsetActionReturned)
{
    mantle_api::AcquireLaneOffsetControlStrategy control_strategy;
    control_strategy.offset = 1_m;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_offset_action());
    EXPECT_EQ(control_strategy.offset(), result.value().lane_offset_action().target_lane_offset());
}

TEST_F(FollowTrajectoryControlStrategyTest,
       GivenFollowTrajectoryControlStrategyWithTimeReference_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference;
    control_strategy_.timeReference = time_reference;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_);

    ASSERT_FALSE(result.has_value());
}

TEST_F(FollowTrajectoryControlStrategyTest,
       GivenFollowTrajectoryControlStrategyWithoutTrajectory_WhenCallConvertToTrafficAction_ThenThrow)
{
    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_));
}

TEST_F(FollowTrajectoryControlStrategyTest,
       GivenFollowTrajectoryControlStrategyWithoutPolylinePoints_WhenCallConvertToTrafficAction_ThenThrow)
{
    control_strategy_.trajectory.type = polyline_;

    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_));
}

INSTANTIATE_TEST_SUITE_P(
    ClothoidSplineWithDifferentReferencePoints,
    FollowTrajectoryControlStrategyTestWithParams,
    ::testing::Values(
        TrajectoryReferencePointTestType{mantle_api::TrajectoryReferencePoint::kUndefined, "refPointUndefined"},
        TrajectoryReferencePointTestType{mantle_api::TrajectoryReferencePoint::kRearAxle, "refPointRearAxle"},
        TrajectoryReferencePointTestType{mantle_api::TrajectoryReferencePoint::kFrontAxle, "refPointFrontAxle"},
        TrajectoryReferencePointTestType{mantle_api::TrajectoryReferencePoint::kCenterOfMass, "refPointCenterOfMass"},
        TrajectoryReferencePointTestType{mantle_api::TrajectoryReferencePoint::kBoundingBoxCenter,
                                         "refPointBoundingBoxCenter"}));

TEST_P(
    FollowTrajectoryControlStrategyTestWithParams,
    GivenFollowTrajectoryControlStrategyWithoutClothoidSplineSegments_WhenCallConvertToTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    control_strategy_.trajectory.type = clothoid_spline_;
    control_strategy_.trajectory.reference = GetParam().reference_point;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_);

    const std::string expected_result{"{\"trajectory\":{\"clothoidSpline\":null},\"referencePoint\":\"" +
                                      GetParam().expected_reference_point_str + "\"}"};

    AssertFollowTrajectoryCustomActionResult(result, expected_result);
}

TEST_F(
    FollowTrajectoryControlStrategyTest,
    GivenFollowTrajectoryControlStrategyWithClothoidSplineSegement_WhenCallConvertToTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    const units::curve::curvature_t expected_curvature_end{1.1};
    const units::curve::curvature_t expected_curvature_start{2.2};
    const units::angle::radian_t expected_h_offset{3.3};
    const units::length::meter_t expected_length{4.4};

    const mantle_api::ClothoidSplineSegment clothoid_spline_segment{expected_curvature_end,
                                                                    expected_curvature_start,
                                                                    expected_h_offset,
                                                                    expected_length,
                                                                    std::optional<mantle_api::Pose>{}};

    clothoid_spline_.segments.push_back(clothoid_spline_segment);
    control_strategy_.trajectory.type = clothoid_spline_;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_);

    const std::string expected_result{
        "{\"trajectory\":{\"clothoidSpline\":{\"segments\":[{\"curvatureEnd\":1.1,\"curvatureStart\":2.2,\"hOffset\":3."
        "3,\"length\":4.4}]}},\"referencePoint\":\"refPointUndefined\"}"};

    AssertFollowTrajectoryCustomActionResult(result, expected_result);
}

TEST_F(
    FollowTrajectoryControlStrategyTest,
    GivenFollowTrajectoryControlStrategyWithClothoidSplineSegementsStartPosition_WhenCallConvertToTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    const mantle_api::Vec3<units::length::meter_t> expected_position{
        units::length::meter_t{1.1}, units::length::meter_t{2.2}, units::length::meter_t{3.3}};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        units::angle::radian_t{4.4}, units::angle::radian_t{5.5}, units::angle::radian_t{6.6}};

    mantle_api::ClothoidSplineSegment clothoid_spline_segment{};
    clothoid_spline_segment.position_start = mantle_api::Pose{expected_position, expected_orientation};

    clothoid_spline_.segments.push_back(clothoid_spline_segment);
    control_strategy_.trajectory.type = clothoid_spline_;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_);

    const std::string expected_result{
        "{\"trajectory\":{\"clothoidSpline\":{\"segments\":[{\"curvatureEnd\":0.0,\"curvatureStart\":0.0,\"hOffset\":0."
        "0,\"length\":0.0,\"positionStart\":{\"x\":1.1,\"y\":2.2,\"z\":3.3,\"yaw\":4.4,\"pitch\":5.5,\"roll\":6.6}}]}},"
        "\"referencePoint\":\"refPointUndefined\"}"};

    AssertFollowTrajectoryCustomActionResult(result, expected_result);
}

TEST_F(FollowTrajectoryControlStrategyTest,
       GivenFollowTrajectoryControlStrategyWithOnePoint_WhenCallConvertToTrafficAction_ThenThrow)
{
    auto point1 = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m};
    polyline_.push_back({{point1, {0_rad, 0_rad, 0_rad}}, {}});
    control_strategy_.trajectory.type = polyline_;

    EXPECT_ANY_THROW(
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_));
}

TEST_F(
    FollowTrajectoryControlStrategyTest,
    GivenFollowTrajectoryControlStrategyWithoutTimeWithPoints_WhenCallConvertToTrafficAction_ThenTrafficActionWithFollowPathActionReturned)
{
    auto point1 = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m};
    auto point2 = mantle_api::Vec3<units::length::meter_t>{4_m, 5_m, 6_m};
    polyline_.push_back({{point1, {0_rad, 0_rad, 0_rad}}, {}});
    polyline_.push_back({{point2, {0_rad, 0_rad, 0_rad}}, {}});
    control_strategy_.trajectory.type = polyline_;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy_);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_follow_path_action());
    ASSERT_EQ(result.value().follow_path_action().path_point().size(), 2);
    EXPECT_TRIPLE(result.value().follow_path_action().path_point()[0].position(), point1);
    EXPECT_TRIPLE(result.value().follow_path_action().path_point()[1].position(), point2);
}

TEST_F(TrafficActionBuilderTest,
       GivenVehicleLightStatesControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    mantle_api::VehicleLightStatesControlStrategy control_strategy;
    control_strategy.light_type = mantle_api::VehicleLightType::kIndicatorLeft;
    control_strategy.light_state = {mantle_api::LightMode::kFlashing};

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    std::string expected_command_type = "light_state_action";
    std::string expected_command =
        "{\"lightType\":{\"vehicleLight\":\"indicatorLeft\"},\"lightState\":{\"mode\":\"flashing\"}}";

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_custom_action());
    EXPECT_EQ(expected_command_type, result.value().custom_action().command_type());
    EXPECT_EQ(expected_command, result.value().custom_action().command());
}

TEST_F(TrafficActionBuilderTest,
       GivenCommandTypeAndCommand_WhenCallCreateCustomTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    std::string command_type = "route";
    std::string command = "5,6,7,8";
    auto result = TrafficActionBuilder::CreateCustomTrafficAction(command_type, command);

    ASSERT_TRUE(result.has_custom_action());
    EXPECT_EQ(command_type, result.custom_action().command_type());
    EXPECT_EQ(command, result.custom_action().command());
}

}  // namespace gtgen::core::environment::traffic_command
