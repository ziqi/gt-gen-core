/*******************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/TrafficCommand/json_types_conversion.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/trajectory.h>
#include <nlohmann/json.hpp>

namespace gtgen::core::environment::traffic_command
{

std::optional<osi3::TrafficAction> TrafficActionBuilder::ConvertToTrafficAction(
    const mantle_api::IEntity& i_entity,
    const map::LaneLocationProvider* lane_location_provider,
    mantle_api::ControlStrategy* control_strategy)
{
    if (auto* follow_velocity_spline_control_strategy =
            dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategy))
    {
        osi3::TrafficAction traffic_action;
        auto speed_action = traffic_action.mutable_speed_action();
        speed_action->set_absolute_target_speed(follow_velocity_spline_control_strategy->default_value());
        return traffic_action;
    }
    if (auto* acquire_lane_offset_control_strategy =
            dynamic_cast<mantle_api::AcquireLaneOffsetControlStrategy*>(control_strategy))
    {
        osi3::TrafficAction traffic_action;
        auto lane_offset_action = traffic_action.mutable_lane_offset_action();
        lane_offset_action->set_target_lane_offset(acquire_lane_offset_control_strategy->offset());
        return traffic_action;
    }
    if (auto* perform_lane_change_control_strategy =
            dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategy))
    {
        const auto entity_position = i_entity.GetPosition();
        const std::optional<int> relative_lane_target = lane_location_provider->GetRelativeLaneTarget(
            entity_position, perform_lane_change_control_strategy->target_lane_id);
        if (relative_lane_target.has_value())
        {
            osi3::TrafficAction traffic_action;
            auto lane_change_action = traffic_action.mutable_lane_change_action();
            // relative_lane_target is negated in OSI. Positive is to the right.
            lane_change_action->set_relative_target_lane(relative_lane_target.value() * -1);
            return traffic_action;
        }
    }
    if (auto* follow_trajectory_control_strategy =
            dynamic_cast<mantle_api::FollowTrajectoryControlStrategy*>(control_strategy);
        follow_trajectory_control_strategy != nullptr &&
        !follow_trajectory_control_strategy->timeReference.has_value())  // trajectory without time/speed
    {
        if (std::holds_alternative<mantle_api::PolyLine>(follow_trajectory_control_strategy->trajectory.type))
        {
            osi3::TrafficAction traffic_action;
            auto follow_path_action = traffic_action.mutable_follow_path_action();
            FillPathPoints(follow_path_action, follow_trajectory_control_strategy);
            return traffic_action;
        }

        if (std::holds_alternative<mantle_api::ClothoidSpline>(follow_trajectory_control_strategy->trajectory.type))
        {
            return FillCustomFollowClothoidSplineAction(follow_trajectory_control_strategy);
        }
    }
    if (auto* vehicle_light_states_control_strategy =
            dynamic_cast<mantle_api::VehicleLightStatesControlStrategy*>(control_strategy))
    {
        return FillCustomLightStateAction(vehicle_light_states_control_strategy);
    }
    Debug("Conversion of control strategy {} to traffic command for external agent not yet supported.",
          control_strategy->type);
    return std::nullopt;
}

osi3::TrafficAction TrafficActionBuilder::CreateCustomTrafficAction(const std::string& command_type,
                                                                    const std::string& command)
{
    osi3::TrafficAction traffic_action;
    auto custom_action = traffic_action.mutable_custom_action();
    custom_action->set_command_type(command_type);
    custom_action->set_command(command);
    return traffic_action;
}

void TrafficActionBuilder::FillPathPoints(
    osi3::TrafficAction_FollowPathAction* follow_path_action,
    mantle_api::FollowTrajectoryControlStrategy* follow_trajectory_control_strategy)
{
    if (!std::holds_alternative<mantle_api::PolyLine>(follow_trajectory_control_strategy->trajectory.type))
    {
        throw environment::EnvironmentException{"TrafficActionBuilder: Trajectory does not contain polyline"};
    }

    const auto& poly_line = std::get<mantle_api::PolyLine>(follow_trajectory_control_strategy->trajectory.type);

    if (poly_line.size() < 2)
    {
        throw environment::EnvironmentException{
            "TrafficActionBuilder: Trajectory polyline contains less than two points"};
    }
    for (std::size_t i = 0; i < poly_line.size(); ++i)
    {
        auto path_point = follow_path_action->add_path_point();
        service::gt_conversion::FillProtoObject(poly_line[i].pose.position, path_point->mutable_position());
    }
}

osi3::TrafficAction TrafficActionBuilder::FillCustomLightStateAction(
    mantle_api::VehicleLightStatesControlStrategy* vehicle_light_states_control_strategy)
{
    nlohmann::ordered_json node = *vehicle_light_states_control_strategy;
    return CreateCustomTrafficAction("light_state_action", node.dump());
}

osi3::TrafficAction TrafficActionBuilder::FillCustomFollowClothoidSplineAction(
    const mantle_api::FollowTrajectoryControlStrategy* follow_trajectory_control_strategy)
{
    if (!std::holds_alternative<mantle_api::ClothoidSpline>(follow_trajectory_control_strategy->trajectory.type))
    {
        throw environment::EnvironmentException{
            "TrafficActionBuilder: Trajectory does not contain clothoid spline to create "
            "CustomFollowClothoidSplineAction"};
    }

    nlohmann::ordered_json node = *follow_trajectory_control_strategy;
    return CreateCustomTrafficAction("follow_clothoid_spline_action", node.dump());
}

}  // namespace gtgen::core::environment::traffic_command
