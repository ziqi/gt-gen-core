/*******************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "osi_trafficcommand.pb.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <optional>
#include <string>

namespace gtgen::core::environment::traffic_command
{

class TrafficActionBuilder
{
  public:
    TrafficActionBuilder() = default;

    /// @brief Converts a control strategy to the corresponding OSI TrafficAction. If conversion is not
    ///        possible/implemented, no value is returned.
    /// @param i_entity  The controlled entity
    /// @param lane_location_provider  Lane location provider
    /// @param control_strategy  The control strategy to be converted
    /// @return  OSI::TrafficAction containing the converted control strategy or no value
    static std::optional<osi3::TrafficAction> ConvertToTrafficAction(
        const mantle_api::IEntity& i_entity,
        const map::LaneLocationProvider* lane_location_provider,
        mantle_api::ControlStrategy* control_strategy);
    static osi3::TrafficAction CreateCustomTrafficAction(const std::string& command_type, const std::string& command);

  private:
    static void FillPathPoints(osi3::TrafficAction_FollowPathAction* follow_path_action,
                               mantle_api::FollowTrajectoryControlStrategy* follow_trajectory_control_strategy);
    static osi3::TrafficAction FillCustomLightStateAction(
        mantle_api::VehicleLightStatesControlStrategy* vehicle_light_states_control_strategy);
    static osi3::TrafficAction FillCustomFollowClothoidSplineAction(
        const mantle_api::FollowTrajectoryControlStrategy* follow_trajectory_control_strategy);
};

}  // namespace gtgen::core::environment::traffic_command

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H
