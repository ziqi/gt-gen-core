/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H

#include <MantleAPI/Common/i_identifiable.h>

#include <cstddef>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace gtgen::core::environment::datastore
{

using Key = std::string;  ///< Type of a key in the DataBuffer
using Value = std::variant<bool,
                           std::vector<bool>,
                           char,
                           std::vector<char>,
                           int,
                           std::vector<int>,
                           std::size_t,
                           std::vector<std::size_t>,
                           float,
                           std::vector<float>,
                           double,
                           std::vector<double>,
                           std::string,
                           std::vector<std::string>>;  ///< Type of a value in the DataBuffer

inline const Key kWildcard = "*";  //!< Wildcard to match any key string. Length of 1 is mandatory.

/// @brief Representation of an entry in the cyclics.
///
struct CyclicRow
{
    /// @brief Construct a new Cyclic Row object.
    ///
    /// @param[in] id     Id of the entity (agent or object)
    /// @param[in] k      Key (topic) associated with the data
    /// @param[in] v      Data value
    CyclicRow(mantle_api::UniqueId id, Key k, Value v);

    bool operator==(const CyclicRow& other) const
    {
        return entity_id == other.entity_id && key == other.key && value == other.value;
    }

    mantle_api::UniqueId entity_id;  //!< Id of the entity (agent or object)
    Key key;                         //!< Key (topic) associated with the data
    Value value;                     //!< Data value
};

using Keys = std::vector<Key>;                                               //!< List of keys
using Values = std::vector<Value>;                                           //!< List of values
using CyclicRowRefs = std::vector<std::reference_wrapper<const CyclicRow>>;  //!< List of references to rows

/// @brief A set of cyclic data elements representing a DataInterface query result.
/// Basic forward iterator properties are provided for convenient result iteration.
///
class CyclicResultInterface
{
  public:
    CyclicResultInterface();
    CyclicResultInterface(const CyclicResultInterface&) = delete;
    CyclicResultInterface(CyclicResultInterface&&) = delete;
    CyclicResultInterface& operator=(const CyclicResultInterface&) & = delete;
    CyclicResultInterface& operator=(CyclicResultInterface&&) & = delete;
    virtual ~CyclicResultInterface();

    /// @return Returns size of cyclic result interface
    virtual std::size_t size() const = 0;

    /// @param[in] index position of cyclic result interface
    /// @return Returns the reference to cyclic row at given point
    virtual const CyclicRow& at(const std::size_t index) const = 0;

    /// @return Returns constant iterator of cyclic row
    virtual CyclicRowRefs::const_iterator begin() const = 0;

    /// @return Returns constant iterator of cyclic row
    virtual CyclicRowRefs::const_iterator end() const = 0;
};

/// @brief The DataBufferReadInterface provides read-only access to the data.
///
class DataBufferReadInterface
{
  public:
    DataBufferReadInterface();
    DataBufferReadInterface(const DataBufferReadInterface&) = delete;
    DataBufferReadInterface(DataBufferReadInterface&&) = delete;
    DataBufferReadInterface& operator=(const DataBufferReadInterface&) & = delete;
    DataBufferReadInterface& operator=(DataBufferReadInterface&&) & = delete;
    virtual ~DataBufferReadInterface();

    /// @brief Retrieves stored cyclic values.
    ///
    /// @param[in]   entity_id   Entity's id
    /// @param[in]   key        Unique topic identification
    /// @return stored cyclic values
    virtual std::unique_ptr<CyclicResultInterface> GetCyclic(const std::optional<mantle_api::UniqueId> entity_id,
                                                             const Key& key) const = 0;
};

/// @brief The DataBufferWriteInterface provides write-only access to the data.
///
class DataBufferWriteInterface
{
  public:
    DataBufferWriteInterface();
    DataBufferWriteInterface(const DataBufferWriteInterface&) = delete;
    DataBufferWriteInterface(DataBufferWriteInterface&&) = delete;
    DataBufferWriteInterface& operator=(const DataBufferWriteInterface&) & = delete;
    DataBufferWriteInterface& operator=(DataBufferWriteInterface&&) & = delete;
    virtual ~DataBufferWriteInterface();

    /// @brief Writes cyclic information
    ///
    /// @param[in]   entity_id   Id of the associated agent or object
    /// @param[in]   key        Unique topic identification
    /// @param[in]   value      Value to be written
    virtual void PutCyclic(const mantle_api::UniqueId entity_id, const Key& key, const Value& value) = 0;
};

/// @brief The DataInterface provides read/write access to the data.
/// This interface combines DataReadInterface and DataWriteInterface and adds some additional
/// methods required for instantiation by the framework.
///
class DataBufferInterface : public DataBufferReadInterface, public DataBufferWriteInterface
{
  public:
    DataBufferInterface();
    DataBufferInterface(const DataBufferInterface&) = delete;
    DataBufferInterface(DataBufferInterface&&) = delete;
    DataBufferInterface& operator=(const DataBufferInterface&) & = delete;
    DataBufferInterface& operator=(DataBufferInterface&&) & = delete;
    ~DataBufferInterface() override;

    /// @brief Clears the data contents.
    virtual void Clear() = 0;
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_DATABUFFERINTERFACE_H
