/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/utils.h"

#include <fstream>

namespace gtgen::core::environment::datastore::utils
{

void WriteJson(const nlohmann::ordered_json& json_object, const std::string& filename)
{
    if (!json_object.empty())
    {
        std::ofstream json_output_file;
        json_output_file.open(filename);
        json_output_file << json_object.dump(4) << std::endl;
        json_output_file.close();
    }
}

}  // namespace gtgen::core::environment::datastore::utils
