/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"

#include "Core/Environment/DataStore/data_buffer_interface.h"
#include "Core/Environment/DataStore/utils.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cstddef>
#include <functional>
#include <memory>
#include <optional>
#include <ostream>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace gtgen::core::environment::datastore
{

using ::testing::Eq;
using ::testing::NotNull;
using ::testing::SizeIs;

std::ostream& operator<<(std::ostream& stream, CyclicRow const& row)
{
    stream << ", entity_id = " << row.entity_id << ", key = " << row.key << " , value = ";

    std::visit(utils::to_string([&stream](const std::string& value_str) { stream << value_str; }, "|"), row.value);

    return stream << "}";
}

class TestBasicDataBuffer : public BasicDataBufferImplementation
{
  public:
    TestBasicDataBuffer() : BasicDataBufferImplementation() {}

    CyclicStore& GetStore() { return cyclic_store_; }

    void SetStore(CyclicStore& new_store) { cyclic_store_ = new_store; }
};

class BasicDataBuffer_GetCyclic_Test : public ::testing::Test
{
  public:
    BasicDataBuffer_GetCyclic_Test()
    {
        implementation = std::make_unique<BasicDataBufferImplementation>();

        // fill some test data

        // value types according to openpass::databuffer::Value
        // bool, char, int, size_t, float, double, std::string and std::vector of each type

        implementation->PutCyclic(0, "bool_val", true);
        implementation->PutCyclic(0, "char_val", 'a');
        implementation->PutCyclic(0, "int_val", 1);
        implementation->PutCyclic(0, "size_val", static_cast<std::size_t>(2));
        implementation->PutCyclic(0, "float_val", 3.3f);
        implementation->PutCyclic(0, "double_val", 4.4);
        implementation->PutCyclic(0, "string_val", "str");

        implementation->PutCyclic(0, "bool_vec", std::vector<bool>{true, false});
        implementation->PutCyclic(0, "char_vec", std::vector<char>{'a'});
        implementation->PutCyclic(0, "int_vec", std::vector<char>{1, 2});
        implementation->PutCyclic(0, "size_vec", std::vector<std::size_t>{3, 4});
        implementation->PutCyclic(0, "float_vec", std::vector<float>{5.5f, 6.6f});
        implementation->PutCyclic(0, "double_vec", std::vector<double>{7.7, 8.8});
        implementation->PutCyclic(0, "string_vec", std::vector<std::string>{"str1", "str2"});

        // data with additional entity ids

        implementation->PutCyclic(1, "int_val", 2);
        implementation->PutCyclic(2, "int_val", 3);

        implementation->PutCyclic(1, "int_val_2", 22);
        implementation->PutCyclic(2, "int_val_2", 23);
    }

    std::unique_ptr<DataBufferInterface> implementation;
};

class BasicDataBuffer_Persistence_Test : public ::testing::Test
{
  public:
    BasicDataBuffer_Persistence_Test()
    {
        implementation = std::make_unique<BasicDataBufferImplementation>();

        implementation->PutCyclic(0, "cyclic_key", 2);
    }

    std::unique_ptr<DataBufferInterface> implementation;
};

TEST(BasicDataBuffer, PutCyclicData_StoresData)
{
    const mantle_api::UniqueId agent_id = 1;
    const int value = 2;
    const std::string key{"key"};
    const CyclicRow expected_row{agent_id, key, value};

    auto ds = std::make_unique<TestBasicDataBuffer>();

    ASSERT_THAT(ds, NotNull());

    ds->PutCyclic(agent_id, key, value);

    EXPECT_THAT(ds->GetStore().back(), Eq(expected_row));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenEntityIdAndKey_ReturnsCorrectData)
{
    const auto result1 = implementation->GetCyclic(1, "int_val");
    const auto result2 = implementation->GetCyclic(0, "int_val");

    ASSERT_THAT(result1->size(), Eq(1));
    ASSERT_THAT(result2->size(), Eq(1));
    EXPECT_THAT(result1->at(0), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result2->at(0), Eq(CyclicRow{0, "int_val", 1}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenKey_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(std::nullopt, "int_val");

    ASSERT_THAT(result->size(), Eq(3));
    EXPECT_THAT(result->at(0), Eq(CyclicRow{0, "int_val", 1}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result->at(2), Eq(CyclicRow{2, "int_val", 3}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenEntityIdAndWildcard_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(2, kWildcard);

    ASSERT_THAT(result->size(), Eq(2));
    EXPECT_THAT(result->at(0), Eq(CyclicRow{2, "int_val", 3}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{2, "int_val_2", 23}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenWildcard_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(std::nullopt, kWildcard);

    ASSERT_THAT(result->size(), Eq(18));

    EXPECT_THAT(result->at(0), Eq(CyclicRow{0, "bool_val", true}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{0, "char_val", 'a'}));
    EXPECT_THAT(result->at(2), Eq(CyclicRow{0, "int_val", 1}));
    EXPECT_THAT(result->at(3), Eq(CyclicRow{0, "size_val", static_cast<std::size_t>(2)}));
    EXPECT_THAT(result->at(4), Eq(CyclicRow{0, "float_val", 3.3f}));
    EXPECT_THAT(result->at(5), Eq(CyclicRow{0, "double_val", 4.4}));
    EXPECT_THAT(result->at(6), Eq(CyclicRow{0, "string_val", "str"}));

    EXPECT_THAT(result->at(7), Eq(CyclicRow{0, "bool_vec", std::vector<bool>{true, false}}));
    EXPECT_THAT(result->at(8), Eq(CyclicRow{0, "char_vec", std::vector<char>{'a'}}));
    EXPECT_THAT(result->at(9), Eq(CyclicRow{0, "int_vec", std::vector<char>{1, 2}}));
    EXPECT_THAT(result->at(10), Eq(CyclicRow{0, "size_vec", std::vector<std::size_t>{3, 4}}));
    EXPECT_THAT(result->at(11), Eq(CyclicRow{0, "float_vec", std::vector<float>{5.5f, 6.6f}}));
    EXPECT_THAT(result->at(12), Eq(CyclicRow{0, "double_vec", std::vector<double>{7.7, 8.8}}));
    EXPECT_THAT(result->at(13), Eq(CyclicRow{0, "string_vec", std::vector<std::string>{"str1", "str2"}}));

    EXPECT_THAT(result->at(14), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result->at(15), Eq(CyclicRow{2, "int_val", 3}));

    EXPECT_THAT(result->at(16), Eq(CyclicRow{1, "int_val_2", 22}));
    EXPECT_THAT(result->at(17), Eq(CyclicRow{2, "int_val_2", 23}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, Result_IsIteratable)
{
    const auto result = implementation->GetCyclic(std::nullopt, "int_val_2");

    ASSERT_THAT(result->size(), Eq(2));

    int expected_value = 22;
    mantle_api::UniqueId id = 1;
    for (const auto& entry_ref : *result)
    {
        const auto& entry = entry_ref.get();
        EXPECT_THAT(entry, Eq(CyclicRow{id, "int_val_2", expected_value}));
        expected_value += 1;
        id += 1;
    }
}

TEST_F(BasicDataBuffer_Persistence_Test, FixtureSetup_StoresData)
{
    const auto cyclic_result = implementation->GetCyclic(std::nullopt, "cyclic_key");

    ASSERT_THAT(cyclic_result->size(), Eq(1));
}

TEST_F(BasicDataBuffer_Persistence_Test, Clear_DeletesNonPersisentData)
{
    implementation->Clear();

    const auto cyclic_result = implementation->GetCyclic(std::nullopt, "cyclic_key");

    ASSERT_THAT(cyclic_result->size(), Eq(0));
}

TEST(BasicDataBuffer_Constants, Wildcard_IsSize1)
{
    ASSERT_THAT(kWildcard, SizeIs(1));
}

}  // namespace gtgen::core::environment::datastore
