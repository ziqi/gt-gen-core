/********************************************************************************
 * Copyright (c) 2024-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/output_generator.h"

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"
#include "Core/Environment/DataStore/data_buffer_interface.h"
#include "Core/Environment/DataStore/utils.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/logging.h"

#include <fmt/format.h>

#include <exception>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <utility>

namespace gtgen::core::environment::datastore
{

OutputGenerator::Settings::Settings(const fs::path& output_path,
                                    std::int32_t current_run_number,
                                    std::int32_t total_runs)
    : output_path_{output_path}, current_run_number_{current_run_number}, total_runs_{total_runs}
{
    if (current_run_number < 0)
    {
        throw EnvironmentException(
            "OutputGenerator::Settings::Settings: Precondition violated - current run (run_number={}) must have "
            "non-negative value",
            current_run_number_);
    }

    if (current_run_number >= total_runs)
    {
        throw EnvironmentException(
            "OutputGenerator::Settings::Settings: Precondition violated - current run (run_number={}) must be smaller "
            "than number of total runs (total_runs={})",
            current_run_number_,
            total_runs_);
    }
}

std::string OutputGenerator::Settings::GetCyclicsFilename() const
{
    return "Cyclics_Run_" + GetRunIdWithLeadingZeros() + ".csv";
}

std::string OutputGenerator::Settings::GetCoreInformationFilename() const
{
    return "CoreInformation.json";
}

fs::path OutputGenerator::Settings::GetEntityOutputFolder(const mantle_api::UniqueId entity_id) const
{
    return GetGlobalOutputFolder() / ("run" + GetRunIdWithLeadingZeros()) /
           ("entity" + fmt::format("{:04}", entity_id));
}

std::string OutputGenerator::Settings::GetRunIdWithLeadingZeros() const
{
    std::stringstream ss{};
    ss << std::setfill('0') << std::setw(static_cast<int>(std::to_string(total_runs_ - 1).length()))
       << GetCurrentRunNumber();
    return ss.str();
}

OutputGenerator::OutputGenerator(const OutputGenerator::Settings& settings)
    : settings_{settings}, data_buffer_{}, cyclics_{}
{
}

void OutputGenerator::Init()
{
    if (!settings_.GetGlobalOutputFolder().empty())
    {
        gtgen::core::service::file_system::CreateDirectoryIfNotExisting(settings_.GetGlobalOutputFolder());
    }
}

void OutputGenerator::PutCyclic(const mantle_api::UniqueId entity_id, const Key& key, const Value& value)
{
    data_buffer_.PutCyclic(entity_id, key, value);
}

void OutputGenerator::Step(const mantle_api::Time& time)
{
    StoreCyclics(time.to<std::int32_t>());
    data_buffer_.Clear();
}

void OutputGenerator::WriteCoreInformation(const nlohmann::ordered_json& core_information)
{
    if (!settings_.GetGlobalOutputFolder().empty())
    {
        gtgen::core::service::file_system::CreateDirectoryIfNotExisting(settings_.GetGlobalOutputFolder());
    }

    gtgen::core::environment::datastore::utils::WriteJson(
        core_information, settings_.GetGlobalOutputFolder() / settings_.GetCoreInformationFilename());
}

void OutputGenerator::FinishRun()
{
    WriteCyclics();
    cyclics_.Clear();
}

void OutputGenerator::StoreCyclics(std::int32_t time)
{
    const auto ds_cyclics = data_buffer_.GetCyclic(std::nullopt, kWildcard);

    for (const CyclicRow& ds_cyclic : *ds_cyclics)
    {
        std::visit(utils::to_string([this, &ds_cyclic, &time](const std::string& value_str) {
                       cyclics_.Insert(time, fmt::format("{:02}:{}", ds_cyclic.entity_id, ds_cyclic.key), value_str);
                   }),
                   ds_cyclic.value);
    }
}

void OutputGenerator::WriteCyclics()
{
    try
    {
        if (!cyclics_.IsEmpty())
        {
            std::ofstream output_file;
            output_file.open(settings_.GetGlobalOutputFolder() / settings_.GetCyclicsFilename(), std::ofstream::trunc);
            const auto& time_steps = cyclics_.GetTimeSteps();
            output_file << fmt::format("Timestep, {}\n", cyclics_.GetHeader());
            std::uint32_t time_step_number = 0;
            for (const auto time_step : time_steps)
            {
                output_file << fmt::format("{}, {}\n", time_step, cyclics_.GetSamplesLine(time_step_number));
                ++time_step_number;
            }
            output_file.flush();
            output_file.close();
        }
    }
    catch (const std::exception& ex)
    {
        Warn("Exception caught: {}", ex.what());
    }
}

}  // namespace gtgen::core::environment::datastore
