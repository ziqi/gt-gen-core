/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_CYCLICS_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_CYCLICS_H

#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace gtgen::core::environment::datastore
{

/// @brief The Cyclics stores the samples which are logged by various modules
///        and written into the Cyclics_Run_###.csv.
///
class Cyclics
{
  public:
    Cyclics();
    Cyclics(const Cyclics&) = delete;
    Cyclics(Cyclics&&) = delete;
    Cyclics& operator=(const Cyclics&) & = delete;
    Cyclics& operator=(Cyclics&&) & = delete;
    ~Cyclics();

    /// @brief Inserts a parameter into the samples.
    ///        If the key does not exist yet, it inserts it into the list of keys
    ///        and all previous timesteps are filled with an empty string.
    ///        If the key exists, but some timesteps are missing for this key, then
    ///        missing timesteps are filled with an empty string.
    ///
    /// @param[in]     time       Timestep in milliseconds.
    /// @param[in]     key        Name of parameter as string.
    /// @param[in]     value      Value of parameter as string.
    void Insert(std::uint32_t time, const std::string& key, const std::string& value);

    /// @return Returns the string header that is written into the simulationOuput.xml
    std::string GetHeader() const;

    /// @brief Returns a single sample for the given timestep for writing into the Cyclics_Run_###.csv.
    ///
    /// @param[in] time_step_number    Number of timesteps
    /// @param[in] key               Key of the sample
    /// @return single lsamples
    std::string GetSample(std::uint32_t time_step_number, const std::string& key) const;

    /// @brief Returns a single line of samples for the given timestep for writing into the Cyclics_Run_###.csv.
    ///
    /// @param[in] time_step_number    Number of timesteps
    /// @return single line of samples
    std::string GetSamplesLine(std::uint32_t time_step_number) const;

    /// @brief Returns all timesteps for which samples exist.
    ///
    /// @return timesteps
    const std::set<std::uint32_t>& GetTimeSteps() const { return time_steps_; }

    /// @brief Returns if cyclics are empty.
    ///
    /// @return True if cyclics are empty
    bool IsEmpty() const { return time_steps_.empty(); }

    /// @brief Clears all samples for a new run.
    ///
    void Clear();

  private:
    std::set<std::uint32_t> time_steps_;
    std::map<std::string, std::vector<std::string>> samples_;
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_CYCLICS_H
