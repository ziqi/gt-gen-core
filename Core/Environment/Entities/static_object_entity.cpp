/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/static_object_entity.h"

namespace gtgen::core::environment::entities
{

mantle_api::StaticObjectProperties* StaticObject::GetProperties() const
{
    return GetPropertiesAs<mantle_api::StaticObjectProperties>();
}

void StaticObject::SetPosition(const mantle_api::Vec3<units::length::meter_t>& position)
{
    BaseEntity::SetPosition(position);
    position_set_ = true;
}

void StaticObject::SetOrientation(const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    BaseEntity::SetOrientation(orientation);
    orientation_set_ = true;
}

bool StaticObject::IsPositionSet() const
{
    return position_set_;
}

bool StaticObject::IsOrientationSet() const
{
    return orientation_set_;
}
}  // namespace gtgen::core::environment::entities
