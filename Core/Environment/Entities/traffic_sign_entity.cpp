/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/traffic_sign_entity.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"

namespace gtgen::core::environment::entities
{

TrafficSignEntity::TrafficSignEntity(mantle_api::UniqueId id, const std::string& name) : StaticObject(id, name) {}

void TrafficSignEntity::AddSupplementarySign(const mantle_api::UniqueId& key, mantle_api::IEntity& supplementary_sign)
{
    supplementary_signs.emplace(key, std::ref(supplementary_sign));
}

mantle_api::IEntity* TrafficSignEntity::GetSupplementarySign(const mantle_api::UniqueId& key) const
{
    if (auto it = supplementary_signs.find(key); it != supplementary_signs.end())
    {
        return &it->second.get();
    }
    return nullptr;
}

std::unordered_map<mantle_api::UniqueId, std::reference_wrapper<mantle_api::IEntity>>
TrafficSignEntity::GetSupplementarySigns() const
{
    return supplementary_signs;
}

bool TrafficSignEntity::DeleteSupplementarySign(const mantle_api::UniqueId& key)
{
    auto it = supplementary_signs.find(key);
    if (it != supplementary_signs.end())
    {
        supplementary_signs.erase(it);
        return true;
    }
    return false;
}

void TrafficSignEntity::SetPosition(const mantle_api::Vec3<units::length::meter_t>& position)
{
    StaticObject::SetPosition(position);

    // Skip updating the supplementary sign's position if the main sign's orientation is not set yet,
    // because the calculation of the supplementary sign's position depends on the main sign's orientation.
    if (!IsOrientationSet())
    {
        return;
    }

    if (const auto* traffic_sign_properties = GetProperties(); traffic_sign_properties != nullptr)
    {
        SetSupplementarySignPositions(traffic_sign_properties);
    }
}

void TrafficSignEntity::SetOrientation(const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    StaticObject::SetOrientation(orientation);
    SetSupplementarySignOrientations(orientation);
}

void TrafficSignEntity::SetSupplementarySignPositions(const mantle_api::StaticObjectProperties* properties)
{
    const auto main_sign_vertical_offset = properties->vertical_offset;
    const auto main_sign_geometric_center = properties->bounding_box.geometric_center;

    for (auto& [unique_id, sign] : supplementary_signs)
    {
        auto* supplementary_sign = dynamic_cast<StaticObject*>(&sign.get());
        if (!supplementary_sign || supplementary_sign->IsPositionSet())
        {
            continue;
        }

        auto supplementary_sign_properties = supplementary_sign->GetProperties();
        if (!supplementary_sign_properties)
        {
            continue;
        }

        // Calculate the local position of the supplementary sign relative to its main sign
        const auto supplementary_sign_vertical_offset = supplementary_sign_properties->vertical_offset;
        const auto supplementary_sign_geometric_center = supplementary_sign_properties->bounding_box.geometric_center;

        mantle_api::Vec3<units::length::meter_t> supplementary_sign_local_position{
            supplementary_sign_geometric_center.x - main_sign_geometric_center.x,
            supplementary_sign_geometric_center.y - main_sign_geometric_center.y,
            supplementary_sign_geometric_center.z + supplementary_sign_vertical_offset - main_sign_vertical_offset -
                main_sign_geometric_center.z};

        auto new_supplementary_sign_position =
            service::glmwrapper::ToGlobalSpace(position_, orientation_, supplementary_sign_local_position);

        supplementary_sign->SetPosition(new_supplementary_sign_position);
    }
}

void TrafficSignEntity::SetSupplementarySignOrientations(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    for (auto& [unique_id, sign] : supplementary_signs)
    {
        auto* supplementary_sign = dynamic_cast<StaticObject*>(&sign.get());
        if (!supplementary_sign || supplementary_sign->IsOrientationSet())
        {
            continue;
        }
        supplementary_sign->SetOrientation(orientation);
    }
}

}  // namespace gtgen::core::environment::entities
