/*******************************************************************************
 * Copyright (c) 2024-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/entity_container.h"

namespace gtgen::core::environment::entities
{

EntityContainer::EntityContainer(mantle_api::IEntity* entity)
    : BaseEntity(entity->GetUniqueId(), entity->GetName()), entity_{entity}
{
    SynchronizeEntityContainer();
}

bool EntityContainer::HasBeenUpdatedFromOutside()
{
    return position_ != entity_->GetPosition() || orientation_ != entity_->GetOrientation();
}

void EntityContainer::SynchronizeEntityContainer()
{
    SetPosition(entity_->GetPosition());
    SetVelocity(entity_->GetVelocity());
    SetAcceleration(entity_->GetAcceleration());
    SetOrientation(entity_->GetOrientation());
    SetOrientationRate(entity_->GetOrientationRate());
    SetOrientationAcceleration(entity_->GetOrientationAcceleration());
}

void EntityContainer::UpdateEntity()
{
    entity_->SetPosition(position_);
    entity_->SetVelocity(velocity_);
    entity_->SetAcceleration(acceleration_);
    entity_->SetOrientation(orientation_);
    entity_->SetOrientationRate(orientation_rate_);
    entity_->SetOrientationAcceleration(orientation_acceleration_);
}

mantle_api::IEntity* EntityContainer::GetEntity()
{
    return entity_;
}

mantle_api::Vec3<units::length::meter_t> EntityContainer::GetPosition() const
{
    return entity_->GetPosition();
}

mantle_api::Vec3<units::length::meter_t> EntityContainer::GetNextPosition() const
{
    return position_;
}

mantle_api::Vec3<units::velocity::meters_per_second_t> EntityContainer::GetVelocity() const
{
    return entity_->GetVelocity();
}

mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> EntityContainer::GetAcceleration() const
{
    return entity_->GetAcceleration();
}

mantle_api::Orientation3<units::angle::radian_t> EntityContainer::GetOrientation() const
{
    return entity_->GetOrientation();
}

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> EntityContainer::GetOrientationRate() const
{
    return entity_->GetOrientationRate();
}

mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>
EntityContainer::GetOrientationAcceleration() const
{
    return entity_->GetOrientationAcceleration();
}

mantle_api::EntityProperties* EntityContainer::GetProperties() const
{
    return entity_->GetProperties();
}

std::vector<std::uint64_t> EntityContainer::GetAssignedLaneIds() const
{
    return entity_->GetAssignedLaneIds();
}

void EntityContainer::SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids)
{
    entity_->SetAssignedLaneIds(assigned_lane_ids);
}

void EntityContainer::SetVisibility(const mantle_api::EntityVisibilityConfig& visibility)
{
    entity_->SetVisibility(visibility);
}

mantle_api::EntityVisibilityConfig EntityContainer::GetVisibility() const
{
    return entity_->GetVisibility();
}

}  // namespace gtgen::core::environment::entities
