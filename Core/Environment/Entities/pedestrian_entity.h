/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_PEDESTRIANENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_PEDESTRIANENTITY_H

#include "Core/Environment/Entities/Internal/base_entity.h"

namespace gtgen::core::environment::entities
{
class PedestrianEntity : public BaseEntity, public mantle_api::IPedestrian
{
  public:
    PedestrianEntity(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

    mantle_api::PedestrianProperties* GetProperties() const override
    {
        return GetPropertiesAs<mantle_api::PedestrianProperties>();
    }
};

}  // namespace gtgen::core::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_PEDESTRIANENTITY_H
