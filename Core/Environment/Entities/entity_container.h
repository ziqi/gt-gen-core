/*******************************************************************************
 * Copyright (c) 2024-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_ENTITY_CONTAINER_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_ENTITY_CONTAINER_H

#include "Core/Environment/Entities/Internal/base_entity.h"

#include <MantleAPI/Traffic/i_entity.h>
namespace gtgen::core::environment::entities
{

class EntityContainer : public BaseEntity
{
  public:
    EntityContainer(mantle_api::IEntity* entity);
    ~EntityContainer() override = default;

    void UpdateEntity();
    void SynchronizeEntityContainer();
    bool HasBeenUpdatedFromOutside();
    mantle_api::IEntity* GetEntity();

    mantle_api::Vec3<units::length::meter_t> GetPosition() const override;
    mantle_api::Vec3<units::length::meter_t> GetNextPosition() const;
    mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocity() const override;
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const override;
    mantle_api::Orientation3<units::angle::radian_t> GetOrientation() const override;
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const override;
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
        const override;
    mantle_api::EntityProperties* GetProperties() const override;
    std::vector<std::uint64_t> GetAssignedLaneIds() const override;
    void SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids) override;
    void SetVisibility(const mantle_api::EntityVisibilityConfig& visibility) override;
    mantle_api::EntityVisibilityConfig GetVisibility() const override;

  protected:
    mantle_api::IEntity* entity_{nullptr};
};

template <class EntityType>
EntityType* CastEntityTypeTo(mantle_api::IEntity* entity)
{
    auto* underlying_entity = dynamic_cast<EntityContainer*>(entity);
    if (underlying_entity == nullptr)
    {
        return dynamic_cast<EntityType*>(entity);
    }
    else
    {
        return dynamic_cast<EntityType*>(underlying_entity->GetEntity());
    }
}

}  // namespace gtgen::core::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_ENTITY_CONTAINER_H
