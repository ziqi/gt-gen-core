/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_ground_truth_builder.h"

#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter_utils.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"

#include <gtest/gtest.h>

#include <cstdint>

namespace gtgen::core::environment::proto_groundtruth
{
namespace
{

class DynamicProtoGroundTruthBuilderTest : public testing::Test
{
  protected:
    void AddHostAndTrafficVehicleToEntityPlacement()
    {
        proto_gt_.mutable_host_vehicle_id()->set_value(0);

        test_utils::AddArtificialHostVehicle(mantle_api::IndicatorState::kRight, "BMW7S2015", entity_repository_);
        test_utils::AddArtificialTrafficVehicle(1, mantle_api::IndicatorState::kOff, "BMW7S2008", entity_repository_);
    }

    void AddFourPedestriansToEntityPlacement()
    {
        test_utils::AddArtificialPedestrian(10, "boy", entity_repository_);
        test_utils::AddArtificialPedestrian(11, "female", entity_repository_);
        test_utils::AddArtificialPedestrian(12, "male", entity_repository_);
        test_utils::AddArtificialPedestrian(13, "girl", entity_repository_);
    }

    /// @brief For simplicity, create always 2 chunks
    ///
    /// The first contains one element (the first of each vector) and the second contains the rest
    environment::chunking::WorldChunks CreateChunksFromEntityPlacement()
    {
        environment::chunking::WorldChunks result;
        environment::chunking::WorldChunk first{};
        environment::chunking::WorldChunk second{};

        const auto& entities{entity_repository_.GetEntities()};
        for (std::size_t i = 0; i < entities.size(); ++i)
        {
            if (i == 0)
            {
                first.entities.push_back(entities.at(i).get());
            }
            else
            {
                second.entities.push_back(entities.at(i).get());
            }
        }

        result.emplace_back(std::move(first));
        result.emplace_back(std::move(second));

        return result;
    }

    service::utility::UniqueIdProvider unique_id_provider_{};
    environment::api::EntityRepository entity_repository_{&unique_id_provider_};
    DynamicProtoGroundTruthBuilder dynamic_proto_gt_builder_;
    osi3::GroundTruth proto_gt_;
};

void AssertMovingObjectMatches(const osi3::MovingObject& moving_object)
{
    using units::literals::operator""_m;
    using units::literals::operator""_rad;
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;

    osi3::Dimension3d expected_boundingbox =
        service::gt_conversion::CreateProtoObject<osi3::Dimension3d>(mantle_api::Dimension3{4_m, 2_m, 1.5_m});
    osi3::Vector3d expected_position = service::gt_conversion::CreateProtoObject<osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m});
    osi3::Vector3d expected_velocity = service::gt_conversion::CreateProtoObject<osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m});
    osi3::Vector3d expected_acceleration = service::gt_conversion::CreateProtoObject<osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m});
    osi3::Orientation3d expected_orientation = service::gt_conversion::CreateProtoObject<osi3::Orientation3d>(
        mantle_api::Orientation3<units::angle::radian_t>{1_rad, 2_rad, 3_rad});
    osi3::Orientation3d expected_orientation_rate = service::gt_conversion::CreateProtoObject<osi3::Orientation3d>(
        mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{4_rad_per_s, 5_rad_per_s, 6_rad_per_s});
    osi3::Orientation3d expected_orientation_acceleration =
        service::gt_conversion::CreateProtoObject<osi3::Orientation3d>(
            mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{
                7_rad_per_s_sq, 8_rad_per_s_sq, 9_rad_per_s_sq});

    EXPECT_EQ(expected_boundingbox.SerializeAsString(), moving_object.base().dimension().SerializeAsString());
    EXPECT_EQ(expected_position.SerializeAsString(), moving_object.base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(), moving_object.base().orientation().SerializeAsString());
    EXPECT_EQ(expected_velocity.SerializeAsString(), moving_object.base().velocity().SerializeAsString());
    EXPECT_EQ(expected_acceleration.SerializeAsString(), moving_object.base().acceleration().SerializeAsString());
    EXPECT_EQ(expected_orientation_rate.SerializeAsString(),
              moving_object.base().orientation_rate().SerializeAsString());
    EXPECT_EQ(expected_orientation_acceleration.SerializeAsString(),
              moving_object.base().orientation_acceleration().SerializeAsString());
    ASSERT_EQ(2, moving_object.assigned_lane_id().size());
    EXPECT_EQ(1, moving_object.assigned_lane_id(0).value());
    EXPECT_EQ(2, moving_object.assigned_lane_id(1).value());
}

TEST_F(DynamicProtoGroundTruthBuilderTest,
       GivenHostAndTrafficVehicles_WhenFillDynamicGroundTruth_ThenVehiclesDataIsFilled)
{
    AddHostAndTrafficVehicleToEntityPlacement();

    dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_);

    const auto& vehicles = test_utils::GetObjectsByType(proto_gt_, osi3::MovingObject::TYPE_VEHICLE);

    ASSERT_EQ(2, vehicles.size());
    AssertMovingObjectMatches(*vehicles[0]);
    AssertMovingObjectMatches(*vehicles[1]);
    EXPECT_EQ(vehicles[0]->vehicle_classification().light_state().indicator_state(),
              ConvertGtGenIndicatorStateToProto(mantle_api::IndicatorState::kRight));
    EXPECT_EQ(vehicles[1]->vehicle_classification().light_state().indicator_state(),
              ConvertGtGenIndicatorStateToProto(mantle_api::IndicatorState::kOff));
    EXPECT_EQ(vehicles[0]->model_reference(), "BMW7S2015");
    EXPECT_EQ(vehicles[1]->model_reference(), "BMW7S2008");
}

TEST_F(DynamicProtoGroundTruthBuilderTest,
       GivenHostAndTrafficVehicles_WhenFillDynamicGroundTruth_ThenHostVehicleIsPresentInTheGT)
{
    AddHostAndTrafficVehicleToEntityPlacement();

    dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_);

    ASSERT_TRUE(proto_gt_.has_host_vehicle_id());
    EXPECT_NE(nullptr, test_utils::GetObjectById(proto_gt_, proto_gt_.host_vehicle_id().value()));
}

TEST_F(DynamicProtoGroundTruthBuilderTest, GivenFourPedestrians_WhenFillDynamicGroundTruth_ThenPedestrianDataIsFilled)
{
    AddFourPedestriansToEntityPlacement();

    dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_);

    const auto& pedestrians = test_utils::GetObjectsByType(proto_gt_, osi3::MovingObject::TYPE_PEDESTRIAN);

    ASSERT_EQ(4, pedestrians.size());

    AssertMovingObjectMatches(*pedestrians[0]);
    AssertMovingObjectMatches(*pedestrians[1]);
    AssertMovingObjectMatches(*pedestrians[2]);
    AssertMovingObjectMatches(*pedestrians[3]);

    EXPECT_EQ(pedestrians[0]->model_reference(), "boy");
    EXPECT_EQ(pedestrians[1]->model_reference(), "female");
    EXPECT_EQ(pedestrians[2]->model_reference(), "male");
    EXPECT_EQ(pedestrians[3]->model_reference(), "girl");
}

TEST_F(DynamicProtoGroundTruthBuilderTest,
       GivenObjectsToIgnoreInGroundTruth_WhenFillingDynamicGroundTruth_ThenIgnoredObjectsAreNotFilled)
{
    // Arrange

    test_utils::AddArtificialVehicles(2, entity_repository_);
    test_utils::AddArtificialPedestrians(2, entity_repository_);

    dynamic_proto_gt_builder_.AddEntityIdToIgnoreList(1002);

    // Act
    dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_);

    // Assert
    const auto& vehicles = test_utils::GetObjectsByType(proto_gt_, osi3::MovingObject::TYPE_VEHICLE);
    const auto& pedestrians = test_utils::GetObjectsByType(proto_gt_, osi3::MovingObject::TYPE_PEDESTRIAN);

    ASSERT_EQ(1, vehicles.size());
    ASSERT_EQ(2, pedestrians.size());

    EXPECT_EQ(1001, vehicles[0]->id().value());
    EXPECT_EQ(1003, pedestrians[0]->id().value());
    EXPECT_EQ(1004, pedestrians[1]->id().value());
}

void AssertAsscendingOrderById(const osi3::GroundTruth& gt)
{
    ASSERT_GE(gt.moving_object_size(), 1);
    uint64_t previous_entity_id = gt.moving_object(0).id().value();
    for (int idx = 1; idx < gt.moving_object_size(); idx++)
    {
        uint64_t current_entity_id = gt.moving_object(idx).id().value();
        ASSERT_LT(previous_entity_id, current_entity_id);
    }
}

TEST_F(DynamicProtoGroundTruthBuilderTest,
       GivenMovingObjects_WhenFillDynamicGroundTruth_ThenMovingObjectsSortedInAscendingOrderOfEntityIdInTheGT)
{
    test_utils::AddArtificialTrafficVehicle(42, mantle_api::IndicatorState::kOff, "vehicle_42", entity_repository_);
    test_utils::AddArtificialTrafficVehicle(24, mantle_api::IndicatorState::kOff, "vehicle_24", entity_repository_);
    test_utils::AddArtificialPedestrian(10, "pedestrian_10", entity_repository_);
    test_utils::AddArtificialPedestrian(15, "pedestrian_10", entity_repository_);

    dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_);

    AssertAsscendingOrderById(proto_gt_);
}

TEST_F(DynamicProtoGroundTruthBuilderTest,
       GivenNoMovingObjects_WhenFillDynamicGroundTruth_ThenNoThrowAndEmptyMovingObjects)
{
    ASSERT_NO_THROW(dynamic_proto_gt_builder_.FillDynamicGroundTruth(CreateChunksFromEntityPlacement(), proto_gt_));
    ASSERT_EQ(proto_gt_.moving_object_size(), 0);
}

}  // namespace
}  // namespace gtgen::core::environment::proto_groundtruth
