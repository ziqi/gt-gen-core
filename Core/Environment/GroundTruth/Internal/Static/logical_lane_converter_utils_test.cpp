/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/logical_lane_converter_utils.h"

#include "osi_logicallane.pb.h"

#include <MapAPI/logical_lane.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{

using units::literals::operator""_m;

template <class T>
void AssertEqualLists(const T& expected_elements, const T& actual_elements)
{
    ASSERT_EQ(expected_elements.size(), actual_elements.size());
    for (int i = 0; i < expected_elements.size(); ++i)
    {
        EXPECT_EQ(expected_elements.Get(i).SerializeAsString(), actual_elements.Get(i).SerializeAsString());
    }
}

class LogicalLanesConverterUtilsTest : public testing::Test
{
  public:
    map_api::LogicalLane& GetLogicalLane() { return logical_lane_; }

    osi3::LogicalLane& GetOsiLogicalLane() { return osi_logical_lane_; }

  private:
    map_api::LogicalLane logical_lane_{};
    osi3::LogicalLane osi_logical_lane_{};
};

TEST_F(LogicalLanesConverterUtilsTest, GivenMapApiLogicalLane_WhenFillLogicalLaneId_ThenIdIsFilledCorrectly)
{
    // Given

    GetLogicalLane().id = map_api::Identifier{11U};

    // When

    FillLogicalLaneId(&GetLogicalLane(), &GetOsiLogicalLane());

    // Expect

    EXPECT_EQ(GetLogicalLane().id, GetOsiLogicalLane().id().value());
}

TEST_F(LogicalLanesConverterUtilsTest, GivenMapApiLogicalLane_WhenFillLogicalLaneType_ThenTypeIsFilledCorrectly)
{
    // Given

    GetLogicalLane().type = map_api::LogicalLane::Type::kUnknown;

    // When

    FillLogicalLaneType(&GetLogicalLane(), &GetOsiLogicalLane());

    // Expect

    EXPECT_EQ(static_cast<osi3::LogicalLane_Type>(GetLogicalLane().type), GetOsiLogicalLane().type());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneSourceReferences_ThenSourceReferencesAreFilledCorrectly)
{
    // Given

    GetLogicalLane().source_references = {map_api::ExternalReference{"reference", "type", {"identifier"}}};

    // When

    FillLogicalLaneSourceReferences(&GetLogicalLane(), &GetOsiLogicalLane());

    // Expect

    google::protobuf::RepeatedPtrField<osi3::ExternalReference> expected_source_references;
    auto first_source_reference = expected_source_references.Add();
    ASSERT_EQ(1, GetLogicalLane().source_references.size());
    ASSERT_EQ(1, GetLogicalLane().source_references.at(0).identifiers.size());
    first_source_reference->set_reference(GetLogicalLane().source_references.at(0).reference);
    first_source_reference->set_type(GetLogicalLane().source_references.at(0).type);
    first_source_reference->add_identifier(GetLogicalLane().source_references.at(0).identifiers.at(0));

    AssertEqualLists(expected_source_references, GetOsiLogicalLane().source_reference());
}

TEST_F(
    LogicalLanesConverterUtilsTest,
    GivenMapApiLogicalLane_WhenFillLogicalLanePhysicalLaneReferencesWithUnknownId_ThenPhysicalLaneReferencesAreNotFilled)
{
    // Given

    map_api::Lane lane{map_api::Identifier{2U}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().physical_lane_references = std::vector<map_api::PhysicalLaneReference>{
        map_api::PhysicalLaneReference{lane, units::length::meter_t{0_m}, units::length::meter_t{30_m}}};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_lane_ids{};
    FillLogicalLanePhysicalLaneReferences(&GetLogicalLane(), &GetOsiLogicalLane(), existing_lane_ids);

    // Expect

    EXPECT_EQ(0, GetOsiLogicalLane().physical_lane_reference().size());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLanePhysicalLaneReferences_ThenPhysicalLaneReferencesAreFilledCorrectly)
{
    // Given

    map_api::Lane lane{map_api::Identifier{2U}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().physical_lane_references = std::vector<map_api::PhysicalLaneReference>{
        map_api::PhysicalLaneReference{lane, units::length::meter_t{0_m}, units::length::meter_t{30_m}}};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_lane_ids{lane.id};
    FillLogicalLanePhysicalLaneReferences(&GetLogicalLane(), &GetOsiLogicalLane(), existing_lane_ids);

    // Expect

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_PhysicalLaneReference> expected_physical_lane_references;
    auto first_physical_lane_reference = expected_physical_lane_references.Add();
    ASSERT_EQ(1, GetLogicalLane().physical_lane_references.size());
    first_physical_lane_reference->mutable_physical_lane_id()->set_value(
        GetLogicalLane().physical_lane_references.at(0).physical_lane.id);
    first_physical_lane_reference->set_start_s(GetLogicalLane().physical_lane_references.at(0).start_s.value());
    first_physical_lane_reference->set_end_s(GetLogicalLane().physical_lane_references.at(0).end_s.value());

    AssertEqualLists(expected_physical_lane_references, GetOsiLogicalLane().physical_lane_reference());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneReferenceLineWithUnknownId_ThenReferenceLineIsNotFilled)
{
    // Given

    map_api::ReferenceLine reference_line{map_api::Identifier{3U}, {}};
    GetLogicalLane().reference_line = &reference_line;

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_reference_line_ids{};
    FillLogicalLaneReferenceLine(&GetLogicalLane(), &GetOsiLogicalLane(), existing_reference_line_ids);

    // Expect

    EXPECT_FALSE(GetOsiLogicalLane().has_reference_line_id());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneReferenceLine_ThenReferenceLineIsFilledCorrectly)
{
    // Given

    map_api::ReferenceLine reference_line{map_api::Identifier{3U}, {}};
    GetLogicalLane().reference_line = &reference_line;

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_reference_line_ids{reference_line.id};
    FillLogicalLaneReferenceLine(&GetLogicalLane(), &GetOsiLogicalLane(), existing_reference_line_ids);

    // Expect

    EXPECT_EQ(GetLogicalLane().reference_line->id, GetOsiLogicalLane().reference_line_id().value());
    EXPECT_EQ(GetLogicalLane().start_s.value(), GetOsiLogicalLane().start_s());
    EXPECT_EQ(GetLogicalLane().end_s.value(), GetOsiLogicalLane().end_s());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneRelationshipsWithUnknownIds_ThenLogicalLaneRelationshipsAreNotFilled)
{
    // Given

    map_api::LogicalLane left_adjacent_logical_lane{
        map_api::Identifier{6U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane right_adjacent_logical_lane{
        map_api::Identifier{7U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane overlapping_logical_lane{
        map_api::Identifier{8U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().right_adjacent_lanes = std::vector<map_api::LogicalLaneRelation>{
        map_api::LogicalLaneRelation{right_adjacent_logical_lane, {}, {}, {}, {}}};
    GetLogicalLane().left_adjacent_lanes = std::vector<map_api::LogicalLaneRelation>{
        map_api::LogicalLaneRelation{left_adjacent_logical_lane, {}, {}, {}, {}}};
    GetLogicalLane().overlapping_lanes = std::vector<map_api::LogicalLaneRelation>{
        map_api::LogicalLaneRelation{overlapping_logical_lane, {}, {}, {}, {}}};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_logical_lane_ids{};
    FillLogicalLaneRelationships(&GetLogicalLane(), &GetOsiLogicalLane(), existing_logical_lane_ids);

    // Expect

    EXPECT_EQ(0, GetOsiLogicalLane().right_adjacent_lane_size());
    EXPECT_EQ(0, GetOsiLogicalLane().left_adjacent_lane_size());
    EXPECT_EQ(0, GetOsiLogicalLane().overlapping_lane_size());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneRelationships_ThenLogicalLaneRelationshipsAreFilledCorrectly)
{
    // Given

    map_api::LogicalLane left_adjacent_logical_lane{
        map_api::Identifier{6U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane right_adjacent_logical_lane{
        map_api::Identifier{7U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane overlapping_logical_lane{
        map_api::Identifier{8U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().right_adjacent_lanes =
        std::vector<map_api::LogicalLaneRelation>{map_api::LogicalLaneRelation{right_adjacent_logical_lane,
                                                                               units::length::meter_t{0_m},
                                                                               units::length::meter_t{30_m},
                                                                               units::length::meter_t{0_m},
                                                                               units::length::meter_t{30_m}}};
    GetLogicalLane().left_adjacent_lanes =
        std::vector<map_api::LogicalLaneRelation>{map_api::LogicalLaneRelation{left_adjacent_logical_lane,
                                                                               units::length::meter_t{0_m},
                                                                               units::length::meter_t{30_m},
                                                                               units::length::meter_t{0_m},
                                                                               units::length::meter_t{30_m}}};
    GetLogicalLane().overlapping_lanes =
        std::vector<map_api::LogicalLaneRelation>{map_api::LogicalLaneRelation{overlapping_logical_lane,
                                                                               units::length::meter_t{10_m},
                                                                               units::length::meter_t{13_m},
                                                                               units::length::meter_t{10_m},
                                                                               units::length::meter_t{13_m}}};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_logical_lane_ids{
        left_adjacent_logical_lane.id, right_adjacent_logical_lane.id, overlapping_logical_lane.id};
    FillLogicalLaneRelationships(&GetLogicalLane(), &GetOsiLogicalLane(), existing_logical_lane_ids);

    // Expect

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_LaneRelation> expected_right_adjacent_lanes;
    auto first_right_adjacent_lane = expected_right_adjacent_lanes.Add();
    ASSERT_EQ(1, GetLogicalLane().right_adjacent_lanes.size());
    first_right_adjacent_lane->mutable_other_lane_id()->set_value(
        GetLogicalLane().right_adjacent_lanes.at(0).other_lane.id);
    first_right_adjacent_lane->set_start_s(GetLogicalLane().right_adjacent_lanes.at(0).start_s.value());
    first_right_adjacent_lane->set_end_s(GetLogicalLane().right_adjacent_lanes.at(0).end_s.value());
    first_right_adjacent_lane->set_start_s_other(GetLogicalLane().right_adjacent_lanes.at(0).start_s_other.value());
    first_right_adjacent_lane->set_end_s_other(GetLogicalLane().right_adjacent_lanes.at(0).end_s_other.value());

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_LaneRelation> expected_left_adjacent_lanes;
    auto first_left_adjacent_lane = expected_left_adjacent_lanes.Add();
    ASSERT_EQ(1, GetLogicalLane().left_adjacent_lanes.size());
    first_left_adjacent_lane->mutable_other_lane_id()->set_value(
        GetLogicalLane().left_adjacent_lanes.at(0).other_lane.id);
    first_left_adjacent_lane->set_start_s(GetLogicalLane().left_adjacent_lanes.at(0).start_s.value());
    first_left_adjacent_lane->set_end_s(GetLogicalLane().left_adjacent_lanes.at(0).end_s.value());
    first_left_adjacent_lane->set_start_s_other(GetLogicalLane().left_adjacent_lanes.at(0).start_s_other.value());
    first_left_adjacent_lane->set_end_s_other(GetLogicalLane().left_adjacent_lanes.at(0).end_s_other.value());

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_LaneRelation> expected_overlapping_lanes;
    auto first_overlapping_lane = expected_overlapping_lanes.Add();
    ASSERT_EQ(1, GetLogicalLane().overlapping_lanes.size());
    first_overlapping_lane->mutable_other_lane_id()->set_value(GetLogicalLane().overlapping_lanes.at(0).other_lane.id);
    first_overlapping_lane->set_start_s(GetLogicalLane().overlapping_lanes.at(0).start_s.value());
    first_overlapping_lane->set_end_s(GetLogicalLane().overlapping_lanes.at(0).end_s.value());
    first_overlapping_lane->set_start_s_other(GetLogicalLane().overlapping_lanes.at(0).start_s_other.value());
    first_overlapping_lane->set_end_s_other(GetLogicalLane().overlapping_lanes.at(0).end_s_other.value());

    AssertEqualLists(expected_right_adjacent_lanes, GetOsiLogicalLane().right_adjacent_lane());
    AssertEqualLists(expected_left_adjacent_lanes, GetOsiLogicalLane().left_adjacent_lane());
    AssertEqualLists(expected_overlapping_lanes, GetOsiLogicalLane().overlapping_lane());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneLogicalLaneBoundaries_ThenLogicalLaneBoundariesAreFilledCorrectly)
{
    // Given

    map_api::LogicalLaneBoundary left_logical_lane_boundary{map_api::Identifier{4U}, {}, {}, nullptr, {}, {}};
    map_api::LogicalLaneBoundary right_logical_lane_boundary{map_api::Identifier{5U}, {}, {}, nullptr, {}, {}};
    GetLogicalLane().right_boundaries =
        std::vector<map_api::RefWrapper<map_api::LogicalLaneBoundary>>{right_logical_lane_boundary};
    GetLogicalLane().left_boundaries =
        std::vector<map_api::RefWrapper<map_api::LogicalLaneBoundary>>{left_logical_lane_boundary};

    // When

    FillLogicalLaneLogicalLaneBoundaries(&GetLogicalLane(), &GetOsiLogicalLane());

    // Expect

    google::protobuf::RepeatedPtrField<osi3::Identifier> expected_right_boundary_ids;
    auto first_right_boundary_id = expected_right_boundary_ids.Add();
    ASSERT_EQ(1, GetLogicalLane().right_boundaries.size());
    first_right_boundary_id->set_value(GetLogicalLane().right_boundaries.at(0).get().id);

    google::protobuf::RepeatedPtrField<osi3::Identifier> expected_left_boundary_ids;
    auto first_left_boundary_id = expected_left_boundary_ids.Add();
    ASSERT_EQ(1, GetLogicalLane().left_boundaries.size());
    first_left_boundary_id->set_value(GetLogicalLane().left_boundaries.at(0).get().id);

    AssertEqualLists(expected_right_boundary_ids, GetOsiLogicalLane().right_boundary_id());
    AssertEqualLists(expected_left_boundary_ids, GetOsiLogicalLane().left_boundary_id());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneConnectionsWithUnknownIds_ThenLogicalLaneConnectionsAreNotFilled)
{
    // Given

    map_api::LogicalLane predecessor_logical_lane{
        map_api::Identifier{9U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane successor_logical_lane{
        map_api::Identifier{10U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().predecessor_lanes =
        std::vector<map_api::RefWrapper<map_api::LogicalLane>>{predecessor_logical_lane};
    GetLogicalLane().successor_lanes = std::vector<map_api::RefWrapper<map_api::LogicalLane>>{successor_logical_lane};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_logical_lane_ids{};
    FillLogicalLaneConnections(&GetLogicalLane(), &GetOsiLogicalLane(), existing_logical_lane_ids);

    // Expect

    EXPECT_EQ(0, GetOsiLogicalLane().right_adjacent_lane_size());
    EXPECT_EQ(0, GetOsiLogicalLane().left_adjacent_lane_size());
    EXPECT_EQ(0, GetOsiLogicalLane().overlapping_lane_size());
}

TEST_F(LogicalLanesConverterUtilsTest,
       GivenMapApiLogicalLane_WhenFillLogicalLaneConnections_ThenLogicalLaneConnectionsAreFilledCorrectly)
{
    // Given

    map_api::LogicalLane predecessor_logical_lane{
        map_api::Identifier{9U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    map_api::LogicalLane successor_logical_lane{
        map_api::Identifier{10U}, {}, {}, nullptr, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}};
    GetLogicalLane().predecessor_lanes =
        std::vector<map_api::RefWrapper<map_api::LogicalLane>>{predecessor_logical_lane};
    GetLogicalLane().successor_lanes = std::vector<map_api::RefWrapper<map_api::LogicalLane>>{successor_logical_lane};

    // When

    const std::unordered_set<mantle_api::UniqueId> existing_logical_lane_ids{predecessor_logical_lane.id,
                                                                             successor_logical_lane.id};
    FillLogicalLaneConnections(&GetLogicalLane(), &GetOsiLogicalLane(), existing_logical_lane_ids);

    // Expect

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_LaneConnection> expected_predecessor_lanes;
    auto first_predecessor_lane = expected_predecessor_lanes.Add();
    ASSERT_EQ(1, GetLogicalLane().predecessor_lanes.size());
    first_predecessor_lane->mutable_other_lane_id()->set_value(GetLogicalLane().predecessor_lanes.at(0).get().id);
    // TODO: map_api does not define an equivalent of osi3::LogicalLane_LaneConnection
    // therefore, unable to set the `at_begin_of_other_lane_` here.

    google::protobuf::RepeatedPtrField<osi3::LogicalLane_LaneConnection> expected_successor_lanes;
    auto first_successor_lane = expected_successor_lanes.Add();
    ASSERT_EQ(1, GetLogicalLane().successor_lanes.size());
    first_successor_lane->mutable_other_lane_id()->set_value(GetLogicalLane().successor_lanes.at(0).get().id);
    // TODO: map_api does not define an equivalent of osi3::LogicalLane_LaneConnection
    // therefore, unable to set the `at_begin_of_other_lane_` here.

    AssertEqualLists(expected_predecessor_lanes, GetOsiLogicalLane().predecessor_lane());
    AssertEqualLists(expected_successor_lanes, GetOsiLogicalLane().successor_lane());
}

}  // namespace gtgen::core::environment::proto_groundtruth
