/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/stationary_object_proto_converter_utils.h"

#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::proto_groundtruth
{

ProtoStationaryObjectClassification::Type ConvertStaticObjectTypeToProtoStationaryObjectType(
    const mantle_api::StaticObjectType static_object_type)
{
    switch (static_object_type)
    {
        case mantle_api::StaticObjectType::kOther:
        {
            return ProtoStationaryObjectClassification::TYPE_OTHER;
        }
        case mantle_api::StaticObjectType::kBridge:
        {
            return ProtoStationaryObjectClassification::TYPE_BRIDGE;
        }
        case mantle_api::StaticObjectType::kBuilding:
        {
            return ProtoStationaryObjectClassification::TYPE_BUILDING;
        }
        case mantle_api::StaticObjectType::kPole:
        {
            return ProtoStationaryObjectClassification::TYPE_POLE;
        }
        case mantle_api::StaticObjectType::kPylon:
        {
            return ProtoStationaryObjectClassification::TYPE_PYLON;
        }
        case mantle_api::StaticObjectType::kDelineator:
        {
            return ProtoStationaryObjectClassification::TYPE_DELINEATOR;
        }
        case mantle_api::StaticObjectType::kTree:
        {
            return ProtoStationaryObjectClassification::TYPE_TREE;
        }
        case mantle_api::StaticObjectType::kBarrier:
        {
            return ProtoStationaryObjectClassification::TYPE_BARRIER;
        }
        case mantle_api::StaticObjectType::kVegetation:
        {
            return ProtoStationaryObjectClassification::TYPE_VEGETATION;
        }
        case mantle_api::StaticObjectType::kCurbstone:
        {
            return ProtoStationaryObjectClassification::TYPE_CURBSTONE;
        }
        case mantle_api::StaticObjectType::kWall:
        {
            return ProtoStationaryObjectClassification::TYPE_WALL;
        }
        case mantle_api::StaticObjectType::kVerticalStructure:
        {
            return ProtoStationaryObjectClassification::TYPE_VERTICAL_STRUCTURE;
        }
        case mantle_api::StaticObjectType::kRectangularStructure:
        {
            return ProtoStationaryObjectClassification::TYPE_RECTANGULAR_STRUCTURE;
        }
        case mantle_api::StaticObjectType::kOverheadStructure:
        {
            return ProtoStationaryObjectClassification::TYPE_OVERHEAD_STRUCTURE;
        }
        case mantle_api::StaticObjectType::kReflectiveStructure:
        {
            return ProtoStationaryObjectClassification::TYPE_REFLECTIVE_STRUCTURE;
        }
        case mantle_api::StaticObjectType::kConstructionSiteElement:
        {
            return ProtoStationaryObjectClassification::TYPE_CONSTRUCTION_SITE_ELEMENT;
        }
        case mantle_api::StaticObjectType::kSpeedBump:
        {
            return ProtoStationaryObjectClassification::TYPE_SPEED_BUMP;
        }
        case mantle_api::StaticObjectType::kEmittingStructure:
        {
            return ProtoStationaryObjectClassification::TYPE_EMITTING_STRUCTURE;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in GTGen static object type to proto stationary object type")
        }
    }
}

}  // namespace gtgen::core::environment::proto_groundtruth
