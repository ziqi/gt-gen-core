/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/traffic_sign_proto_converter.h"

#include "Core/Environment/Entities/traffic_sign_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Profiling/profiling.h"

namespace gtgen::core::environment::proto_groundtruth
{

void FillSupplementarySignValues(const environment::map::MountedSign::SupplementarySign& gtgen_supp_traffic_sign,
                                 osi3::TrafficSign::SupplementarySign& gt_supp_traffic_sign)
{
    GTGEN_PROFILE_SCOPE

    for (const auto& gtgen_supp_sign_value : gtgen_supp_traffic_sign.value_information)
    {
        auto proto_sign_value = gt_supp_traffic_sign.mutable_classification()->mutable_value()->Add();

        proto_sign_value->set_value(gtgen_supp_sign_value.value);
        proto_sign_value->set_value_unit(static_cast<osi3::TrafficSignValue::Unit>(gtgen_supp_sign_value.value_unit));
        proto_sign_value->set_text(gtgen_supp_sign_value.text);
    }
}

void FillTrafficSignSupplementarySigns(const environment::map::MountedSign& gtgen_traffic_sign,
                                       osi3::TrafficSign& gt_traffic_sign)
{
    GTGEN_PROFILE_SCOPE

    for (const auto& gtgen_supplementary_sign : gtgen_traffic_sign.supplementary_signs)
    {
        auto proto_supp_traffic_sign = gt_traffic_sign.add_supplementary_sign();

        proto_supp_traffic_sign->mutable_classification()->set_type(
            static_cast<osi3::TrafficSign::SupplementarySign::Classification::Type>(gtgen_supplementary_sign.type));
        proto_supp_traffic_sign->mutable_classification()->set_variability(
            static_cast<osi3::TrafficSign::Variability>(gtgen_supplementary_sign.variability));
        service::gt_conversion::FillProtoObject(gtgen_supplementary_sign.pose.position,
                                                proto_supp_traffic_sign->mutable_base()->mutable_position());
        service::gt_conversion::FillProtoObject(gtgen_supplementary_sign.dimensions,
                                                proto_supp_traffic_sign->mutable_base()->mutable_dimension());
        service::gt_conversion::FillProtoObject(gtgen_supplementary_sign.pose.orientation,
                                                proto_supp_traffic_sign->mutable_base()->mutable_orientation());
        FillSupplementarySignValues(gtgen_supplementary_sign, *proto_supp_traffic_sign);

        auto proto_sign_lane_id = proto_supp_traffic_sign->mutable_classification()->mutable_assigned_lane_id();
        for (const auto& gtgen_lane_id : gtgen_supplementary_sign.GetAssignedLanes())
        {
            proto_sign_lane_id->Add()->set_value(gtgen_lane_id);
        }

        auto proto_actors = proto_supp_traffic_sign->mutable_classification()->mutable_actor();
        for (const auto& gtgen_actor : gtgen_supplementary_sign.actors)
        {
            proto_actors->Add(static_cast<osi3::TrafficSign_SupplementarySign_Classification_Actor>(gtgen_actor));
        }
    }
}

void FillProtoGroundTruthTrafficSign(const map::TrafficSign* gtgen_traffic_sign, osi3::GroundTruth& groundtruth)
{
    auto proto_traffic_sign = groundtruth.add_traffic_sign();

    service::gt_conversion::FillProtoObject(
        gtgen_traffic_sign->pose.position, proto_traffic_sign->mutable_main_sign()->mutable_base()->mutable_position());
    service::gt_conversion::FillProtoObject(
        gtgen_traffic_sign->dimensions, proto_traffic_sign->mutable_main_sign()->mutable_base()->mutable_dimension());
    service::gt_conversion::FillProtoObject(
        gtgen_traffic_sign->pose.orientation,
        proto_traffic_sign->mutable_main_sign()->mutable_base()->mutable_orientation());

    proto_traffic_sign->mutable_id()->set_value(gtgen_traffic_sign->id);
    osi3::TrafficSign_MainSign_Classification* proto_class =
        proto_traffic_sign->mutable_main_sign()->mutable_classification();
    proto_class->set_type(static_cast<osi3::TrafficSign::MainSign::Classification::Type>(gtgen_traffic_sign->type));
    proto_class->set_variability(static_cast<osi3::TrafficSign::Variability>(gtgen_traffic_sign->variability));
    proto_class->set_direction_scope(
        static_cast<osi3::TrafficSign::MainSign::Classification::DirectionScope>(gtgen_traffic_sign->direction_scope));
    proto_class->mutable_value()->set_text(gtgen_traffic_sign->value_information.text);
    proto_class->mutable_value()->set_value(gtgen_traffic_sign->value_information.value);
    proto_class->mutable_value()->set_value_unit(
        static_cast<osi3::TrafficSignValue::Unit>(gtgen_traffic_sign->value_information.value_unit));

    for (const auto& gtgen_lane_id : gtgen_traffic_sign->GetAssignedLanes())
    {
        proto_class->mutable_assigned_lane_id()->Add()->set_value(gtgen_lane_id);
    }

    if (auto gtgen_mounted_sign{dynamic_cast<const environment::map::MountedSign*>(gtgen_traffic_sign)};
        gtgen_mounted_sign != nullptr)
    {
        FillTrafficSignSupplementarySigns(*gtgen_mounted_sign, *proto_traffic_sign);
    }
}

void FillProtoGroundTruthRoadMarking(const map::GroundSign* gtgen_ground_sign, osi3::GroundTruth& groundtruth)
{
    auto proto_road_marking = groundtruth.add_road_marking();

    // Conversion of Stationary Base
    service::gt_conversion::FillProtoObject(gtgen_ground_sign->pose.position,
                                            proto_road_marking->mutable_base()->mutable_position());
    service::gt_conversion::FillProtoObject(gtgen_ground_sign->dimensions,
                                            proto_road_marking->mutable_base()->mutable_dimension());
    service::gt_conversion::FillProtoObject(gtgen_ground_sign->pose.orientation,
                                            proto_road_marking->mutable_base()->mutable_orientation());

    proto_road_marking->mutable_id()->set_value(gtgen_ground_sign->id);

    // Conversion of Road Marking Classification
    osi3::RoadMarking_Classification* proto_class = proto_road_marking->mutable_classification();

    proto_class->set_type(static_cast<osi3::RoadMarking::Classification::Type>(gtgen_ground_sign->marking_type));

    proto_class->set_monochrome_color(
        ConvertGtGenRoadMarkingColorToOsi(gtgen_ground_sign->marking_color, gtgen_ground_sign->id));

    proto_class->set_value_text(gtgen_ground_sign->value_information.text);

    proto_class->set_traffic_main_sign_type(
        static_cast<osi3::TrafficSign::MainSign::Classification::Type>(gtgen_ground_sign->type));

    // Conversion of Traffic Sign Base
    proto_class->mutable_value()->set_text(gtgen_ground_sign->value_information.text);
    proto_class->mutable_value()->set_value(gtgen_ground_sign->value_information.value);
    proto_class->mutable_value()->set_value_unit(
        static_cast<osi3::TrafficSignValue::Unit>(gtgen_ground_sign->value_information.value_unit));

    for (const auto& gtgen_lane_id : gtgen_ground_sign->GetAssignedLanes())
    {
        proto_class->mutable_assigned_lane_id()->Add()->set_value(gtgen_lane_id);
    }
}

osi3::RoadMarking_Classification_Color ConvertGtGenRoadMarkingColorToOsi(
    const osi::OsiRoadMarkingColor& gtgen_marking_color,
    const mantle_api::UniqueId& road_marking_id)
{
    switch (gtgen_marking_color)
    {
        case osi::OsiRoadMarkingColor::kUnknown:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_UNKNOWN;
        }
        case osi::OsiRoadMarkingColor::kOther:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_OTHER;
        }
        case osi::OsiRoadMarkingColor::kWhite:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_WHITE;
        }
        case osi::OsiRoadMarkingColor::kYellow:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_YELLOW;
        }
        case osi::OsiRoadMarkingColor::kBlue:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_BLUE;
        }
        case osi::OsiRoadMarkingColor::kRed:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_RED;
        }
        case osi::OsiRoadMarkingColor::kGreen:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_GREEN;
        }
        case osi::OsiRoadMarkingColor::kViolet:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_VIOLET;
        }
        case osi::OsiRoadMarkingColor::kOrange:
        {
            return osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_ORANGE;
        }
        default:
        {
            throw EnvironmentException(
                "SensorViewBuilder: An unexpected enum value for the road marking color of the ground sign with id {} "
                "has been provided. Actual "
                "value: {}",
                road_marking_id,
                gtgen_marking_color);
        }
    }
}

void AddSupplementarySignEntityToGroundTruth(const mantle_api::IEntity& supplementary_sign_entity,
                                             osi3::TrafficSign& gt_traffic_sign)
{

    const auto* properties =
        dynamic_cast<mantle_ext::SupplementarySignProperties*>(supplementary_sign_entity.GetProperties());
    if (!properties)
    {
        Warn("Entity named '{}' is not a supplementary sign and cannot be added to the traffic sign groundtruth.",
             supplementary_sign_entity.GetName());
        return;
    }

    auto proto_supp_traffic_sign = gt_traffic_sign.add_supplementary_sign();
    auto* proto_base = proto_supp_traffic_sign->mutable_base();

    service::gt_conversion::FillProtoObject(supplementary_sign_entity.GetPosition(), proto_base->mutable_position());
    service::gt_conversion::FillProtoObject(properties->bounding_box.dimension, proto_base->mutable_dimension());
    service::gt_conversion::FillProtoObject(supplementary_sign_entity.GetOrientation(),
                                            proto_base->mutable_orientation());

    auto* proto_class = proto_supp_traffic_sign->mutable_classification();
    proto_class->set_type(
        static_cast<osi3::TrafficSign::SupplementarySign::Classification::Type>(properties->supplementary_sign_type));
    proto_class->set_variability(static_cast<osi3::TrafficSign::Variability>(properties->variability));

    auto* proto_value = proto_class->add_value();
    proto_value->set_value_unit(static_cast<osi3::TrafficSignValue::Unit>(properties->unit));
    proto_value->set_value(properties->value);
    proto_value->set_text(properties->text);

    auto* proto_actors = proto_class->mutable_actor();
    for (const auto& gtgen_actor : properties->actors)
    {
        proto_actors->Add(static_cast<osi3::TrafficSign_SupplementarySign_Classification_Actor>(gtgen_actor));
    }
}

void AddTrafficSignEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth)
{
    const auto* properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties());
    if (properties == nullptr)
    {
        return;
    }

    auto* proto_traffic_sign = ground_truth.add_traffic_sign();
    auto* proto_base = proto_traffic_sign->mutable_main_sign()->mutable_base();
    auto* proto_class = proto_traffic_sign->mutable_main_sign()->mutable_classification();

    proto_traffic_sign->mutable_id()->set_value(entity->GetUniqueId());
    service::gt_conversion::FillProtoObject(entity->GetPosition(), proto_base->mutable_position());
    service::gt_conversion::FillProtoObject(entity->GetOrientation(), proto_base->mutable_orientation());
    service::gt_conversion::FillProtoObject(properties->bounding_box.dimension, proto_base->mutable_dimension());

    proto_class->set_type(static_cast<osi3::TrafficSign::MainSign::Classification::Type>(properties->sign_type));
    proto_class->set_variability(static_cast<osi3::TrafficSign::Variability>(properties->variability));
    proto_class->set_direction_scope(
        static_cast<osi3::TrafficSign::MainSign::Classification::DirectionScope>(properties->direction));
    proto_class->mutable_value()->set_value_unit(static_cast<osi3::TrafficSignValue::Unit>(properties->unit));
    proto_class->mutable_value()->set_value(properties->value);
    proto_class->mutable_value()->set_text(properties->text);

    for (const auto& gtgen_lane_id : entity->GetAssignedLaneIds())
    {
        proto_class->mutable_assigned_lane_id()->Add()->set_value(gtgen_lane_id);
    }

    if (const auto* traffic_sign_entity = dynamic_cast<const entities::TrafficSignEntity*>(entity))
    {
        for (const auto& [key, supplementary_sign] : traffic_sign_entity->GetSupplementarySigns())
        {
            AddSupplementarySignEntityToGroundTruth(supplementary_sign, *proto_traffic_sign);
        }
    }
}

void AddRoadMarkingEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth)
{
    const auto* properties = dynamic_cast<mantle_ext::RoadMarkingProperties*>(entity->GetProperties());
    if (properties == nullptr)
    {
        return;
    }

    auto* proto_road_marking = ground_truth.add_road_marking();
    auto* proto_base = proto_road_marking->mutable_base();
    auto* proto_class = proto_road_marking->mutable_classification();

    proto_road_marking->mutable_id()->set_value(entity->GetUniqueId());
    service::gt_conversion::FillProtoObject(entity->GetPosition(), proto_base->mutable_position());
    service::gt_conversion::FillProtoObject(entity->GetOrientation(), proto_base->mutable_orientation());
    service::gt_conversion::FillProtoObject(properties->bounding_box.dimension, proto_base->mutable_dimension());

    proto_class->set_type(static_cast<osi3::RoadMarking::Classification::Type>(properties->marking_type));
    proto_class->set_monochrome_color(
        ConvertGtGenRoadMarkingColorToOsi(properties->marking_color, entity->GetUniqueId()));

    proto_class->set_traffic_main_sign_type(
        static_cast<osi3::TrafficSign::MainSign::Classification::Type>(properties->sign_type));
    proto_class->mutable_value()->set_value_unit(static_cast<osi3::TrafficSignValue::Unit>(properties->unit));
    proto_class->mutable_value()->set_value(properties->value);
    proto_class->mutable_value()->set_text(properties->text);
    proto_class->set_value_text(properties->text);

    for (const auto& gtgen_lane_id : entity->GetAssignedLaneIds())
    {
        proto_class->mutable_assigned_lane_id()->Add()->set_value(gtgen_lane_id);
    }
}

}  // namespace gtgen::core::environment::proto_groundtruth
