/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H

#include "Core/Environment/Map/GtGenMap/signs.h"
#include "osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::proto_groundtruth
{
/// @deprecated - remove when legacy code is deleted - and the gtgen map does contain only mantle entities
void FillProtoGroundTruthTrafficSign(const environment::map::TrafficSign* gtgen_traffic_sign,
                                     osi3::GroundTruth& groundtruth);

void AddSupplementarySignEntityToGroundTruth(const mantle_api::IEntity& supplementary_sign_entity,
                                             osi3::TrafficSign& gt_traffic_sign);

void AddTrafficSignEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth);

/// @deprecated - remove when legacy code is deleted - and the gtgen map does contain only mantle entities
void FillProtoGroundTruthRoadMarking(const environment::map::GroundSign* gtgen_ground_sign,
                                     osi3::GroundTruth& groundtruth);

void AddRoadMarkingEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth);

osi3::RoadMarking_Classification_Color ConvertGtGenRoadMarkingColorToOsi(
    const osi::OsiRoadMarkingColor& gtgen_marking_color,
    const mantle_api::UniqueId& road_marking_id);

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H
