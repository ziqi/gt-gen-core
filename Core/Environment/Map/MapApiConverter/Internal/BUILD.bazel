cc_library(
    name = "lane_boundary_converter",
    srcs = ["lane_boundary_converter.cpp"],
    hdrs = ["lane_boundary_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "lane_boundary_converter_test",
    timeout = "short",
    srcs = ["lane_boundary_converter_test.cpp"],
    # Necessary for EXPECT_EQ(ConvertLaneBoundaryColor(static_cast<map_api::LaneBoundary::Color>(999)), LaneBoundary::Color::kUnknown);
    copts = ["-Wno-conversion"],
    deps = [
        ":lane_boundary_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "lane_converter",
    srcs = ["lane_converter.cpp"],
    hdrs = ["lane_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "lane_converter_test",
    timeout = "short",
    srcs = ["lane_converter_test.cpp"],
    deps = [
        ":lane_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "lane_group_converter",
    srcs = ["lane_group_converter.cpp"],
    hdrs = ["lane_group_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "lane_group_converter_test",
    timeout = "short",
    srcs = ["lane_group_converter_test.cpp"],
    copts = ["-Wno-conversion"],
    deps = [
        ":lane_group_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "mapapi_to_gtgenmap_converter_impl",
    srcs = ["mapapi_to_gtgenmap_converter_impl.cpp"],
    hdrs = ["mapapi_to_gtgenmap_converter_impl.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__subpackages__"],
    deps = [
        ":lane_boundary_converter",
        ":lane_converter",
        ":lane_group_converter",
        ":road_object_converter",
        ":roadmarking_converter",
        ":traffic_light_converter",
        ":traffic_sign_converter",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/Map/Common:i_any_to_gtgenmap_converter",
        "//Core/Environment/Map/GtGenMap:gtgen_map_finalizer",
        "//Core/Service/Logging:logging",
        "//Core/Service/Utility:unique_id_provider",
    ],
)

cc_test(
    name = "mapapi_to_gtgenmap_converter_impl_test",
    timeout = "short",
    srcs = ["mapapi_to_gtgenmap_converter_impl_test.cpp"],
    deps = [
        ":mapapi_to_gtgenmap_converter_impl",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "roadmarking_converter",
    srcs = ["roadmarking_converter.cpp"],
    hdrs = ["roadmarking_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        ":roadmarking_types_converter",
        ":traffic_sign_converter",
        ":traffic_sign_types_converter",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "roadmarking_converter_test",
    timeout = "short",
    srcs = ["roadmarking_converter_test.cpp"],
    deps = [
        ":roadmarking_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "roadmarking_types_converter",
    srcs = ["roadmarking_types_converter.cpp"],
    hdrs = ["roadmarking_types_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "roadmarking_types_converter_test",
    timeout = "short",
    srcs = ["roadmarking_types_converter_test.cpp"],
    deps = [
        ":roadmarking_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_light_converter",
    srcs = ["traffic_light_converter.cpp"],
    hdrs = ["traffic_light_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        ":traffic_sign_types_converter",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "traffic_light_converter_test",
    timeout = "short",
    srcs = ["traffic_light_converter_test.cpp"],
    deps = [
        ":traffic_light_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_sign_converter",
    srcs = ["traffic_sign_converter.cpp"],
    hdrs = ["traffic_sign_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        ":traffic_sign_types_converter",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "traffic_sign_converter_test",
    timeout = "short",
    srcs = ["traffic_sign_converter_test.cpp"],
    deps = [
        ":traffic_sign_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_sign_types_converter",
    srcs = ["traffic_sign_types_converter.cpp"],
    hdrs = ["traffic_sign_types_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "traffic_sign_types_converter_test",
    timeout = "short",
    srcs = ["traffic_sign_types_converter_test.cpp"],
    deps = [
        ":traffic_sign_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "road_object_converter",
    srcs = ["road_object_converter.cpp"],
    hdrs = ["road_object_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        ":road_object_types_converter",
        "//Core/Environment/Map/GtGenMap:road_objects",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "road_object_converter_test",
    timeout = "short",
    srcs = ["road_object_converter_test.cpp"],
    deps = [
        ":road_object_converter",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "road_object_types_converter",
    srcs = ["road_object_types_converter.cpp"],
    hdrs = ["road_object_types_converter.h"],
    visibility = ["//Core/Environment/Map/MapApiConverter:__pkg__"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Service/Osi:osi",
        "@mantle_api",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "road_object_types_converter_test",
    timeout = "short",
    srcs = ["road_object_types_converter_test.cpp"],
    deps = [
        ":road_object_converter",
        "@googletest//:gtest_main",
    ],
)
