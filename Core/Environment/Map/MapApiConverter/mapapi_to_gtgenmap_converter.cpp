/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/mapapi_to_gtgenmap_converter.h"

namespace gtgen::core::environment::map
{

MapApiToGtGenMapConverter::MapApiToGtGenMapConverter(service::utility::UniqueIdProvider& unique_id_provider,
                                                     const map_api::Map& data,
                                                     GtGenMap& gtgen_map)
    : impl_{std::make_unique<MapApiToGtGenMapConverterImpl>(unique_id_provider, data, gtgen_map)}
{
}

MapApiToGtGenMapConverter::~MapApiToGtGenMapConverter() = default;

void MapApiToGtGenMapConverter::Convert()
{
    impl_->Convert();
}

std::map<mantle_api::UniqueId, mantle_api::UniqueId> MapApiToGtGenMapConverter::GetNativeToGtGenTrafficLightIdMap()
    const
{
    return {};
}

}  // namespace gtgen::core::environment::map
