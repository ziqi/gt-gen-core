/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANESHAPEEXTRACTOR_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANESHAPEEXTRACTOR_H

#include <vector>

namespace gtgen::core::environment::map
{
struct Lane;
struct LaneBoundary;

void ExtractLaneShape(Lane& gtgen_lane,
                      const std::vector<const LaneBoundary*>& left_lane_boundaries,
                      const std::vector<const LaneBoundary*>& right_lane_boundaries);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANESHAPEEXTRACTOR_H
