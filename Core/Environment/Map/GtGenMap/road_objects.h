/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_ROADOBJECTS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_ROADOBJECTS_H

#include "Core/Service/Osi/stationary_object_types.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>

#include <string>
#include <vector>

namespace gtgen::core::environment::map
{

struct RoadObject
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> base_polygon;
    std::string name;
    mantle_api::Pose pose;
    mantle_api::Dimension3 dimensions;
    mantle_api::StaticObjectType type{mantle_api::StaticObjectType::kInvalid};
    osi::StationaryObjectEntityMaterial material{osi::StationaryObjectEntityMaterial::kOther};
    mantle_api::UniqueId id{0};
};

using RoadObjects = std::vector<RoadObject>;

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_ROADOBJECTS_H
