/*******************************************************************************
 * Copyright (c) 2018-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2018-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_PATCH_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_PATCH_H

#include "Core/Environment/Map/Geometry/polygon.h"

#include <MantleAPI/Common/i_identifiable.h>

#include <memory>
#include <vector>

namespace gtgen::core::environment::map
{
struct Patch
{
    Patch(mantle_api::UniqueId id, const Polygon3d& shape_3d) : id{id}, shape_3d{shape_3d}
    {
        for (const auto point : shape_3d.points)
        {
            shape_2d.points.push_back(glm::vec2{point.x, point.y});
        }
    }

    virtual ~Patch() = default;
    Patch(const Patch& other) = default;
    Patch(Patch&& other) = default;
    Patch& operator=(const Patch& other) = default;
    Patch& operator=(Patch&& other) = default;

    mantle_api::UniqueId id;

    Polygon3d shape_3d;
    Polygon2d shape_2d;
};

using Patches = std::vector<std::shared_ptr<Patch>>;

struct FixedFrictionPatch : Patch
{
    FixedFrictionPatch(mantle_api::UniqueId id, const Polygon3d& shape_3d, double mue) : Patch{id, shape_3d}, mue{mue}
    {
    }

    ~FixedFrictionPatch() override = default;
    FixedFrictionPatch(const FixedFrictionPatch& other) = default;
    FixedFrictionPatch(FixedFrictionPatch&& other) = default;
    FixedFrictionPatch& operator=(const FixedFrictionPatch& other) = default;
    FixedFrictionPatch& operator=(FixedFrictionPatch&& other) = default;

    double mue;
};

using FixedFrictionPatches = std::vector<std::shared_ptr<FixedFrictionPatch>>;

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_PATCH_H
