/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_MAP_API_DATA_WRAPPER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_MAP_API_DATA_WRAPPER_H

#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MapAPI/map.h>

#include <unordered_map>

namespace gtgen::core::environment::map
{

class MapApiDataWrapper
{
  public:
    template <class T>
    const T* Find(mantle_api::UniqueId id) const
    {
        if (Contains<T>(id))
        {
            const std::size_t index = GetIndexMap<T>().at(id);
            return &GetDataContainer<T>()[index];
        }
        return nullptr;
    }

    template <class T>
    T* Find(mantle_api::UniqueId id)
    {
        return const_cast<T*>(std::as_const(*this).Find<T>(id));
    }

    /// @brief Get the single element from the container with specific id
    template <class T>
    const T& Get(mantle_api::UniqueId id) const
    {
        auto* element = Find<T>(id);
        if (!element)
        {
            GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(exception::MapElementNotFound(
                ("Could not find " + GetExceptionString<T>() + " with ID {}").c_str(), id));
        }
        return *element;
    }

    template <class T>
    T& Get(mantle_api::UniqueId id)
    {
        return const_cast<T&>(std::as_const(*this).Get<T>(id));
    }

    /// @brief Get the data type container itself
    template <class T>
    const std::vector<T>& GetAll() const
    {
        return GetDataContainer<T>();
    }

    template <class T>
    void Add(T element)
    {
        mantle_api::UniqueId id = element.id;

        if (Contains<T>(id))
        {
            GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(exception::MapElementNotFound(
                ("Tried to add " + GetExceptionString<T>() + " already contained in the map ID {}").c_str(), id));
        }

        if constexpr (std::is_same_v<T, map_api::LogicalLaneBoundary>)
        {
            if (element.reference_line)
            {
                element.reference_line = Find<map_api::ReferenceLine>(element.reference_line->id);
            }
            RefreshReferences(element.physical_boundaries);
        }

        GetDataContainer<T>().emplace_back(std::move(element));
        GetIndexMap<T>()[id] = GetDataContainer<T>().size() - 1;
    }

    void RefreshReferencesForLogicalLane(map_api::LogicalLane& logical_lane);

    void ClearMap();

  private:
    template <class T>
    void RefreshReferences(std::vector<T>& reference_vector)
    {
        for (auto& element : reference_vector)
        {
            try
            {
                element = std::ref(Get<typename T::type>(element.get().id));
            }
            catch (const exception::MapElementNotFound&)
            {
                // Element not found, skip updating this reference
                continue;
            }
        }
    }

    template <class T>
    bool Contains(mantle_api::UniqueId id) const
    {
        return GetIndexMap<T>().count(id) > 0;
    }

    using IndexMap = std::unordered_map<mantle_api::UniqueId, std::size_t>;

    template <class T>
    const IndexMap& GetIndexMap() const;

    template <class T>
    IndexMap& GetIndexMap()
    {
        return const_cast<IndexMap&>(std::as_const(*this).GetIndexMap<T>());
    }

    template <class T>
    const std::vector<T>& GetDataContainer() const;

    template <class T>
    std::vector<T>& GetDataContainer()
    {
        return const_cast<std::vector<T>&>(std::as_const(*this).GetDataContainer<T>());
    }

    template <class T>
    std::string GetExceptionString() const;

  protected:
    std::vector<map_api::ReferenceLine> reference_lines_{};
    std::vector<map_api::Lane> lanes_{};
    std::vector<map_api::LaneBoundary> lane_boundaries_{};
    std::vector<map_api::LogicalLane> logical_lanes_{};
    std::vector<map_api::LogicalLaneBoundary> logical_lane_boundaries_{};

    IndexMap id_reference_line_index_map_{};
    IndexMap id_lane_index_map_{};
    IndexMap id_lane_boundary_index_map_{};
    IndexMap id_logical_lane_index_map_{};
    IndexMap id_logical_lane_boundary_index_map_{};
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_MAP_API_DATA_WRAPPER_H
