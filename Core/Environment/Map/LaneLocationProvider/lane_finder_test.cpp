/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/LaneLocationProvider/lane_finder.h"

#include "Core/Tests/TestUtils/MapUtils/gtgen_map_builder.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{

TEST(LaneFinderTest, GivenEmptyGtGenMap_WhenCheckingForNotExistingLane_ThenLaneIsNotContained)
{
    test_utils::GtGenMapBuilder map_builder;
    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_FALSE(lane_finder.LaneExists(0));
}

TEST(LaneFinderTest, GivenNonEmptyGtGenMap_WhenCheckingForNotExistingLane_ThenLaneIsNotContained)
{
    test_utils::GtGenMapBuilder map_builder;
    map_builder.AddLane(1, 1);
    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_FALSE(lane_finder.LaneExists(0));
}

TEST(LaneFinderTest, GivenNonEmptyGtGenMap_WhenCheckingForExistingLane_ThenLaneIsContained)
{
    mantle_api::UniqueId lane_id = 0;
    test_utils::GtGenMapBuilder map_builder;
    map_builder.AddLane(0, lane_id);

    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_TRUE(lane_finder.LaneExists(lane_id));
}

TEST(LaneFinderTest, GivenEmptyGtGenMap_WhenCheckingMultipleNonExistingLanes_ThenNoLaneIsContained)
{
    mantle_api::UniqueId lane_relation0{0};
    mantle_api::UniqueId lane_relation1{1};
    mantle_api::UniqueId lane_relation2{2};
    test_utils::GtGenMapBuilder map_builder;

    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_FALSE(lane_finder.AnyLaneExists({lane_relation0, lane_relation1, lane_relation2}));
}

TEST(LaneFinderTest, GivenNonEmptyGtGenMap_WhenCheckingMultipleNonExistingLanes_ThenNoLaneIsContained)
{
    mantle_api::UniqueId lane_relation1{1};
    mantle_api::UniqueId lane_relation2{2};
    test_utils::GtGenMapBuilder map_builder;
    map_builder.AddLane(0, 0);

    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_FALSE(lane_finder.AnyLaneExists({lane_relation1, lane_relation2}));
}

TEST(LaneFinderTest, GivenNonEmptyGtGenMap_WhenCheckingExistenceMultipleLanes_ThenLaneIsContained)
{
    mantle_api::UniqueId lane_0_id = 0;
    mantle_api::UniqueId lane_relation1{1};
    mantle_api::UniqueId lane_relation2{2};
    test_utils::GtGenMapBuilder map_builder;
    map_builder.AddLane(0, lane_0_id);

    LaneFinder lane_finder(map_builder.gtgen_map);
    EXPECT_TRUE(lane_finder.AnyLaneExists({lane_0_id, lane_relation1, lane_relation2}));
}

}  // namespace gtgen::core::environment::map
