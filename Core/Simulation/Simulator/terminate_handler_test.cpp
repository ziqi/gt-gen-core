/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/terminate_handler.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace gtgen::core::simulation::simulator
{

TEST(TerminateHandlerTest, GivenNoCustomTerminateHandler_WhenTerminateHandlerCalled_ThenExit)
{
    EXPECT_EXIT(TerminateHandler(), testing::ExitedWithCode(1), ".*");
}

TEST(TerminateHandlerTest,
     GivenCustomTerminateHandler_WhenTerminateHandlerCalled_ThenExecutePreviousHandlerAndTerminate)
{
    SetTerminateHandler();
    EXPECT_DEATH(TerminateHandler(), ".*");
}

TEST(TerminateHandlerTest, GivenTerminateHandlerSet_WhenTerminate_ThenCorrectTerminateHandlerIsSet)
{
    SetTerminateHandler();

    auto expected_terminate_handler = TerminateHandler;
    auto current_terminate_handler = std::get_terminate();

    EXPECT_EQ(expected_terminate_handler, current_terminate_handler);
}

TEST(TerminateHandlerTest, GivenFlyingExcepton_WhenLogTerminateReason_ThenExceptionIsLogged)
{
    testing::internal::CaptureStdout();
    try
    {
        throw std::runtime_error("test");
    }
    catch (std::runtime_error&)
    {
        LogTerminateReasonAndCleanupLogging();
        std::string terminate_reason = testing::internal::GetCapturedStdout();
        EXPECT_THAT(terminate_reason, testing::HasSubstr("std::runtime_error \n    what(): test"));
    }
}

TEST(TerminateHandlerTest, GivenFlyingExcepton_WhenGetCurrentExceptionErrorMesage_ThenMessageCorrect)
{
    try
    {
        throw std::runtime_error("test");
    }
    catch (std::runtime_error&)
    {
        auto msg = GetCurrentExceptionErrorMsg();
        EXPECT_EQ("std::runtime_error \n    what(): test", msg);
    }
}

TEST(TerminateHandlerTest, GivenNoFlyingExcepton_WhenGetCurrentExceptionErrorMesage_ThenMessageEmpty)
{
    auto msg = GetCurrentExceptionErrorMsg();
    EXPECT_TRUE(msg.empty());
}

}  // namespace gtgen::core::simulation::simulator
