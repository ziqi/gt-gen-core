/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_FORMATTING_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_FORMATTING_H

#include "Core/Export/simulation_parameters.h"

#include <fmt/format.h>

#include <iterator>

/// @brief Formatter for SimulationParameters that influence the actual simulation execution
///
/// By default, prints all elements with description and separated by newlines.
/// Possible format strings:
///    {:modified} -- Same layout as default, but print only elements that are non-default
template <>
struct fmt::formatter<gtgen::core::SimulationParameters>
{
    template <typename ParseContext>
    auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        std::string format;
        auto it{ctx.begin()};
        while (it != ctx.end() && *it != '}')
        {
            ++it;
        }
        format.append(ctx.begin(), it);

        print_all_ = format != "modified";

        return it;
    }

    template <typename FormatContext>
    auto format(const gtgen::core::SimulationParameters& p,
                FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        std::string result;
        auto out{std::back_inserter(result)};
        Append(out, "Scenario", p.scenario, print_all_);
        Append(out, "Custom log directory", p.custom_log_directory, print_all_);
        Append(out, "Custom user settings", p.custom_user_settings_path, print_all_);
        Append(out, "Custom GTGEN_DATA path", p.custom_gtgen_data_directory, print_all_);
        Append(out, "Random seed", p.random_seed, print_all_);
        Append(out, "Custom output directory path", p.custom_output_directory_path, print_all_);
        Append(out, "Log cyclics", p.log_cyclics, print_all_);

        return format_to(ctx.out(), "{}", result);
    }

    template <typename OutIterator>
    void Append(OutIterator out, const std::string& label, const std::string& content, bool also_if_default = true)
    {
        if (also_if_default || !content.empty())
        {
            format_to(out, "\n{}: {}", label, content);
        }
    }

    template <typename OutIterator>
    void Append(OutIterator out, const std::string& label, bool content, bool append_if_default)
    {
        if (append_if_default || content)
        {
            format_to(out, "\n{}: {}", label, content);
        }
    }

  private:
    bool print_all_{false};
};

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_FORMATTING_H
