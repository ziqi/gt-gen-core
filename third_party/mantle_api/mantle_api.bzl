load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")

_TAG = "v8.0.0"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "abb18f7180da012c02bda874752a7efd9ceab1da31e616174790f07a17c4fa3f",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
