# Third Party Libraries in use

You can find the list of third-party libraries used in this project in the [NOTICE.md](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/blob/main/NOTICE.md?ref_type=heads). 

# Licenses in use

| License                                 |    Type     |                           Additional information/comment                           |
| --------------------------------------- | :---------: | :--------------------------------------------------------------------------------: |
| Apache-2.0                              | Open Source |                        free license, compatible with GPLv3                         |
| MIT License                             | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| Boost License 1.0                       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 2-Clause "Simplified" License       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 3-Clause "New" or "Revised" License | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| The Happy Bunny License and MIT License | Open Source |                     Happy Bunny License (modified MIT License)                     |
| Zlib License                            | Open Source |                           free license, without copyleft                           |
| Eclipse Public License 2.0              | Open Source | free license, **weak copyleft**, additions to the same module have to be published |
