load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@rules_proto//proto/private:dependencies.bzl", "protobuf_workspace")

_VERSION_PROTOBUF = "3.19.1"
_VERSION_PROTOBUF_EXTENDED = "3.19.1"

def protobuf():
    """External dependencies: protobuf with precompiled protoc"""

    maybe(
        http_archive,
        name = "com_github_protocolbuffers_protobuf",
        sha256 = "87407cd28e7a9c95d9f61a098a53cf031109d451a7763e7dd1253abf8b4df422",
        strip_prefix = "protobuf-{version}".format(version = _VERSION_PROTOBUF_EXTENDED),
        url = "https://github.com/protocolbuffers/protobuf/archive/v{version}.tar.gz".format(version = _VERSION_PROTOBUF_EXTENDED),
    )

    maybe(
        http_archive,
        name = "com_google_protobuf_protoc_linux_x86_64",
        build_file = "@rules_proto//proto/private:BUILD.protoc",
        sha256 = "4b18a69b3093432ee0531bc9bf3c4114f81bde1670ade2875f694180ac8bd7f6",
        url = "https://github.com/protocolbuffers/protobuf/releases/download/v{version}/protoc-{version}-linux-x86_64.zip".format(version = _VERSION_PROTOBUF),
    )

    protobuf_workspace(name = "com_google_protobuf")
