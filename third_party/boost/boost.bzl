load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

def create_alias_rules(modules):
    """Helper method to reduce code duplication for alias creation

    Args:
      modules: the list of modules for which to create aliases
    """

    for module in modules:
        native.alias(
            name = module,
            actual = "@boost_171//:{}".format(module),
            visibility = ["//visibility:public"],
        )

def boost_deps():
    maybe(
        http_archive,
        name = "boost_171",
        url = "https://archives.boost.io/release/1.71.0/source/boost_1_71_0.tar.gz",
        build_file = Label("//:third_party/boost/boost_171.BUILD"),
        strip_prefix = "boost_1_71_0/",
        sha256 = "96b34f7468f26a141f6020efb813f1a2f3dfb9797ecf76a7d7cbd843cc95f5bd",
        patches = [
            Label("//:third_party/boost/boost_asio_no_zero_as_nullptr_constant.patch"),
            # Resolves "undefined template 'boost::serialization::nvp'", see this issue:
            # https://github.com/boostorg/serialization/issues/186
            # The following issue indicates we will be able to drop this patch when moving to a version >= 1.73:
            # https://github.com/lgottwald/PaPILO/issues/31
            Label("//:third_party/boost/multiprecision_missing_nvp_include.patch"),
            Label("//:third_party/boost/singleton_reference_binding_to_null_pointer_is_an_undefined_behavior.patch"),
            # Resolves "load of value ___, which is not a valid value for type 'boost::dll::load_mode::type'". Issue:
            # https://github.com/boostorg/dll/issues/30
            # Fixed in version >= 1.73
            Label("//:third_party/boost/boost_dll_load_mode_enum_undefined_behavior.patch"),
        ],
    )
