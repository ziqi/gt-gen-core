load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")

_TAG = "v0.4.0"

def map_sdk():
    maybe(
        http_archive,
        name = "map_sdk",
        url = ECLIPSE_GITLAB + "/openpass/map-sdk/-/archive/{tag}/map-sdk-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "7e51abc9e049951b13cac39eba249fd7c5fb71444e70f5e4f741949e67483858",
        strip_prefix = "map-sdk-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
